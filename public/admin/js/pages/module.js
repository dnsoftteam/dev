$(function () {
    //load lang
    load_lang('module');
    //load table ajax
    init_data_table();
    //bind checkbox table
    init_checkbox_table();

    $("#category_id").change(function(){
        $( "form#a_form" ).submit();
    });


});



//form them moi
function add_form()
{
    save_method = 'add';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();
    $('#modal_form').modal('show');
}
//form sua
function edit_form(id)
{
    save_method = 'update';
    $('#form')[0].reset();
    $('.form-group').removeClass('has-error');
    $('.help-block').empty();

    //Ajax Load data from ajax
    $.ajax({
        url : url_ajax_edit+"/"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="name"]').val(data.name);
            $('[name="slug"]').val(data.slug);
            $('[name="description"]').val(data.description);
            $("#controller option[value='"+data.controller+"']").prop("selected", true);


            $('#modal_form').modal('show');
            $('.modal-title').text(language['heading_title_edit']);

            $('#modal_form').modal('show');
            $('.modal-title').text(language['heading_title_edit']);


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $(".modal-body").prepend(box_alert('alert-danger',language['error_try_again']));
        }
    });
}

//ajax luu form
function save()
{
    $('#btnSave').text(language['btn_saving']); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url;

    if(save_method == 'add') {
        url = url_ajax_add;
    } else {
        url = url_ajax_update;
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.error) //có lỗi
            {
                $(".modal-body").prepend(box_alert('alert-danger',data.msg));
            }
            else
            {
                $('#modal_form').modal('hide');
                reload_table();
            }

            $('#btnSave').text(language['btn_save']); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
            $('#token').val(data.token);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $(".modal-body").prepend(box_alert('alert-danger',language['error_try_again']));
            $('#btnSave').text(language['btn_save']); //change button text
            $('#btnSave').attr('disabled',false); //set button enable

        }
    });
}
