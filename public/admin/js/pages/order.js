$(function () {
    //load lang
    load_lang('order');
    //load table ajax
    init_data_table();
    //bind checkbox table
    init_checkbox_table();

    $(document).on('change','[name=is_status]', function () {
        var is_status=$(this).val();
        var id= $(this).data('id');
        var key= 'is_status';
        var cls='';
        console.log(is_status);
        switch (is_status){
            case  0:
                cls='label-danger';
                break;
            case 1:
                cls='label-info';
                break;
            case 2:
                cls='label-warning';
                break;
            case 3:
                cls='label-warning';
            case 4:
                cls='label-success';
                break;
            default:
                cls='label-info';
                break;
        }

        $.ajax({
            url:url_ajax_update,
            type:'POST',
            data:{val:is_status,key:key,id:id},
            success:function(data){
                console.log(data);
                $('#is_status').html('<span class="label '+cls+'">'+language['status_pay_'+is_status]+'</span>');
                reload_table();

            }
        });
    });
    $(document).on('change','[name=bill_code]', function () {
        var bill_code=$(this).val();
        var key= 'bill_code';
        var id= $(this).data('id');
        $.ajax({
            url:url_ajax_update,
            type:'POST',
            data:{val:bill_code,key:key,id:id},
            success:function(data){
                toastr.success('Cập nhật thành công');
                console.log(data);
                reload_table();

            }
        });
    });
    $(document).on('change','[name=transport_id]', function () {
        var transport_id=$(this).val();
        var key= 'transport_id';
        var id= $(this).data('id');
        console.log(id);
        $.ajax({
            url:url_ajax_update,
            type:'POST',
            data:{val:transport_id,key:key,id:id},
            success:function(data){
                toastr.success('Cập nhật thành công');

                reload_table();

            }
        });
    });
});
 function paymentStatus(id,payment_status){
     $('#payment_status').html('');
     var cls='';
     if(payment_status==1) cls='label-danger';
     else cls='label-success';
     var key='payment_status';
     $.ajax({
        url:url_ajax_update,
         type:"POST",
         data:{id:id,key:key,val:payment_status},
         success:function(data){
            console.log(data);
            $('#payment_status').html('<span class="label '+cls+'">'+language['status_pay_'+payment_status]+'</span>');
            reload_table();
         }
     });
 }
function view_item(id) {

    //Ajax Load data from ajax
    $.ajax({
        url : url_ajax_view+'/'+id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $.each(data, function( key, value ) {
                $('td#'+key).html(value);
            });

            $('table.list-detail tbody').html('');
            $.each(data.order_detail, function( key, value ) {
                var tr = '<tr id="'+key+'">';
                $.each(value, function (k,v) {
                    tr += '<td>'+v+'</td>';
                });
                tr += '</tr>';
                $('table.list-detail tbody').append(tr);
                console.log(tr);
            });
            $('#modal_form').modal('show');
            $('.modal-title').text(language['heading_title_view']);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $(".modal-body").prepend(box_alert('alert-danger',language['error_try_again']));
        }
    });
}




