$(function () {
    //load lang
    load_lang('product');
    //load slug
    init_slug('title', 'slug');
    //load table ajax
  init_data_table(20);
    //bind checkbox table
    init_checkbox_table();
    tinymce.init(optionTinyMCE);
    loadFilterCate();
  loadFilterCategory();
});

//form them moi
function add_form() {
    slug_disable = false;
    save_method = 'add';
    loadCategory();
    loadProfessional();
    // loadProperty('style_product');
    $('#modal_form').modal('show');
}

//form sua
function edit_form(id) {
    slug_disable = true;
    save_method = 'update';

    //Ajax Load data from ajax
    $.ajax({
        url: url_ajax_edit + "/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data) {
            $.each(data.data, function (key, value) {

              $.each(value, function( k, v) {
                if($.inArray(k,['title','meta_title','description','meta_description','slug','content','meta_keyword','content']) !== -1){
                  $('[name="'+k+'['+value.language_code+']"]').val(v);
                  if(tinymce.get(k+'_' + value.language_code)) tinymce.get(k+'_' + value.language_code).setContent(v);
                }else{
                  $('[name="'+k+'"]').val(v);
                }
              });
                $('[name="meta_keyword[' + value.language_code + ']"]').tagsinput('add', value.meta_keyword);
            });



            // if (data[0].is_featured && data[0].is_featured == 1) $('input[name="is_featured"]').bootstrapSwitch('state', true);

            if (data.category_id) {
                loadCategory(data.category_id);
                $('select[name="category_id[]"] > option').prop("selected", "selected").trigger("change");
            }

            loadCategory();
              loadImageThumb(data.data[0].thumbnail);

            if(data.data[0].album){
                loadMultipleMedia(JSON.parse(data.data[0].album));
            }

            $('#modal_form').modal('show');
            $('.modal-title').text(language['heading_title_edit']);
          window.history.pushState({path: current_url + '?edit=' + id}, '', current_url + '?edit=' + id);

        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(textStatus);
            console.log(jqXHR);
        }
    });
}

//form biên tập nội dung crawler
function editor_form(id) {
  slug_disable = false;
  save_method = 'add';
  loadCategory();
  //Ajax Load data from ajax
  $.ajax({
    url: url_ajax_edit + "/" + id,
    type: "GET",
    dataType: "JSON",
    success: function (data) {
      $.each(data.data, function (key, value) {
        $.each(value, function (k, v) {
          if ($.inArray(k, ['title', 'meta_title', 'description', 'meta_description', 'slug', 'content', 'meta_keyword']) !== -1) {
            $('[name="' + k + '[' + value.language_code + ']"]').val(v);
            if (v!=null && tinymce.get(k + '_' + value.language_code)) tinymce.get(k + '_' + value.language_code).setContent(v);
          } else {
            $('[name="' + k + '"]').val(v);
          }
        });
        $('[name="meta_keyword[' + value.language_code + ']"]').tagsinput('add', value.meta_keyword);
      });
      if (data.account_id) {
        $('select[name="account_id"]').select2({
          data: [data.account_id]
        });
        $('select[name="account_id"] > option').prop("selected", "selected").trigger("change");
      } else loadProfessional();
      //loadImageThumbApi(data.thumbnail);
      if (data.category_id) {
        loadCategory(data.category_id);
        $('select[name="category_id[]"] > option').prop("selected", "selected").trigger("change");
      }

      loadCategory();

      if (data.branch) {
        loadProperty('style_product', data.style_product);
        $('select[name="property[style_product][]"] > option').prop("selected", "selected").trigger("change");
      } else loadProperty('style_product');
      if (data.branch) {
        loadProperty('branch', data.branch);
        $('select[name="property[branch][]"] > option').prop("selected", "selected").trigger("change");
      } else loadProperty('branch');
      if (data.made_in) {
        loadProperty('made_in', data.made_in);
        $('select[name="property[made_in][]"] > option').prop("selected", "selected").trigger("change");
      } else loadProperty('made_in');
      if (data.unit) {
        loadProperty('unit', data.unit);
        $('select[name="property[unit][]"] > option').prop("selected", "selected").trigger("change");
      } else loadProperty('unit');
      if (data.material) {
        loadProperty('material', data.material);
        $('select[name="property[material][]"] > option').prop("selected", "selected").trigger("change");
      } else loadProperty('material');

      loadMultipleMediaApi(data.album);
      $('.fancybox').fancybox();
      $('#modal_form').modal('show');
      $('.modal-title').text("Biên tập nội dung");
      window.history.pushState({path: current_url + '?edit=' + id}, '', current_url + '?edit=' + id);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      alert(textStatus);
      console.log(jqXHR);
    }
  });
}


//ajax luu form
function save() {
    $('#btnSave').text(language['btn_saving']); //change button text
    $('#btnSave').attr('disabled', true); //set button disable
    var url;

    if (save_method == 'add') {
        url = url_ajax_add;
    } else {
        url = url_ajax_update;
    }

    for (var j = 0; j < tinyMCE.editors.length; j++) {
        var content = tinymce.get(tinyMCE.editors[j].id).getContent();
        $('#' + tinyMCE.editors[j].id).val(content);
    }
    // ajax adding data to database

    var data = new FormData();

//Form data
    var form_data = $('#form').serializeArray();
    $.each(form_data, function (key, input) {
        data.append(input.name, input.value);
    });

    $.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: "JSON",
        processData: false,
        contentType: false,
        success: function (data) {
            toastr[data.type](data.message);
            if (data.type === "warning") {
                $('span.text-danger').remove();
                $.each(data.validation, function (i, val) {
                    $('[name="' + i + '"]').after(val);
                })
            } else {
                $('#modal_form').modal('hide');
                reload_table();
            }
            $('#btnSave').text(language['btn_save']);
            $('#btnSave').attr('disabled', false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('#btnSave').text(language['btn_save']);
            $('#btnSave').attr('disabled', false);
        }
    });
}
function loadProfessional(dataSelected) {
    $('select[name="account_id"]').select2({
        allowClear: true,
        placeholder: "Vui lòng chọn nhà thầu",
        data: dataSelected,
        ajax: {
            url: url_ajax_load_professional,
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
}

function loadFilterCate() {
    $("select.filter_category").select2({
        allowClear: true,
        placeholder: 'Select an item',
        ajax: {
            url: url_ajax_load_category,
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true,
        }
    });
    $("select.filter_category").on('change', function () {
        // var id = $(this).val();
        // var dataCategory = {parent_id:id, category_id:id};
        // filterDatatables(dataCategory);
        getDataformFilter();
    });
}
function loadCategory(dataSelected) {
    $('#form select[name="category_id[]"]').select2({
        allowClear: true,
        placeholder: language['text_category'],
        multiple: true,
        data: dataSelected,
        ajax: {
            url: url_ajax_load_category,
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
}
function loadProperty(type, dataSelected) {
    $('select[name="property[' + type + '][]"]').select2({
        allowClear: true,
        placeholder: "Vui lòng chọn thuộc tính",
        multiple: true,
        width: '100%',
        data: dataSelected,
        ajax: {
            url: url_ajax_load_property + '/' + type,
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
}

function loadFilterSiteCrawlerNoData() {
  if(typeof url_ajax_load_not_data!='undefined'){
    $("select.filter_category_not_data").select2({
      allowClear: true,
      placeholder: 'Select an item',
      ajax: {
        url: url_ajax_load_not_data,
        dataType: 'json',
        delay: 250,
        processResults: function (data) {
          return {
            results: data
          };
        },
        cache: true,
      }
    });
    $("select.filter_category_not_data").on('change', function () {
      // var id = $(this).val();
      // var dataCategory = {parent_id:id, category_id:id};
      // filterDatatables(dataCategory);
      $('select.filter_category').val('');
      getDataformFilter();
    });
  }
}