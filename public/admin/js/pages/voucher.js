var generated = [],
  possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

$(function () {
  //load lang
  //load table ajax
  if (typeof page_type !== 'undefined') dataFilter = {page_type: page_type};
  init_data_table();

  //bind checkbox table
  init_checkbox_table();

  $(".generator").on("click", function (e) {
    generateCodes(1, 20);
    return false;
  });
});


//form them moi
function add_form() {
  slug_disable = false;
  save_method = 'add';
  $('.form-group').removeClass('has-error');
  $('#modal_form').modal('show');
}

//form sua
function edit_form(id) {
  slug_disable = true;
  save_method = 'update';
  $('.form-group').removeClass('has-error');
  $('.alert').empty();
  //Ajax Load data from ajax
  $.ajax({
    url: url_ajax_edit + "/" + id,
    type: "GET",
    dataType: "JSON",
    success: function (data) {
      $.each(data.post, function (k, v) {
        $('[name="' + k + '"]').val(v);
      });
      $('img#thumbnail').attr('src', media_url + data.post.thumbnail);
      $('#modal_form').modal('show');
      $('.modal-title').text(language['heading_title_edit']);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      $(".modal-body").prepend(box_alert('alert-danger', language['error_try_again']));
    }
  });
}

//ajax luu form
function save() {
  $('#btnSave').text(language['btn_saving']); //change button text
  $('#btnSave').attr('disabled', true); //set button disable
  var url;
  if (save_method == 'add') {
    url = url_ajax_add;
  } else {
    url = url_ajax_update;
  }
  // ajax adding data to database
  $.ajax({
    url: url,
    type: "POST",
    data: $('form').serialize(),
    dataType: "JSON",
    success: function (data) {
      if (data.type === "validate_error") {
        $(".modal-body").prepend(box_alert('alert-danger', data.message));
      } else {
        toastr[data.type](data.message);
        $('#modal_form').modal('hide');
        reload_table();
      }
      $('#btnSave').text(language['btn_save']); //change button text
      $('#btnSave').attr('disabled', false); //set button enable
    },
    error: function (jqXHR, textStatus, errorThrown) {
      $(".modal-body").prepend(box_alert('alert-danger', language['error_try_again']));
      $('#btnSave').text(language['btn_save']); //change button text
      $('#btnSave').attr('disabled', false); //set button enable

    }
  });
}

function generateCodes(number, length) {


  $(".generator_code").val(generateCode(length));
}

function generateCode(length) {
  var text = "";

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  var check = check_code(text);
  if (check == 1) {
    generateCode(length);
  } else {
    return text;
  }

}

function check_code(code) {
  var result;
  $.ajax({
    url: ajax_check_code,
    type: 'POST',
    async: true,
    data: {code: code},
    success: function (data) {
      result= data;
    }
  });
  return result;
}