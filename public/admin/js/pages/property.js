$(function () {
    //load lang
    load_lang('property');
    //load slug
    init_slug('title','slug');
    //load table ajax
    init_data_table(20);
    //bind checkbox table
    init_checkbox_table();
    //Bật kéo thả sắp xếp record
    updateSortDatatables();

});

//form them moi
function add_form() {
    slug_disable = false;
    save_method = 'add';
    /*Load category parent*/
    loadParentCategory();
    /*Load category parent*/
    $('#modal_form').modal('show');
}

//form sua
function edit_form(id)
{
    slug_disable = true;
    save_method = 'update';

    //Ajax Load data from ajax
    $.ajax({
        url : url_ajax_edit+"/"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $.each(data.data, function( key, value ) {
                $('[name="'+key+'"]').val(value);
            });
            loadImageThumb(data.data.thumbnail);
            $('#modal_form').modal('show');
            $('.modal-title').text(language['heading_title_edit']);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert(textStatus);
            console.log(jqXHR);
        }
    });
}

//ajax luu form
function save()
{
    $('#btnSave').text(language['btn_saving']); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url;

    if(save_method == 'add') {
        url = url_ajax_add;
    } else {
        url = url_ajax_update;
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data){
            toastr[data.type](data.message);
            if(data.type === "warning"){
                $('span.text-danger').remove();
                $.each(data.validation,function (i, val) {
                    $('[name="'+i+'"]').after(val);
                })
            } else {
                $('#modal_form').modal('hide');
                reload_table();
            }
            $('#btnSave').text(language['btn_save']);
            $('#btnSave').attr('disabled',false);
        }, error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('#btnSave').text(language['btn_save']);
            $('#btnSave').attr('disabled',false);
        }
    });
}

function loadParentCategory() {
    $('select[name="parent_id"]').select2({
        allowClear: true,
        placeholder: language['text_category'],
        ajax: {
            url: url_ajax_load,
            data: {id:$('input[name="id"]').val()},
            dataType: 'json',
            delay: 250,
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
}