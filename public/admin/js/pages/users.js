$(function () {

    //load table ajax
    init_data_table();
    //bind checkbox table
    init_checkbox_table();
});
//form them moi
function add_form()
{
    save_method = 'add';
    $('[name="username"]').removeAttr('disabled');
    $('[name="email"]').removeAttr('disabled');
    $('.help-component').empty();
    $('#modal_form').modal('show');
}

//form sua
function edit_form(id)
{
    save_method = 'update';
    $('.help-component').empty();

    //Ajax Load data from ajax
    $.ajax({
        url : url_ajax_edit+"/"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="username"]').val(data.username).attr('disabled',true);
            $('[name="email"]').val(data.email).attr('disabled',true);
            $('[name="first_name"]').val(data.first_name);
            $('[name="last_name"]').val(data.last_name);
            $('[name="company"]').val(data.company);
            $('[name="phone"]').val(data.phone);
            $('select[name="group_id"]').val(data.group_id);
            $('#modal_form').modal('show');
            $('.modal-title').text(language['heading_title_edit']);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $(".modal-body").prepend(box_alert('alert-danger',language['error_try_again']));
        }
    });
}

//ajax luu form
function save()
{
    $('#btnSave').text(language['btn_saving']); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url;

    if(save_method == 'add') {
        url = url_ajax_add;
    } else {
        url = url_ajax_update;
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            toastr[data.type](data.message);
          if(data.type === "warning"){
            $('span.text-danger').remove();
            // $.each(data.validation, function (i, val) {
            //     $('[name="' + i + '"]').after('<span class="text-danger">'+val +'</span>');
            // });
            if(data.validation){
              $(".modal-body").prepend(box_alert('alert-danger', data.validation));
            }else{
              $(".modal-body").prepend(box_alert('alert-danger', data.message));
            }
          } else {
            $('#modal_form').modal('hide');
            reload_table();
          }
            $('#btnSave').text(language['btn_save']); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
            $('#token').val(data.token);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            $('#btnSave').text(language['btn_save']); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
        }
    });
}
