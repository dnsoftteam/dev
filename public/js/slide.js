$(document).ready(function() {
    if ($('.slider-post').length > 0) {
        slickInitEvents();
    }
    $('.slide-home').owlCarousel({
        smartSpeed: 1000,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        margin: 0,
        dots: true,
        autoplayHoverPause: true,
        video: true,
        slideSpeed: 300,
        nav: false,
        items: 1,
        autoplay: true,
        autoplayTimeout: 5000,
        singleItem: true,
    });
    //slideTourVoucherHome
    $('.slide-tour-home').owlCarousel({
        loop: false,
        margin: 25,
        dots: true,
        nav: true,
        navText: ["<span class='arrow_carrot-left'></span>", "<span class='arrow_carrot-right'></span>"],
        navClass: ["slide-prev", "slide-next"],
        autoplay: true,
        autoplayTimeout: 5000,
        smartSpeed: 800,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
            },
            600: {
                items: 2,
                nav: true,
            },
            991: {
                items: 4,
                nav: true,
            },
            1025: {
                items: 4,
                nav: true,
                loop: false,
                margin: 25,
            }
        }
    });
    //slideTopDestinationHome
    $('.slideTopDestination').owlCarousel({
        loop: false,
        margin: 25,
        dots: true,
        nav: true,
        navText: ["<span class='arrow_carrot-left'></span>", "<span class='arrow_carrot-right'></span>"],
        navClass: ["slide-prev", "slide-next"],
        autoplay: true,
        autoplayTimeout: 5000,
        smartSpeed: 800,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: false,
            },
            600: {
                items: 2,
                nav: true,
            },
            991: {
                items: 4,
                nav: true,
            },
            1025: {
                items: 4,
                nav: true,
                loop: false,
                margin: 25,
            }
        }
    });
    //slide Feedback home
    $('.slideFeedback').owlCarousel({
        smartSpeed: 1000,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        margin: 0,
        dots: true,
        navText: ["<span class='arrow_carrot-left'></span>", "<span class='arrow_carrot-right'></span>"],
        navClass: ["slide-prev", "slide-next"],
        autoplayHoverPause: true,
        video: true,
        slideSpeed: 300,
        nav: true,
        items: 1,
        autoplay: true,
        autoplayTimeout: 5000,
        singleItem: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                dots: true,
                nav: false,
            }
        }
    });
    //slide partner Home
    $('.slidePartner').owlCarousel({
        loop: false,
        margin: 25,
        dots: false,
        nav: true,
        navText: ["<span class='arrow_carrot-left'></span>", "<span class='arrow_carrot-right'></span>"],
        navClass: ["slide-prev", "slide-next"],
        autoplay: true,
        autoplayTimeout: 5000,
        smartSpeed: 800,
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
                dots: true,
                nav: false,
            },
            600: {
                items: 3,
                dots: true,
            },
            991: {
                items: 4,
            },
            1025: {
                items: 6,
                margin: 25,
            }
        }
    });
    //slideBlogHot
    $('.slideBlogHot').owlCarousel({
        loop: false,
        margin: 25,
        dots: false,
        nav: true,
        navText: ["<span class='arrow_carrot-left_alt'></span>", "<span class='arrow_carrot-right_alt'></span>"],
        navClass: ["slide-blog-prev", "slide-blog-next"],
        autoplay: true,
        autoplayTimeout: 5000,
        smartSpeed: 800,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
                nav: true,
            },
            991: {
                items: 3,
                nav: true,
            },
            1025: {
                items: 3,
                nav: true,
                loop: false,
                margin: 25,
            }
        }
    });
    //slideImgTour
    $('.slideImgTour').owlCarousel({
        loop: false,
        margin: 12,
        dots: false,
        nav: true,
        navText: ["<span class='arrow_carrot-left_alt'></span>", "<span class='arrow_carrot-right_alt'></span>"],
        navClass: ["slide-blog-prev", "slide-blog-next"],
        autoplay: true,
        autoplayTimeout: 5000,
        smartSpeed: 800,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
                nav: true,
            },
            991: {
                items: 3,
                nav: true,
            },
            1025: {
                items: 4,
                nav: true,
                loop: false,
                margin: 12,
            }
        }
    });
    //slideImgpartner
    $('.slideImgPartner').owlCarousel({
        loop: false,
        margin: 20,
        dots: true,
        nav: false,
        navText: ["<span class='arrow_carrot-left_alt'></span>", "<span class='arrow_carrot-right_alt'></span>"],
        navClass: ["slide-blog-prev", "slide-blog-next"],
        autoplay: true,
        autoplayTimeout: 5000,
        smartSpeed: 800,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 2,
            },
            991: {
                items: 3,
            },
            1025: {
                items: 3,
                loop: false,
                margin: 20,
            }
        }
    });
});

function slickInitEvents() {
    $('.slider-post .slider-big').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-small',
        autoplay: false
    });
    $('.slider-post .slider-small').slick({
        slidesToShow: 8,
        slidesToScroll: 1,
        asNavFor: '.slider-big',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    centerMode: false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 769,
                settings: {
                    centerMode: false,
                    slidesToShow: 6,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: false,
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
}