var options = {
    //target:        '#output1',   // target element(s) to be updated with server response
    beforeSubmit:  showRequest,  // pre-submit callback
    success:       showResponse,  // post-submit callback
    type:      'POST',        // 'get' or 'post', override for form's 'method' attribute
    dataType:  'JSON'        // 'xml', 'script', or 'json' (expected server response type)
    //clearForm: true        // clear all form fields after successful submit
    //resetForm: true        // reset the form after successful submit
    // $.ajax options can be used here too, for example:
    //timeout:   3000
};

$(document).ready(function() {
    $('header nav>ul>li a[href="'+urlCurrent+'"]').parent().addClass('active');
    $('header nav>ul>li a[href="'+urlCurrentMenu+'"]').parent().addClass('active');
    $('.menu-product-cate > .container > ul li a[href="'+urlCurrentMenu+'"]').parent().addClass('active');

    $('input[name="search"]').keydown(function (e) {
        if (e.keyCode === 13) {
            var text = $('input[name="search"]').val();
            if(text.length > 0){
                location.href = base_url + "search/"+text;
            }else{
                toastr['warning']('Vui lòng nhập từ khóa để tìm kiếm !');
            }
        }
    });

    $('form[method="post"]').ajaxForm(options);

    $('.select2').select2();

  //compare
  if($('.item-tour').length>0){
    $('.item-tour .bottom-tour .compare').click(function(event) {
      event.preventDefault();
      let id=$(this).data('id');
      ajax_view_compare(id,this);
      $(this).prev().toggleClass('goIn');
    });
    $('.item-tour .bottom-tour').on('click','.close-popup',function(event) {
      $('.popup-compare-tour').remove().fadeOut(500);
    });
  }

  // Add tour
  $(document).on('click','.addCompare',function (e) {
    e.preventDefault();
    let id=$(this).data('id');
    addCompare(id,this);
  });
  $(document).on('focus','.search-banner .form-group input',function () {
    load_destinations(this);
  });
  $(document).on('focusout','.search-banner .form-group input',function () {
    $('.search-destinations.home.goIn').remove();
  });

  // $('.k-date-picker').datepicker({
  //   changeMonth: true,
  //   changeYear: true,
  //   showButtonPanel: true,
  //   dateFormat: 'm/yy',
  //   minDate:'m',
  //   onClose: function(dateText, inst) {
  //     var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
  //     var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
  //     $(this).datepicker('setDate', new Date(year, month, 1));
  //   }
  // });
  // chage_lang_jquery_ui();
});

function load_destinations(_this){
  $.ajax({
    url:base_url + 'home/load_destinations',
    type:'POST',
    success:function (data) {
      $(_this).parent().parent().append(data);
    },
  });
}
$(document).on('mouseenter', '[data-toggle="pill"]', function() {
  $('.tab-content.search-des .tab-pane').removeClass('show');
  $(this).tab('show');
});
$("#shareNative").jsSocials({
  showLabel: false,
  showCount: false,

  shares: [{
    renderer: function() {
      var $result = $("<div>");

      var script = document.createElement("script");
      script.text = "window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return t;js=d.createElement(s);js.id=id;js.src=\"https://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,\"script\",\"twitter-wjs\"));";
      $result.append(script);

      $("<a>").addClass("twitter-share-button")
        .text("Tweet")
        .attr("href", "https://twitter.com/share")
        .appendTo($result);

      return $result;
    }
  },{
    renderer: function() {
      var $result = $("<div>");

      var script = document.createElement("script");
      script.text = "(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = \"//connect.facebook.net/vi_EN/sdk.js#xfbml=1&version=v2.3\"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));";
      $result.append(script);

      $("<div>").addClass("fb-share-button")
        .attr("data-layout", "button_count")
        .appendTo($result);

      return $result;
    }
  }, {
    renderer: function() {
      var $result = $("<div>");

      var script = document.createElement("script");
      script.src = "https://apis.google.com/js/platform.js";
      $result.append(script);

      $("<div>").addClass("g-plus")
        .attr({
          "data-action": "share",
          "data-annotation": "bubble"
        })
        .appendTo($result);

      return $result;
    }
  },{
    renderer: function() {
      var $result = $("<div>");

      var script = document.createElement("script");
      script.src = "//assets.pinterest.com/js/pinit.js";
      $result.append(script);

      $("<a>").append($("<img>").attr("//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png"))
        .attr({
          href: "//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fjs-socials.com%2Fdemos%2F&media=%26quot%3Bhttp%3A%2F%2Fgdurl.com%2Fa653%26quot%3B&description=Next%20stop%3A%20Pinterest",
          //"data-pin-do": "buttonPin",
          //"data-pin-config": "beside",
          //"data-pin-color": "red"
        })
        .appendTo($result);
      return $result;
    }
  }]
});



function show_popup(timeStart){
    console.log(timeStart*100);
    if(!localStorage.getItem('is_load_popup')){
        setTimeout(function() {
            jQuery('#popup-event').modal({
                show:true,
                keyboard: false,
                backdrop: 'static',
            });
            localStorage.setItem('is_load_popup',true);
        }, timeStart*1000);
    }

}


// pre-submit callback
function showRequest(formData, jqForm, options) {
    var queryString = $.param(formData);
    console.log('About to submit: \n\n' + queryString);
    if($('.icon_load').length > 0){
      $('.icon_load').show();
    }
    return true;
}

// post-submit callback
function showResponse(response, statusText, xhr, $form)  {
    if(typeof response.type !== 'undefined'){
        toastr[response.type](response.message);
        if(response.type === "warning"){
            $form.find('span.text-danger').remove();
            $form.find('.form-control.is-invalid').removeClass('is-invalid');
            $.each(response.validation,function (key, val) {
                $form.find('[name="'+key+'"]').addClass('is-invalid').after(val);
            })
        }else{
          $form[0].reset();
            $form.find('span.text-danger').remove();
            $form.find('.form-control.is-invalid').removeClass('is-invalid');
        }
      if($('.icon_load').length > 0){
        $('.icon_load').hide();
      }
    }
    /*setTimeout(function () {
        location.reload();
    },3000);*/
    console.log('status: ' + statusText + '\n\nresponseText: \n' + response +
        '\n\nThe output div should have already been updated with the responseText.');
}

function shareByMail(title, body) {
    window.location.href = "mailto:" + "?subject=" + title + "&body=" + body;
}
function printPage() {
    window.print();
}

function panigation_review(element) {
  var button = $('#k_panigation');
  var id = button.attr('data-cat');
  var page_curent = button.attr('data-page');
  var page_total = button.attr('data-total');
  $.ajax({
    type: 'POST',
    url: ajax_load_panigation,
    dataType: 'html',
    data: {'cat': id, 'page': page_curent},
    beforeSend: function () {
      $('.icon_load').show();
    },
    success: function (res) {
      $('#'+element).append(res);
      $('.icon_load').hide();
      page_curent = parseInt(page_curent) + 1;
      button.attr('data-page', page_curent);
      if (page_total <= page_curent) {
        button.remove();
      }
    }
  });
  return false;
}

function loadDistrict(e) {
  $.getJSON(base_url + "public/quan_huyen.json", function (t) {
    var o = [];
    $.each(t, function (t, a) {
      a.parent_code === e && o.push("<option value='" + a.code + "'>" + a.name + "</option>")
    }), $('[name="district"]').html(o)
  })
}
function onchangeProvince() {
  loadDistrict($('[name="city"]').val())
}
function ajax_view_compare(id,_this) {
  $('.popup-compare-tour').remove();
  $.ajax({
    url:url_load_compase,
    type:'POST',
    dataType:'html',
    data:{id:id},
    success:function (data) {
      $(_this).parent().append(data);
    }
  });
}
function addCompare(id,_this) {
  $.ajax({
    url:url_add_compase,
    type:'POST',
    dataType:'json',
    data:{id:id},
    success:function (data) {
      toastr[data.type](data.message);
      if(data.type=='success'){
        ajax_load_compare();
        $(_this).removeClass('addCompare').html('Chi tiết').prop('href',base_url+'compare');

      }
    }
  });
}
function ajax_load_compare() {
  $.ajax({
    url:base_url + 'home/load_list_compare',
    type:'POST',
    dataType:'html',
    success:function (data) {
      $('#k-info-compare').html(data);
    }
  });
}
function search_tour(_this,element) {
  var href;
  if(typeof element!="undefined" && element=='date'){
    var val = $(_this).val();
    href=$(this).data('val');
    if(val!=''){
      window.location.href=href+val;
    }
  }else{
    href = $(_this).val();
    window.location.href=href;
  }


}

function chage_lang_jquery_ui() {
  $.datepicker.regional["vi-VN"] =
    {
      closeText: "Chọn",
      prevText: "Trước",
      nextText: "Sau",
      currentText: "Hôm nay",
      monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
      monthNamesShort: ["Một", "Hai", "Ba", "Bốn", "Năm", "Sáu", "Bảy", "Tám", "Chín", "Mười", "Mười một", "Mười hai"],
      dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
      dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
      dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
      weekHeader: "Tuần",
      dateFormat: "dd/mm/yy",
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ""
    };

  $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
}