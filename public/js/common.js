$(document).ready(function() {
    /**************** WOW js *****************/
    // Repeat demo content
    var $body = $('body');
    var $box = $('.box');
    for (var i = 0; i < 20; i++) {
        $box.clone().appendTo($body);
    }

    // Helper function for add element box list in WOW
    WOW.prototype.addBox = function(element) {
        this.boxes.push(element);
    };

    // Init WOW.js and get instance
    var wow = new WOW(
    {
        mobile: false
    });
    wow.init();

    // Attach scrollSpy to .wow elements for detect view exit events,
    // then reset elements and add again for animation
    $('.wow').on('scrollSpy:exit', function() {
        $(this).css({
            'visibility': 'hidden',
            'animation-name': 'none'
        }).removeClass('animated');
        wow.addBox(this);
    }).scrollSpy();
    //mmenu
    if ($('.icon-reponsive').length > 0) {
        $('.icon-reponsive').click(function(event) {
            event.preventDefault();
            $("#menu-mobile").mmenu({
                /*extensions: ['pagedim-black'],*/
            });
        });
    }
    $(window).scroll(function() {
        if ($(window).scrollTop() > 45) {
            $('header').addClass('scroll');
        } else {
            $('header').removeClass('scroll')
        }
    });
    //jssocials
    if($(".shareSocials").length>0){
        $(".shareSocials").jsSocials({
            showLabel: false,
            showCount: false,
            shares: ["facebook", "twitter", "googleplus", "pinterest", "linkedin"]
        });
    }
    //fancybox
    if ($(window).width() > 991 && $("[data-fancybox]").length > 0) {
        $("[data-fancybox]").fancybox({
            thumbs: {
                autoStart: true
            }
        });
    }
    //stick news-details
    if ($(window).width() > 991) {
        if ($('.sidebar-news,.list-voucher-hot,.sidebar-voucher,.sidebar-tour').length > 0) {
            $('.sidebar-news,.list-voucher-hot,.sidebar-voucher,.sidebar-tour').stick_in_parent({
                offset_top: 90
            });
        }
    }
    // 5 ly do
    $('.list-why-home .title').click(function(event) {
        $('.wrap-why').slideToggle();
    });
    //tour-details
    $('.navigation__link').bind('click', function(e) {
        e.preventDefault(); // prevent hard jump, the default behavior

        var target = $(this).attr("href"); // Set the target as variable
        var target = $(target).offset().top - 120;

        // perform animated scrolling by getting top-position of target-element and set it as scroll target
        $('html, body').stop().animate({
                scrollTop: target
        }, 600, function() {
                location.hash = target; //attach the hash (#jumptarget) to the pageurl
        });

        return false;
    });
    $(window).scroll(function() {
        if($('.menu-tour-details').length>0){
            if($(window).scrollTop() > $('.slide-top-details').offset().top - 88){
                $('.menu-tour-details').addClass('fix');  
            }else{
                $('.menu-tour-details').removeClass('fix');
            }
        }
    });
    //dat tour
    $(window).trigger('resize');
});
$(window).scroll(function() {
    var scrollDistance = $(window).scrollTop();

    // Show/hide menu on scroll
    //if (scrollDistance >= 850) {
    //      $('nav').fadeIn("fast");
    //} else {
    //      $('nav').fadeOut("fast");
    //}

    // Assign active class to nav links while scolling
    $('.page-section').each(function(i) {
        if ($(this).offset().top <= scrollDistance + 120) {
                $('.navigation a.active').removeClass('active');
                $('.navigation a').eq(i).addClass('active');
        }
    });
}).scroll();

function onchangePayment(){
    $('.box-info-bank').hide();
    var id=$('#option-payment').val();
    $('#select' + id).show();

}