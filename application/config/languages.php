<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 14/12/2017
 * Time: 4:47 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Supported Languages
|--------------------------------------------------------------------------
|
| Contains all languages your site will store data in. Other languages can
| still be displayed via language files, thats totally different.
|
| Check for HTML equivalents for characters such as � with the URL below:
|    http://htmlhelp.com/reference/html40/entities/latin1.html
|
*/
$config['cms_language'] = array('vi'=>'Tiếng Việt');
$config['cms_lang_cnf'] = array('vi'=>'vietnamese');
$config['default_language'] = 'vi';