<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 18/12/2017
 * Time: 4:20 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
$config['error_prefix'] = '<span class="text-danger">';
$config['error_suffix'] = '</span>';