<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = 'page/_404';
$route['translate_uri_dashes'] = FALSE;
$route['sitemap.xml'] = 'seo/sitemap';
$route['admin'] = 'admin/dashboard';

/*Route post*/
$route['(:any)-c(:num)']      = 'news/category/$2';
$route['(:any)-c(:num)/page/(:num)']   = 'news/category/$2/$3';
$route['(:any)-d(:num)'] ='news/detail/$2/';
/*Route post*/
/*Route customer*/
$route['khach-hang']      = 'customer/index';
$route['khach-hang/page/(:num)']      = 'customer/index/$1';
//Route customer en
$route['project']      = 'customer/index';
$route['project/page/(:num)']      = 'customer/index';

// voucher
$route['(:any)-cv(:num)']      = 'voucher/category/$2';
$route['(:any)-cv(:num)/page/(:num)']   = 'voucher/category/$2/$3';
$route['(:any)-dv(:num)'] ='voucher/detail/$2/';
/*voucher*/

// tour
$route['(:any)-ct(:num)']      = 'tour/category/$2';
$route['(:any)-ct(:num)/page/(:num)']   = 'tour/category/$2/$3';
$route['(:any)-dt(:num)'] ='tour/detail/$2/';

$route['book-tour'] ='tour/booktour';
$route['book-tour/(:num)'] ='tour/booktour/$1';

$route['search-tour']   = 'tour/search';

/*tour*/

/* Route search */
$route['search/(:any)']   = 'search/index/$1';
$route['search/(:any)/page/(:num)']   = 'search/index/$1/$2';
/* Route search*/

$route['compare']      = 'home/compare';

$route['(:any)']      = 'page/index/$1';
$route['(:any)/page/(:num)']      = 'page/index/$1/$2';
/*Route page*/