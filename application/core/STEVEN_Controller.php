<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 05/12/2017
 * Time: 4:24 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class STEVEN_Controller extends CI_Controller
{
  public $template_path = '';
  public $template_main = '';
  public $templates_assets = '';

  public function __construct()
  {
    parent::__construct();

    //Load library
    $this->load->library(array('session', 'form_validation'));
    $this->load->helper(array('data', 'security', 'url', 'directory', 'file', 'form', 'datetime', 'language', 'debug', 'numberics','format'));
    $this->config->load('languages');
    //Load file language
    $this->lang->load('general');
    //Load database
    $this->load->database();
    if (DEBUG_MODE == TRUE) {
      //Load third party
      $this->load->add_package_path(APPPATH . 'third_party', 'codeigniter-debugbar');
      $this->output->enable_profiler(TRUE);
    }
  }

  function switchLanguage($language)
  {
    $this->load->library('user_agent');
    if (!empty($language)) {
      $this->session->set_userdata('public_lang_code', $language);
      if ($this->agent->is_referral()) {
        redirect($this->agent->referrer());
      }
    }
  }

  public function toSlug($doc)
  {
    $str = addslashes(html_entity_decode($doc));
    $str = $this->toNormal($str);
    $str = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
    $str = preg_replace("/( )/", '-', $str);
    $str = str_replace('/', '', $str);
    $str = str_replace("\/", '', $str);
    $str = str_replace("+", "", $str);
    $str = strtolower($str);
    $str = stripslashes($str);
    return trim($str, '-');
  }

  public function toNormal($str)
  {
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);
    return $str;
  }

}

class Admin_Controller extends STEVEN_Controller
{

  public function __construct()
  {
    parent::__construct();

    //set đường dẫn template
    $this->template_path = 'admin/';
    $this->template_main = $this->template_path . '_layout';
    $this->templates_assets = base_url() . 'public/admin/';

    //fix language admin tiếng việt
    $this->session->admin_lang = 'vi';


    //tải thư viện
    $this->load->library(array('ion_auth', 'breadcrumbs'));
    //load helper
    $this->load->helper(array('authorization', 'image', 'link', 'text', 'button'));
    //Load config
    $this->config->load('seo');
    $this->config->load('permission');

    $configMinify['assets_dir'] = 'public/admin';
    $configMinify['assets_dir_css'] = 'public/admin/css';
    $configMinify['assets_dir_js'] = 'public/admin/js';
    $configMinify['css_dir'] = 'public/admin/css';
    $configMinify['js_dir'] = 'public/admin/js';
    $this->load->library('minify', $configMinify);
    $this->minify->enabled = FALSE;

    $this->check_auth();
  }

  // add log action
  public function addLogaction($action, $note)
  {
    $this->load->model("Log_action_model", "logaction");
    $data['action'] = $action;
    $data['note'] = $note;
    $data['uid'] = $this->session->user_id;
    $dates = "%Y-%m-%d %h:%i:%s";
    $time = time();

    $data['created_time'] = mdate($dates, $time);

    $this->logaction->save($data);
  }

  public function check_auth()
  {
    if (!$this->ion_auth->logged_in()) {
      //chưa đăng nhập thì chuyển về page login
      redirect(BASE_ADMIN_URL . 'auth/login?url=' . urlencode(current_url()), 'refresh');
    } else {
      if ($this->ion_auth->in_group(1) != true) {
        if (!$this->session->admin_permission) {
          $this->load->model('Groups_model', 'group');
          $groupModel = new Groups_model();
          $group = $groupModel->get_group_by_userid((int)$this->session->userdata('user_id'));
          $data = $groupModel->getById($group->group_id);
          if (!empty($data)) {
            $this->session->admin_permission = json_decode($data->permission, true);
            $this->session->admin_group_id = (int)$group->group_id;
          }
        }
        $controller = $this->router->fetch_class();
        if (!in_array($controller, array('dashboard'))) {
          if (!$this->session->admin_permission[$controller]['view']) {//check quyen view
            redirect('admin/dashboard/notPermission');
          }
        }
      } else {
        $this->session->admin_group_id = 1;//ID nhóm admin
//        redirect($this->input->get('url'));
      }
    }
  }

}

class Public_Controller extends STEVEN_Controller
{
  public $setting = array();
  public $_message = array();
  public $_controller;
  public $_method;
  protected $_category_tree;
  protected $_list_category_id_queue;
  protected $_listmenu;

  public function __construct()
  {
    parent::__construct();

    //set đường dẫn template
    $this->template_path = 'public/default/';
    $this->template_main = $this->template_path . '_layout';
    $this->templates_assets = base_url() . 'public/';

    //load cache driver
    $this->load->driver('cache', array('adapter' => 'file'));

    //tải thư viện
    $this->load->library(array('minify', 'cart', 'breadcrumbs'));

    //load helper
    $this->load->helper(array('cookie', 'navmenus', 'link', 'title', 'format', 'image', 'text', 'builder_query'));

    //Detect mobile
    //$this->detectMobile = new Mobile_Detect();

    //Language
    $lang_code = $this->input->get('lang');
    $lang_cnf = $this->config->item('cms_lang_cnf');
    //set session language
    if (!empty($lang_code) && array_key_exists($lang_code, $lang_cnf)) {
      $this->session->public_lang_code = $lang_code;
      $this->session->public_lang_full = $lang_cnf[$lang_code];
    }
    if (empty($this->session->public_lang_code)) {
      //không có lang code thì mặc định hiển thị tiếng việt
      $this->session->public_lang_code = 'vi';
      $this->session->public_lang_full = 'vietnamese';
    }

    $this->config->set_item('language', $this->session->public_lang_full);
    $this->lang->load(array('auth', 'ion_auth', 'frontend'));

//đọc file setting
    $dataSetting = file_get_contents(FCPATH . 'config' . DIRECTORY_SEPARATOR . 'settings.cfg');
    $dataSetting = $dataSetting ? json_decode($dataSetting, true) : array();
    if (!empty($dataSetting)) foreach ($dataSetting as $key => $item) {
      if ($key === 'meta') {
        $oneMeta = $item[$this->session->public_lang_code];
        if (!empty($oneMeta)) foreach ($oneMeta as $keyMeta => $value) {
          $this->settings[$keyMeta] = str_replace('"', '\'', $value);
        }
      } else
        $this->settings[$key] = $item;
    }
    //Set flash message
    $this->_message = $this->session->flashdata('message');
    if (MAINTAIN_MODE == TRUE) $this->load->view('public/coming_soon');
    $this->minify->enabled = FALSE;

    $this->_controller = $this->router->fetch_class();
    $this->_method = $this->router->fetch_method();
  }

//Query list category & children category
  public function _query_category_id($categories, $parent_id = 0)
  {
    $this->_list_category_id_queue[] = $parent_id;
    if (!empty($categories)) foreach ($categories as $key => $item) {
      if ($item->parent_id == $parent_id) {
        $this->_list_category_id_queue[] = $item->id;
        unset($categories[$key]);
        $this->_query_category_id($categories, $item->id);
      }
    }
  }

  public function submitContact()
  {
    if ($this->input->server('REQUEST_METHOD') == 'POST') {
      $this->load->library('email');
      $emailTo = $this->settings['contact'][$this->session->public_lang_code]['email'];
      $emailToCC = 'steven.mucian@gmail.com';
      $emailToBCC = '';


      $this->form_validation->set_rules('email', 'email', 'trim|required');
      $this->form_validation->set_rules('title', $this->lang->line('form_text_title'), 'trim|required');
      $this->form_validation->set_rules('content', $this->lang->line('form_text_content'), 'trim|required');
      $this->form_validation->set_rules('g-recaptcha-response', 'captcha', 'required');
      if ($this->form_validation->run() == true) {

        //Check config setting reCaptcha
        $this->load->library('recaptcha');
        $captchaResponse = $this->input->post('g-recaptcha-response');
        $remoteIp = $this->input->ip_address();
        if ($this->recaptcha->verify($captchaResponse, $remoteIp) == false) {
          $message['type'] = 'error';
          $message['message'] = "Bạn chưa xác thực Captcha !";
          if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            die(json_encode($message));
          } else {
            $this->session->set_flashdata('message', $message);
            redirect($this->uri->uri_string(), 'refresh');
          }

        }
        //Check config setting reCaptcha


        $emailFrom = $this->input->post('email');
        $nameFrom = $this->input->post('fullname');
        //$phone = $this->input->post('phone');
        $title = $this->input->post('title');
        $content = $this->input->post('content');

        $contentHtml = '
                <h2>Dear ' . $this->settings['name'] . ' !</h2></br>
                <p>Họ và tên: ' . $nameFrom . '</p>
                <p>Nội dung: ' . $content . '</p>
            ';
        $this->email->from($emailFrom, $nameFrom);

        $this->email->to($emailTo);
        if (!empty($emailToCC)) $this->email->cc($emailToCC);
        if (!empty($emailToBCC)) $this->email->bcc($emailToBCC);

        $this->email->subject($title);
        $this->email->message($contentHtml);

        if ($this->email->send()) {
          $message['type'] = 'success';
          $message['message'] = "Gửi thông tin liên hệ thành công !";
          $this->session->set_flashdata('message', $message);
        } else {
          //dd($this->email->print_debugger(array('headers')));
          $message['type'] = 'error';
          $message['message'] = 'Gửi thông tin liên hệ thất bại !';
          $this->session->set_flashdata('message', $message);
        }
        redirect(current_url(), 'refresh');
      }
    }
  }

  public function sendMailCheckout($data)
  {
    /*Config setting*/
    $config['protocol'] = $this->settings['protocol'];
    $config['smtp_host'] = $this->settings['smtp_host'];
    $config['smtp_user'] = $this->settings['smtp_user'];
    $config['smtp_pass'] = $this->settings['smtp_pass'];
    $config['smtp_port'] = $this->settings['smtp_port'];
    $this->load->library('email', $config);
    $emailTo = $data['email']; //Send mail cho khach hang
    $emailToCC = $this->settings['email_admin']; //Send mail cho ban quan tri

    $emailFrom = $emailToCC;
    $nameFrom = "Thông báo đơn hàng mới - " . $data['order_id'];

    $address = $data['address'];
    $phone = $data['phone'];

    $contentHtml = '
            <h2>Đơn hàng mới được tạo !</h2>
            <p>Họ và tên: ' . $nameFrom . '</p>
            <p>Số điện thoại: ' . $phone . '</p>
            <p>Địa chỉ: ' . $address . '</p>
            <p>Đơn đặt hàng mã: ' . $data['order_id'] . '</p>
        ';

    $this->email->from($emailFrom, $nameFrom);

    $this->email->to($emailTo);
    if (!empty($emailToCC)) $this->email->cc($emailToCC);
    if (!empty($emailToBCC)) $this->email->bcc($emailToBCC);

    $this->email->subject('Thông báo đơn hàng mới từ ' . base_url());
    $this->email->message($contentHtml);

    if ($this->email->send()) {
      $message['type'] = 'success';
      $message['message'] = "Gửi thông tin đặt hàng về mail thành công !";
      $this->session->set_flashdata('message', $message);
    } else {
      $this->email->print_debugger(array('headers'));
      $message['type'] = 'error';
      $message['message'] = 'Gửi thông tin đặt hàng mail thất bại !';
      $this->session->set_flashdata('message', $message);
    }
  }
}

class API_Controller extends CI_Controller
{
  const RESPONSE_SUCCESS = 200;
  const RESPONSE_EXIST = 201;
  const RESPONSE_REQUEST_ERROR = 400;
  const RESPONSE_LOGIN_ERROR = 401;
  const RESPONSE_LOGIN_DENIED = 403;
  const RESPONSE_NOT_EXIST = 404;
  const RESPONSE_LIMITED = 406;
  const RESPONSE_SERVER_ERROR = 500;
  const SECRET_KEY = 'c9c5afb32cd115d8edbc773c644e30ee';

  public function __construct()
  {
    parent::__construct();
    $this->auth();
    $this->load->database();
    $this->load->library(array('session', 'form_validation'));
    $this->load->helper(array( 'security', 'url','form','debug'));

  }

  public function auth()
  {
    $dataPOST = $this->input->request_headers();

    $checksum=md5('admin'.md5('admin123').self::SECRET_KEY.'2018');

    if (empty($dataPOST['Username'])
      || empty($dataPOST['Password'])
      || empty($dataPOST['Time'])
      || empty($dataPOST['Checksum'])
      || md5($dataPOST['Username'] . md5($dataPOST['Password']) . self::SECRET_KEY . $dataPOST['Time']) != $dataPOST['Checksum']) {
      $response['code'] = 400;
      $response['message'] = 'Bad request';
      die(json_encode($response));
    }
  }

}