<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 05/12/2017
 * Time: 4:24 CH
 */
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Smart model.
 */
class STEVEN_Model extends CI_Model
{
    public $table;
    public $table_trans;
    public $table_category;
    public $primary_key;
    public $column_order;
    public $column_search;
    public $order_default;

    public function __construct()
    {
        parent::__construct();
        $this->table            = str_replace('_model','',get_Class($this));
        $this->primary_key      = "id";
        $this->column_order     = array("$this->table.id","$this->table.id","$this->table_trans.title","$this->table.is_status","$this->table.updated_time","$this->table.created_time"); //thiết lập cột sắp xếp
        $this->column_search    = array("$this->table_trans.title"); //thiết lập cột search
        $this->order_default    = array("$this->table.created_time" => "DESC"); //cột sắp xếp mặc định

        //load cache driver
        $this->load->driver('cache',array('adapter'=>'file', 'backup' => 'file'));
    }

    /*Hàm xử lý các tham số truyền từ Datatables Jquery*/
    public function _get_datatables_query()
    {
        if(!empty($this->input->post('columns'))) {
            $i = 0;
            foreach ($this->column_search as $item) // loop column
            {
                if ($this->input->post('search')['value']) // if datatable send POST for search
                {
                    if ($i === 0) // first loop
                    {
                        $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                        $this->db->like($item, $this->input->post('search')['value']);
                    } else {
                        $this->db->or_like($item, $this->input->post('search')['value']);
                    }

                    if (count($this->column_search) - 1 == $i) //last loop
                        $this->db->group_end(); //close bracket
                }
                $i++;
            }

            if ($this->input->post('Product')){
                $this->db->order_by($this->column_order[$this->input->post('Product')['0']['column']], $this->input->post('Product')['0']['dir']);
            } else if (isset($this->order_default)) {
                $order = $this->order_default;
                $this->db->order_by(key($order), $order[key($order)]);
            }
        }
    }

    /*
     * Lấy tất cả dữ liệu
     * */
    public function getAll($lang_code = null){
        $this->db->from($this->table);
        if(!empty($this->table_trans)) $this->db->join($this->table_trans,"$this->table.id = $this->table_trans.id");
        if(!empty($lang_code)) $this->db->where('language_code',$lang_code);
        $query = $this->db->get();
        return $query->result();
    }



    /*
     * Đếm tổng số bản ghi
     * */
    public function getTotalAll()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    //Xử lý tham số truyền vào. Tham số truyền vào phải dạng Array
    public function _where($args, $typeQuery = null){
        $select = "*";
        //$lang_code = $this->session->admin_lang; //Mặc định lấy lang của Admin
        $page = 1; //Page default
        $limit = 10;

        extract($args);
        //$this->db->distinct();
        $this->db->select($select);
        $this->db->from($this->table);

        if($this->table === 'product') $this->db->select('IF(`price_sale` = 0, `price`, `price_sale`) AS `price_sort`');

        if(!empty($this->table_trans)){
            $this->db->join($this->table_trans, "$this->table.id = $this->table_trans.id");
            if(empty($lang_code)) $lang_code = $this->session->admin_lang;
            if(!empty($lang_code)) $this->db->where("$this->table_trans.language_code",$lang_code);
        }

        if(!empty($this->table_category) && !empty($category_id)){
            $nameModel = str_replace('_model','',$this->table);
            $this->db->join($this->table_category, "$this->table.id = $this->table_category.{$nameModel}_id");
            $this->db->where_in("$this->table_category.category_id", $category_id);
        }
        /*Lọc các thuộc tính của sản phẩm*/
        if(isset($filter_price_min) && !empty($filter_price_max)){
            $this->db->where("IF(`price_sale` != 0, `price_sale`, `price`) >=",$filter_price_min);
            $this->db->where("IF(`price_sale` != 0, `price_sale`, `price`) <=",$filter_price_max);
        }


        if(!empty($filter_type))
            $this->db->where_in("$this->table.type",$filter_type);

        /*Lọc các thuộc tính của sản phẩm*/
        if (isset($parent_id) && $parent_id!=='')
            $this->db->where("$this->table.parent_id",$parent_id);

        if (!empty($position_id))
            $this->db->where("$this->table.position_id",$position_id);

        if(!empty($category_type))
            $this->db->where("$this->table.type",$category_type);


        if(isset($page_type))
            $this->db->where("$this->table.style",$page_type);

        if(isset($page_type_not))
            $this->db->where("$this->table.style !=",$page_type_not);

        if (!empty($is_featured))
            $this->db->where("$this->table.is_featured",$is_featured);


        if (!empty($is_status))
            $this->db->where("$this->table.is_status",$is_status);

        if (!empty($category_id_page))
            $this->db->where("$this->table.category_id",$category_id_page);

        if (!empty($in))
            $this->db->where_in("$this->table.id",$in);

        if (!empty($or_in))
            $this->db->or_where_in("$this->table.id",$or_in);

        if (!empty($not_in))
            $this->db->where_not_in("$this->table.id",$not_in);

        if (!empty($or_not_in))
            $this->db->or_where_not_in("$this->table.id",$or_not_in);
        if(!empty($order_by))
          $this->db->order_by("$this->table.".$order_by,'DESC');
        $this->db->group_by("$this->table.id");
        //query for datatables jquery
        $this->_get_datatables_query();

        if(!empty($search)){
            $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
            if(!empty($this->column_search)) foreach ($this->column_search as $field) $this->db->like($field, $search);
            else $this->db->like("$this->table_trans.title", $search);
            $this->db->group_end(); //close bracket
        }

        if($typeQuery === null){
            if(!empty($order) && is_array($order)){
                foreach ($order as $k => $v)
                    $this->db->order_by($k, $v);
            } else if(isset($this->order_default)) {
                $order = $this->order_default;
                $this->db->order_by(key($order), $order[key($order)]);
            }
            $offset = ($page-1)*$limit;
            $this->db->limit($limit,$offset);
        }
    }

    public function getTotal($args = []){
        $this->_where($args, "count");
        $query = $this->db->get();
        //ddQuery($this->db);
        return $query->num_rows();
    }

    public function getData($args = array(),$returnType = "object"){
        $this->_where($args);
        $query = $this->db->get();
        //ddQuery($this->db);
        if($returnType !== "object") return $query->result_array(); //Check kiểu data trả về
        else return $query->result();
    }

    /*
     * Lấy dữ liệu một hàng ra
     * Truyền vào id
     * */
    public function getById($id,$select='*',$lang_code = null){

        $this->db->select($select);
        $this->db->from($this->table);
        if(!empty($this->table_trans)) $this->db->join($this->table_trans,"$this->table.id = $this->table_trans.id");
        $this->db->where("$this->table.id",$id);
        if(empty($this->table_trans)){
            $query = $this->db->get();
            return $query->row();
        }

        if(!empty($lang_code)){
            $this->db->where("$this->table_trans.language_code",$lang_code);
            $query = $this->db->get();
            return $query->row();
        }else{
            $query = $this->db->get();
            return $query->result();
        }
    }


    public function checkExistByField($field, $value, $tablename = ''){
        $this->db->select('1');
        if ($tablename == '') {
            $tablename = $this->table;
        }
        $this->db->from($tablename);
        $this->db->where($field, $value);
        return $this->db->count_all_results() > 0 ? true : false;
    }

    public function getSelect2($ids){
        $this->db->select("$this->table.id, $this->table_trans.title AS text");
        $this->db->from($this->table);
        if(!empty($this->table_trans)) $this->db->join($this->table_trans,"$this->table.id = $this->table_trans.id");
        if(is_array($ids)) $this->db->where_in("$this->table.id",$ids);
        else $this->db->where("$this->table.id",$ids);
        $this->db->where("$this->table_trans.language_code",$this->session->admin_lang);
        $query = $this->db->get();
        //ddQuery($this->db);
        return $query->result();
    }


    public function save($data, $tablename = '')
    {
        if ($tablename == '') {
            $tablename = $this->table;
        }
        $data_store = array();
        if(!empty($data)) foreach ($data as $k=>$item){
            if(!is_array($item)) $data_store[$k] = $item;
        }

        if(!$this->db->insert($tablename, $data_store)){
            log_message('info',json_encode($data_store));
            log_message('error',json_encode($this->db->error()));
            return false;
        }else {
            $id = $this->db->insert_id();

            /*Xử lý bảng category nếu có*/
            if(!empty($this->table_category) && !empty($data['category_id']) && is_array($data['category_id'])){
                $dataCategory = $data['category_id'];
                if(!empty($dataCategory)) foreach ($dataCategory as $item){
                    $tmpCategory[$this->table."_id"] = $id;
                    $tmpCategory["category_id"] = $item;
                    if(!$this->insert($tmpCategory, $this->table_category)) return false;
                    unset($tmpCategory);
                }
            }
            if(isset($data['category_id'])) unset($data['category_id']);

            /*Xử lý bảng category nếu có*/
            if(!empty($this->table_trans)){
                //thêm vào bảng product_translation
                foreach ($this->config->item('cms_language') as $lang_code => $lang_name) {
                    $data_trans = array();
                    $data_trans['id'] = $id;
                    $data_trans['language_code'] = $lang_code;
                    foreach ($data as $k => $item) {
                        if($lang_code !== 'vi') $lang_code_value = 'vi';
                        else $lang_code_value = $lang_code;
                        if (is_array($item)) {
                            if($k === 'title' || $k === 'meta_title') $data_trans[$k] = !empty($item[$lang_code_value]) ? addslashes($item[$lang_code_value]) : '';
                            else if(is_array($item[$lang_code_value])) $data_trans[$k] = !empty($item[$lang_code_value]) ? json_encode($item[$lang_code_value]) : '';
                            $data_trans[$k] = !empty($item[$lang_code_value]) ? $item[$lang_code_value] : '';
                        }
                        if(isset($item[$lang_code_value]) && is_array($item[$lang_code_value])) $data_trans[$k] = !empty($item[$lang_code_value]) ? json_encode($item[$lang_code_value]) : '';
                    }
                    if (!$this->db->insert($this->table_trans, $data_trans)) {
                        log_message('info', json_encode($data_trans));
                        log_message('error', json_encode($this->db->error()));
                        return false;
                    }
                }
            }
            $this->cache->clean();
            return $id;
        }
    }



    public function search($conditions = null, $limit = 500, $offset = 0, $tablename = '')
    {
        if ($tablename == '') {
            $tablename = $this->table;
        }
        if ($conditions != null) {
            $this->db->where($conditions);
        }

        $query = $this->db->get($tablename, $limit, $offset);

        return $query->result();
    }

    public function single($conditions, $tablename = '')
    {
        if ($tablename == '') {
            $tablename = $this->table;
        }
        $this->db->where($conditions);

        return $this->db->get($tablename)->row();
    }

    public function insert($data, $tablename = '')
    {
        if ($tablename == '') {
            $tablename = $this->table;
        }
        $this->db->insert($tablename, $data);
        $this->cache->clean();
        return $this->db->affected_rows();
    }

    public function insertOnUpdate($data, $tablename = '')
    {
        if ($tablename == '') {
            $tablename = $this->table;
        }
        $data_update = [];
        if(!empty($data)) foreach ($data as $k => $val){
            $data_update[] = $k." = '".$val."'";
        }

        $queryInsertOnUpdate = $this->db->insert_string($tablename, $data)." ON DUPLICATE KEY UPDATE ".implode(', ', $data_update);
        if(!$this->db->query($queryInsertOnUpdate)){
            log_message('info',json_encode($data));
            log_message('error',json_encode($this->db->error()));
            return false;
        }
        $this->cache->clean();
        return $this->db->affected_rows();
    }

    public function update($conditions, $data, $tablename = '')
    {
        if ($tablename == '') {
            $tablename = $this->table;
        }

        $dataInfo = [];
        if(!empty($data)) foreach ($data as $key => $value){
            if(!is_array($value)) {
                $dataInfo[$key] = $value;
                unset($data[$key]);
            }
        }
        if(!$this->db->update($tablename, $dataInfo, $conditions)){
            log_message('info',json_encode($conditions));
            log_message('info',json_encode($data));
            log_message('error',json_encode($this->db->error()));
            return false;
        }
        /*Xử lý bảng category nếu có*/
        if(!empty($this->table_category) && !empty($data['category_id']) && is_array($data['category_id'])){
            $dataCategory = $data['category_id'];
            $tmpCategory[$this->table."_id"] = $conditions['id'];
            $this->delete($tmpCategory, $this->table_category);
            if(!empty($dataCategory)) foreach ($dataCategory as $item){
                $tmpCategory["category_id"] = $item;
                if(!$this->insert($tmpCategory, $this->table_category)) return false;
            }
            unset($data['category_id']);
        }
        /*Xử lý bảng category nếu có*/
        if(!empty($this->table_trans)){
            if($this->config->item('cms_language')) foreach ($this->config->item('cms_language') as $lang_code => $lang_name){
                $data_trans = array();
                $data_update = array();
                foreach ($data as $k=>$item){
                    if(is_array($item)) {
                        if($k === 'title' || $k === 'meta_title') {
                            $data_trans[$k] = !empty($item[$lang_code]) ? addslashes($item[$lang_code]) : '';
                            $data_update[] = $k." = '".addslashes($item[$lang_code])."'";
                        } else if(is_array($item[$lang_code])) {
                            $data_trans[$k] = !empty($item[$lang_code]) ? json_encode($item[$lang_code]) : '';
                            $data_update[] = $k." = '".json_encode($item[$lang_code])."'";
                        }else{
                            $data_trans[$k] = !empty($item[$lang_code]) ? $item[$lang_code] : '';
                            $data_update[] = $k." = '".$item[$lang_code]."'";
                        }
                    }
                }
                //dd($data_trans);
                if(!empty($data_trans)){
                    $where_trans = array('id'=>$conditions['id'],'language_code'=>$lang_code);
                    if(!$this->db->update($this->table_trans, $data_trans, $where_trans)){
                        log_message('info',json_encode($data_trans));
                        log_message('error',json_encode($this->db->error()));
                        return false;
                    }
                    /*$where_trans = array('id'=>$conditions['id'],'language_code'=>$lang_code);
                    $data_trans = array_merge($where_trans,$data_trans);
                    $queryInsertOnUpdate = $this->db->insert_string($this->table_trans, $data_trans)." ON DUPLICATE KEY UPDATE id = {$conditions['id']}, language_code = '{$lang_code}', ".implode(", ", $data_update);
                    echo $queryInsertOnUpdate;
                    if(!$this->db->query($queryInsertOnUpdate)){
                        log_message('info',json_encode($where_trans));
                        log_message('info',json_encode($data_trans));
                        log_message('error',json_encode($this->db->error()));
                        return false;
                    }*/
                }
            }
        }
        $this->cache->clean();
        return true;
    }


    public function delete($conditions, $tablename = '')
    {
        if ($tablename == '') {
            $tablename = $this->table;
        }
        $this->db->where($conditions);
        if(!$this->db->delete($tablename)){
            log_message('info',json_encode($conditions));
            log_message('info',json_encode($tablename));
            log_message('error',json_encode($this->db->error()));
        }
        $this->cache->clean();
        return $this->db->affected_rows();
    }

    public function count($conditions = null, $tablename = '')
    {
        if ($conditions != null) {
            $this->db->where($conditions);
        }

        if ($tablename == '') {
            $tablename = $this->table;
        }

        $this->db->select('1');
        return $this->db->get($tablename)->num_rows();
    }

    public function getpostAllLang($id){
        $this->db->distinct();
        $this->db->select(['slug','language_code']);
        $this->db->from($this->table_trans);
        $this->db->where('id',$id);
        $query= $this->db->get()->result();
        return $query;
    }
}
