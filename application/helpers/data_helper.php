<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('_getCatById')) {
  function _getCatById($cateId)
  {
    $_this =& get_instance();
    $_this->load->model('category_model');
    $catModel = new Category_model();
    if (!$_this->cache->get('_all_category_' . $_this->session->public_lang_code)) {
      $_this->cache->save('_all_category_' . $_this->session->public_lang_code, $_this->_data_category->getAll($_this->session->public_lang_code), 60 * 60 * 30);
    }
    $_all_category = $_this->cache->get('_all_category_' . $_this->session->public_lang_code);
    $data = $catModel->getByIdCached($_all_category, $cateId);
    return $data;
  }

}
if (!function_exists('getCatByParent')) {
  function getCatByParent($cateId, $type)
  {
    $_this =& get_instance();
    $_this->load->model('category_model');
    $catModel = new Category_model();
    $params = array(
      'parent_id' => $cateId,
      'is_status' => 1,
      'type' => $type,
      'limit' => 8,
      'category_type' => $type
//      'langeague_code'=>$_this->session->public_lang_code
    );
    $data = $catModel->getData($params);
    return $data;
  }
}

if (!function_exists('getCatTourFeatured')) {
  function getCatTourFeatured()
  {
    $_this =& get_instance();
    $_this->load->model('category_model');
    $catModel = new Category_model();
    $params = array(
      'is_status' => 1,
      'limit' => 16,
      'category_type' => 'national',
      'is_featured' => 1
    );
    $data = $catModel->getData($params);
    return $data;
  }
}

if (!function_exists('getPageById')) {
  function getPageById($cateId)
  {
    $_this =& get_instance();
    $_this->load->model('page_model');
    $postModel = new Page_model();
    $data = $postModel->getById($cateId, '*', $_this->session->public_lang_code);
    return $data;
  }
}
if (!function_exists('getPageById')) {
  function getPageById($cateId)
  {
    $_this =& get_instance();
    $_this->load->model('page_model');
    $pageModel = new Page_model();
    $data = $pageModel->getById($cateId, '*', $_this->session->public_lang_code);
    return $data;
  }
}

if (!function_exists('getListCatCustomer')) {
  function getListCatCustomer($type = 'post')
  {
    $_this =& get_instance();
    $_this->load->model('category_model');
    $pageModel = new Category_model();
    $data = $pageModel->getAllCategoryByType($_this->session->public_lang_code, $type, '');
    return $data;
  }

}
if (!function_exists('getCatByIdTour')) {
  function getCatByIdTour($id)
  {
    $_this = get_instance();
    $_this->load->model('tour_model');
    $tourModel = new Tour_model();
    $data = $tourModel->getCategoryByPostId($id, $_this->session->public_lang_code);
    return $data;
  }
}
if (!function_exists('getTourById')) {
  function getTourById($id)
  {
    $_this = get_instance();
    $_this->load->model('tour_model');
    $tourModel = new Tour_model();
    $data = $tourModel->getById($id, '', $_this->session->public_lang_code);
    return $data;
  }
}
if (!function_exists('getCatByIdVoucher')) {
  function getCatByIdVoucher($id)
  {
    $_this = get_instance();
    $_this->load->model('voucher_model');
    $tourModel = new Voucher_model();
    $data = $tourModel->getCategoryByPostId($id, $_this->session->public_lang_code);
    return $data;
  }
}
if (!function_exists('showRatting')) {
  function showRatting($diem)
  {
    for ($i = 1; $i <= 5; $i++) {
      if ($i <= $diem) {
        echo '<i class="icon_star active"></i>';
      } else {
        echo '<i class="icon_star"></i>';
      }
    }
  }
}
if (!function_exists('getCatVoucher')) {
  function getCatVoucher()
  {
    $_this = get_instance();
    $_this->load->model('category_model');
    $catmodel = new Category_model();
    $params = array(
      'limit' => 6,
      'category_type' => 'voucher',
      'order_by' => 'is_featured',
      'is_status' => 1
    );
    $data = $catmodel->getData($params);
    return $data;
  }
}
if (!function_exists('getVoucherFeatured')) {
  function getVoucherFeatured()
  {
    $_this = get_instance();
    $_this->load->model('voucher_model');
    $vouchermodel = new Voucher_model();
    $params = array(
      'limit' => 17,
      'is_status' => 1,
      'is_featured' => 1
    );
    $data = $vouchermodel->getDataVoucher($params);
    return $data;
  }
}
if (!function_exists('getTag')) {
  function getTag($keyword)
  {
    $html = "";
    $list_tag = explode(',', $keyword);
    foreach ($list_tag as $key => $value) {
      $html .= "<a href=" . getUrlTag($value) . ">" . $value . "</a>, ";
    }
    return trim($html, ', ');
  }
}
if (!function_exists('getProvince')) {
  function getProvince()
  {
    $_this =& get_instance();
    $_this->load->model('location_model');
    $data = new Location_model();
    $province = $data->loadCity();
    return $province;
  }
}
if (!function_exists('getDistrictById')) {
  function getDistrictById($id)
  {
    $_this =& get_instance();
    $_this->load->model('location_model');
    $data = new Location_model();
    $district = $data->loadDistrict($id);
    return $district;
  }
}
if (!function_exists('getPostById')) {
  function getPostById($id)
  {
    $_this =& get_instance();
    $_this->load->model('post_model');
    $postModel = new Post_model();
    $data = $postModel->getById($id,'',$_this->session->public_lang_code);
    return $data;
  }
}
