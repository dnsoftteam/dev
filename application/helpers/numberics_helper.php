<?php
/**
 * Created by PhpStorm.
 * User: askeyh3t
 * Date: 11/17/2017
 * Time: 10:07 AM
 */

 if (!defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('getNumberics')){
    function getNumberics($arr){
        $arrnews=end($arr);
        $arrnews=key($arr);

        preg_match_all('/\d+/', $arrnews, $matches);
        return (int)end($matches[0]);
    }
}
