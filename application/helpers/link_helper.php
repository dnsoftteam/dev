<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('getUrlCateNews')) {
    function getUrlCateNews($optional){

      if(is_object($optional)) $optional = (array) $optional;
        $id = $optional['id'];
        if(!empty($optional['slug'])){
          $slug = $optional['slug'];

        }else{
          $_this =& get_instance();
          $_this->load->model('category_model');
          $categoryModel=new Category_model();
          $dataSlug=$categoryModel->getUrl($id);
          if(!empty($dataSlug)){
            $slug=$dataSlug->slug;
          }else{
            $slug='';
          }
        }
        $linkReturn = BASE_URL;
        $linkReturn .= "$slug-c$id";
        if(isset($optional['page'])) $linkReturn .= '/page/';
        return $linkReturn;
    }
}

if (!function_exists('getUrlCateCustomer')) {
    function getUrlCateCustomer($optional){
        if(is_object($optional)) $optional = (array) $optional;
        $id = $optional['id'];
        $slug = $optional['slug'];
        $linkReturn = BASE_URL;
        $linkReturn .= "$slug-cl$id";
        if(isset($optional['page'])) $linkReturn .= '/page/';
        return $linkReturn;
    }
}
if (!function_exists('getUrlCatTour')) {
    function getUrlCatTour($optional){
        if(is_object($optional)) $optional = (array) $optional;
        $id = $optional['id'];
        $slug = $optional['slug'];
        $linkReturn = BASE_URL;
        $linkReturn .= "$slug-ct$id";
        if(isset($optional['page'])) $linkReturn .= '/page/';
        return $linkReturn;
    }
}
if (!function_exists('getUrlCatVoucher')) {
    function getUrlCatVoucher($optional){
        if(is_object($optional)) $optional = (array) $optional;
        $id = $optional['id'];
        $slug = $optional['slug'];
        $linkReturn = BASE_URL;
        $linkReturn .= "$slug-cv$id";
        if(isset($optional['page'])) $linkReturn .= '/page/';
        return $linkReturn;
    }
}


if (!function_exists('getUrlNews')) {
    function getUrlNews($optional){
        if(is_object($optional)) $optional = (array) $optional;
        $id = $optional['id'];
        $slug = $optional['slug'];
        $linkReturn = BASE_URL;
        $linkReturn .= "$slug-d$id";
        if(isset($optional['page'])) $linkReturn .= '/page/';
        return $linkReturn;
    }
}

if (!function_exists('getUrlTour')) {
    function getUrlTour($optional){
        if(is_object($optional)) $optional = (array) $optional;
        $id = $optional['id'];
        $slug = $optional['slug'];
        $linkReturn = BASE_URL;
        $linkReturn .= "$slug-dt$id";
        if(isset($optional['page'])) $linkReturn .= '/page/';
        return $linkReturn;
    }
}
if (!function_exists('getUrlVoucher')) {
    function getUrlVoucher($optional){
        if(is_object($optional)) $optional = (array) $optional;
        $id = $optional['id'];
        $slug = $optional['slug'];
        $linkReturn = BASE_URL;
        $linkReturn .= "$slug-dv$id";
        if(isset($optional['page'])) $linkReturn .= '/page/';
        return $linkReturn;
    }
}

if (!function_exists('getUrlPage')) {

  function getUrlPage($optional = []){
    $_this =& get_instance();
        if(is_object($optional)) $optional = (array) $optional;
        $linkReturn = BASE_URL;
        if(!empty($optional['slug'])) {
            $slug = $optional['slug'];
            $linkReturn .= "$slug";
        }else{
          $_this->load->model('page_model');
          $pageModel=new Page_model();
          $data=$pageModel->getById($optional['id'],'*',$_this->session->public_lang_code);
          $linkReturn .= "$data->slug";
        }
        if(isset($optional['page'])) $linkReturn .= '/page/';
        return $linkReturn;
    }
}

if (!function_exists('getUrlTag')) {
    function getUrlTag($keyword){
        $_this =& get_instance();
        $_this->load->library('session');
        $_this->load->helper('url');
        $slug = $_this->toSlug($keyword);
        $linkReturn = BASE_URL."search/$slug";
        return $linkReturn;
    }
}

if (!function_exists('getUrlSearch')) {
    function getUrlSearch($keyword){
        $_this =& get_instance();
        $_this->load->library('session');
        $_this->load->helper('url');
        $slug = $_this->toSlug($keyword);
        $linkReturn = BASE_URL."search/$slug";
        return $linkReturn;
    }
}



