<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
if (!function_exists('formatMoney')) {
  function formatMoney($price, $default = true)
  {
    $_this =& get_instance();
    $_this->load->language('frontend');
    return !empty($price) ? number_format($price) . " VND" : (($default == true) ? $_this->lang->line('text_contact_buy') : '');
  }
}
if (!function_exists('getYoutubeKey')) {
  function getYoutubeKey($url)
  {
    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
    $youtube_id = $match[1];
    return trim($youtube_id);
  }
}
if (!function_exists('formatTimeLeft')) {
  function formatTimeLeft($date1, $date2)
  {
    $diff = (strtotime($date2) - strtotime($date1));
    if ($diff > 0) {
      $diff = abs($diff);
      $days = floor($diff / (60 * 60 * 24));
      $hours = floor(($diff - $days * 60 * 60 * 24) / (60 * 60));
      $minutes = floor(($diff - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
      $seconds = floor(($diff - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));
      echo $days . " ngày " . $hours . ":" . $minutes . ":" . $seconds;
    }else{
      echo date('d/m/Y',strtotime($date1));
    }

  }
}

if (!function_exists('show_selected')) {
  function show_selected($param,$value)
  {
    $selected='';
    if(!empty($_REQUEST[$param]) && $_REQUEST[$param]==$value) $selected='selected';
    return $selected;
  }
}