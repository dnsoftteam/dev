<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 14/12/2017
 * Time: 12:21 SA
 */
?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 3.0
    </div>
    <strong>Copyright &copy; 2017 <a href="https://apecsoft.asia">APECSOFT</a>.</strong> All rights reserved.
</footer>
