<div class="tab-pane" id="tab_contact">

  <ul class="nav nav-tabs">
    <?php foreach ($this->config->item('cms_language') as $lang_code => $lang_name) { ?>
      <li<?php echo ($lang_code == 'vi') ? ' class="active"' : ''; ?>>
        <a href="#tab_contact1_<?php echo $lang_code; ?>" data-toggle="tab">
          <img src="<?php echo $this->templates_assets; ?>/flag/<?php echo $lang_code ?>.png"> <?php echo $lang_name; ?>
        </a>
      </li>
    <?php } ?>
  </ul>

  <div class="tab-content">
    <?php foreach ($this->config->item('cms_language') as $lang_code => $lang_name) { ?>
      <div class="tab-pane <?php echo ($lang_code == 'vi') ? 'active' : ''; ?>"
           id="tab_contact1_<?php echo $lang_code; ?>">
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Trụ sở chính</label>
                <input name="contact[<?php echo $lang_code; ?>][address]"
                       placeholder="Địa chỉ"
                       class="form-control" type="text"
                       value="<?php echo !empty($contact[$lang_code]['address']) ? $contact[$lang_code]['address'] : ''; ?>"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Điện thoại</label>
                <input name="contact[<?php echo $lang_code; ?>][phone]"
                       placeholder="Điện thoại"
                       class="form-control" type="text"
                       value="<?php echo !empty($contact[$lang_code]['phone']) ? $contact[$lang_code]['phone'] : ''; ?>"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Fax</label>
                <input name="contact[<?php echo $lang_code; ?>][fax]"
                       placeholder="Fax"
                       class="form-control" type="text"
                       value="<?php echo !empty($contact[$lang_code]['fax']) ? $contact[$lang_code]['fax'] : ''; ?>"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Email</label>
                <input name="contact[<?php echo $lang_code; ?>][email]"
                       placeholder="mail"
                       class="form-control" type="text"
                       value="<?php echo !empty($contact[$lang_code]['email']) ? $contact[$lang_code]['email'] : ''; ?>"/>
              </div>
            </div>
          </div>

        </div>
      </div>
    <?php } ?>
    <div class="form-group">
      <label>Địa chỉ Google maps (Nhập kinh độ và vĩ độ)</label>
      <div class="row">
        <div class="col-sm-6 col-xs-12">
          <input name="contact[maps_latitude]"
                 placeholder="Kinh độ (latitude)"
                 class="form-control" type="text"
                 value="<?php echo !empty($contact['maps_latitude']) ? $contact['maps_latitude'] : ''; ?>"/>
        </div>
        <div class="col-sm-6 col-xs-12">
          <input name="contact[maps_longitude]"
                 placeholder="Vĩ độ (longitude)"
                 class="form-control" type="text"
                 value="<?php echo !empty($contact['maps_longitude']) ? $contact['maps_longitude'] : ''; ?>"/>
        </div>
      </div>
    </div>
  </div>

  <fieldset class="form-group album-contain">
    <legend>Danh sách chi nhánh</legend>
    <?php
    $total_chi_nhanh = 0;
    if (!empty($chi_nhanh)):
      $total_chi_nhanh = getNumberics($chi_nhanh);
//    dd($chi_nhanh);
    endif;
    ?>
    <div data-id="<?php echo $total_chi_nhanh ?>" id="chi_nhanh">
      <?php if (!empty($chi_nhanh)) foreach ($chi_nhanh as $key => $keytem):
        ?>
        <fieldset>
          <div class="tab-pane _flex" id="tab_history">
            <div class="col-md-12">
              <ul class="nav nav-tabs">
                <?php foreach ($this->config->item('cms_language') as $lang_code => $lang_name) { ?>
                  <li<?php echo ($lang_code == 'vi') ? ' class="active"' : ''; ?>>
                    <a href="#tab_<?php echo $lang_code . $key; ?>" data-toggle="tab">
                      <?php echo $lang_name; ?>
                    </a>
                  </li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php
                foreach ($this->config->item('cms_language') as $lang_code => $lang_name) { ?>
                  <div class="tab-pane <?php echo ($lang_code == 'vi') ? 'active' : ''; ?>"
                       id="tab_<?php echo $lang_code . $key; ?>">
                    <fieldset style="width: 100%">
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="text"
                                 name="chi_nhanh[<?php echo $key; ?>][<?php echo $lang_code ?>][location]"
                                 class="form-control"
                                 placeholder="Tiêu đề"
                                 value="<?php echo !empty($keytem[$lang_code]['location']) ? $keytem[$lang_code]['location'] : ''; ?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text"
                                 name="chi_nhanh[<?php echo $key; ?>][<?php echo $lang_code ?>][address]"
                                 class="form-control"
                                 placeholder="Chi nhánh"
                                 value="<?php echo !empty($keytem[$lang_code]['address']) ? $keytem[$lang_code]['address'] : ''; ?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text"
                                 name="chi_nhanh[<?php echo $key; ?>][<?php echo $lang_code ?>][tel]"
                                 class="form-control"
                                 placeholder="Tel"
                                 value="<?php echo !empty($keytem[$lang_code]['tel']) ? $keytem[$lang_code]['tel'] : ''; ?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text"
                                 name="chi_nhanh[<?php echo $key; ?>][<?php echo $lang_code ?>][fax]"
                                 class="form-control"
                                 placeholder="Fax"
                                 value="<?php echo !empty($keytem[$lang_code]['fax']) ? $keytem[$lang_code]['fax'] : ''; ?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text"
                                 name="chi_nhanh[<?php echo $key; ?>][<?php echo $lang_code ?>][email]"
                                 class="form-control"
                                 placeholder="Email"
                                 value="<?php echo !empty($keytem[$lang_code]['email']) ? $keytem[$lang_code]['email'] : ''; ?>">
                        </div>
                      </div>

                      <div class="col-sm-6 col-xs-12">
                        <input name="chi_nhanh[<?php echo $key; ?>][<?php echo $lang_code ?>][lat]"
                               placeholder="Kinh độ (latitude)"
                               class="form-control" type="text"
                               value="<?php echo !empty($keytem[$lang_code]['lat']) ? $keytem[$lang_code]['lat'] : ''; ?>"/>
                      </div>
                      <div class="col-sm-6 col-xs-12">
                        <input name="chi_nhanh[<?php echo $key; ?>][<?php echo $lang_code ?>][long]"
                               placeholder="Vĩ độ (longitude)"
                               class="form-control" type="text"
                               value="<?php echo !empty($keytem[$lang_code]['long']) ? $keytem[$lang_code]['long'] : ''; ?>"/>
                      </div>

                    </fieldset>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>

          <i class="glyphicon glyphicon-trash removeInput" onclick="removeInputImage(this)"></i>
        </fieldset>
      <?php endforeach; ?>
    </div>
    <button type="button" class="btn btn-primary btnAddMore"
            onclick="addInputElementSettings('chi_nhanh',document.getElementById('chi_nhanh').getAttribute('data-id'),null,'ajax_load_item_chi_nhanh')">
      <i class="fa fa-plus"> <?php echo lang('btn_add'); ?>
      </i></button>
  </fieldset>

  <fieldset class="form-group album-contain">
    <legend>Mạng xã hội</legend>
    <div class="form-group">
      <label><?php echo lang('form_social_fb'); ?></label>
      <input name="social_fb" placeholder="<?php echo lang('form_social_fb'); ?>"
             class="form-control" type="text"
             value="<?php echo isset($social_fb) ? $social_fb : ''; ?>"/>
    </div>
    <div class="form-group">
      <label><?php echo lang('form_social_google'); ?></label>
      <input name="social_google"
             placeholder="<?php echo lang('form_social_google'); ?>"
             class="form-control" type="text"
             value="<?php echo isset($social_google) ? $social_google : ''; ?>"/>
    </div>
    <div class="form-group">
      <label><?php echo lang('form_social_twitter'); ?></label>
      <input name="social_twitter"
             placeholder="<?php echo lang('form_social_twitter'); ?>"
             class="form-control" type="text"
             value="<?php echo isset($social_twitter) ? $social_twitter : ''; ?>"/>
    </div>
    <div class="form-group">
      <label><?php echo lang('form_social_youtube'); ?></label>
      <input name="social_youtube"
             placeholder="<?php echo lang('form_social_youtube'); ?>"
             class="form-control" type="text"
             value="<?php echo isset($social_youtube) ? $social_youtube : ''; ?>"/>
    </div>
    <div class="form-group">
      <label><?php echo lang('form_social_instagram'); ?></label>
      <input name="social_instagram"
             placeholder="<?php echo lang('form_social_instagram'); ?>"
             class="form-control" type="text"
             value="<?php echo isset($social_instagram) ? $social_instagram : ''; ?>"/>
    </div>
    <div class="form-group">
      <label><?php echo lang('form_social_tumblr'); ?></label>
      <input name="social_tumblr"
             placeholder="<?php echo lang('form_social_tumblr'); ?>"
             class="form-control" type="text"
             value="<?php echo isset($social_tumblr) ? $social_tumblr : ''; ?>"/>
    </div>
    <div class="form-group">
      <label><?php echo lang('form_social_linkedin'); ?></label>
      <input name="social_linkedin"
             placeholder="<?php echo lang('form_social_linkedin'); ?>"
             class="form-control" type="text"
             value="<?php echo isset($social_linkedin) ? $social_linkedin : ''; ?>"/>
    </div>
  </fieldset>
</div>
