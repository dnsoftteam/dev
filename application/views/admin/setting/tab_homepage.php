<div class="tab-pane" id="tab_homepage">

  <fieldset class="form-group album-contain">
    <legend for="">Slide trang chủ</legend>
    <?php
    $totalItemSlide = 0;
    if (!empty($slides)):
      $totalItemSlide = getNumberics($slides);
    endif;
    ?>
    <div data-id="<?php echo $totalItemSlide ?>" id="slides">
      <?php if (!empty($slides)) foreach ($slides as $i => $item):
        ?>
        <fieldset>
          <div class="tab-pane" id="tab_store">
            <ul class="nav nav-tabs">
              <?php foreach ($this->config->item('cms_language') as $lang_code => $lang_name) { ?>
                <li<?php echo ($lang_code == 'vi') ? ' class="active"' : ''; ?>>
                  <a href="#tab_<?php echo $lang_code . $i; ?>" data-toggle="tab">
                    <?php echo $lang_name; ?>
                  </a>
                </li>
              <?php } ?>
            </ul>
            <div class="tab-content">
              <?php foreach ($this->config->item('cms_language') as $lang_code => $lang_name) { ?>
                <div class="tab-pane <?php echo ($lang_code == 'vi') ? 'active' : ''; ?>"
                     id="tab_<?php echo $lang_code . $i; ?>">
                  <fieldset style="width: 100%">
                    <div class="col-md-6">
                      <input type="text"
                             name="slides[<?php echo $i; ?>][<?php echo $lang_code ?>][title]"
                             class="form-control"
                             placeholder="Tiêu đề"
                             value="<?php echo !empty($item[$lang_code]['title']) ? $item[$lang_code]['title'] : ''; ?>">
                    </div>
                    <div class="col-md-4">
                      <input type="text"
                             name="slides[<?php echo $i; ?>][<?php echo $lang_code ?>][link]"
                             class="form-control"
                             placeholder="Đường dẫn"
                             value="<?php echo !empty($item[$lang_code]['link']) ? $item[$lang_code]['link'] : ''; ?>">
                    </div>
                    <div class="col-md-2">
                      <input
                          id="slides_<?php echo $lang_code . $i; ?>"
                          name="slides[<?php echo $i; ?>][<?php echo $lang_code ?>][img]"
                          value="<?php echo !empty($item[$lang_code]['img']) ? $item[$lang_code]['img'] : ''; ?>"
                          class="form-control col-md-6" type="hidden"
                          style="width: 50%"/>
                      <img onclick="chooseImage('slides_<?php echo $lang_code . $i; ?>')"
                           src="<?php echo isset($item[$lang_code]['img']) ? getImageThumb($item[$lang_code]['img']) : 'http://via.placeholder.com/100x50'; ?>"
                           alt="" width="100">
                    </div>
                  </fieldset>
                </div>
              <?php } ?>
            </div>
          </div>

          <i class="glyphicon glyphicon-trash removeInput" onclick="removeInputImage(this)"></i>
        </fieldset>
      <?php endforeach; ?>
    </div>
    <button type="button" class="btn btn-primary btnAddMore"
            onclick="addInputElementSettings('slides',document.getElementById('slides').getAttribute('data-id'),null,'ajax_load_item_slide')">
      <i class="fa fa-plus"> <?php echo lang('btn_add'); ?>
      </i></button>
  </fieldset>
  <fieldset>
    <legend for="">Tại sao nên chọn sharetour</legend>
    <div class="form-group">
      <div class="row _flex">
        <div class="col-md-12">
          <textarea name="taisaochon" class="form-control tinymce"
                    rows="3"
                    placeholder="Giới thiệu"><?php echo !empty($taisaochon) ? $taisaochon : ''; ?></textarea>
        </div>
      </div>
    </div>
  </fieldset>
  <fieldset class="album-contain">
    <legend for="">5 lý do chọn sharetour</legend>
    <?php
    $totalOption = 0;
    if (!empty($option)):
      $totalOption = getNumberics($option);
      foreach ($option as $key => $item) {
        $key = $key + 1;
        ?>
        <fieldset class="form-group">
          <div class="row _flex">
            <div class="col-md-4">

              <input type="text" placeholder="Tiêu đề" name="option[<?php echo $key ?>][title]" class="form-control"
                     value="<?php echo (!empty($item['title'])) ? $item['title'] : '' ?>">
            </div>
            <div class="col-md-4">
              <textarea name="option[<?php echo $key ?>][content]" class="form-control"
                        placeholder="Nội dung"><?php echo (!empty($item['content'])) ? $item['content'] : '' ?></textarea>
            </div>
            <div class="col-md-4 _flex">
              <input id="option_<?php echo $key ?>_img" name="option[<?php echo $key ?>][img]"
                     value="<?php echo (!empty($item['img'])) ? $item['img'] : '' ?>" class="form-control col-md-6"
                     type="text" style="width: 50%"/>
              <img onclick="chooseImage('option_<?php echo $key ?>_img')"
                   src="<?php echo isset($item['img']) ? getImageThumb($item['img']) : 'http://via.placeholder.com/100x50'; ?>"
                   style="margin-left: 30px"
                   alt="" width="100">
            </div>
          </div>
        </fieldset>
        <?php
      }
    endif;
    ?>
    <label for="">Banner quảng cáo</label>
    <div class="row">
      <div class="col-xs-8">
        <input type="text" name="link_adv" placeholder="Link quảng cáo"
               value="<?php echo !empty($link_adv) ? $link_adv : '' ?>" class="form-control">
      </div>
      <div class="col-md-4">
        <input id="banner_adv" name="banner_adv" value="<?php echo (!empty($banner_adv)) ? $banner_adv : '' ?>"
               class="form-control col-md-6" type="text" style="width: 50%"/>
        <img onclick="chooseImage('banner_adv')"
             src="<?php echo isset($banner_adv) ? getImageThumb($banner_adv, 200, 100, true) : 'http://via.placeholder.com/200x100'; ?>"
             style="margin-left: 30px"
             alt="" width="100">
      </div>
    </div>
  </fieldset>

  <fieldset class="form-group album-contain">
    <legend for="">Ý kiến khách hàng</legend>
    <?php
    $totalFeedBack = 0;
    if (!empty($feedback)):
      $totalFeedBack = getNumberics($feedback);
    endif;
    ?>
    <div data-id="<?php echo $totalFeedBack ?>" id="feedback">
      <?php if (!empty($feedback)) foreach ($feedback as $i => $item):
        ?>
        <fieldset>
          <div class="col-md-12">
            <div class="tab-content">
              <div class="tab-pane active">
                <div class="row _flex" style="">
                  <div class="col-md-4">
                    <input type="text"
                           name="feedback[<?php echo $i; ?>][name]"
                           class="form-control"
                           placeholder="Tiêu đề"
                           value="<?php echo !empty($item['name']) ? $item['name'] : ''; ?>">
                  </div>
                  <div class="col-md-6">
                      <textarea class="form-control" rows="4"
                                name="feedback[<?php echo $i; ?>][content]"
                                placeholder="Nội dung"><?php echo !empty($item['content']) ? $item['content'] : ''; ?></textarea>
                  </div>
                  <div class="col-md-2">
                    <input
                        id="feedback_<?php echo $i; ?>"
                        name="feedback[<?php echo $i; ?>][img]"
                        value="<?php echo !empty($item['img']) ? $item['img'] : ''; ?>"
                        class="form-control col-md-6" type="hidden"
                        style="width: 50%"/>
                    <img onclick="chooseImage('feedback_<?php echo $i; ?>')"
                         src="<?php echo isset($item['img']) ? getImageThumb($item['img']) : 'http://via.placeholder.com/100x50'; ?>"
                         alt="" width="100">
                  </div>

                </div>
              </div>

            </div>
          </div>
          <i class="glyphicon glyphicon-trash removeInput" onclick="removeInputImage(this)"></i>
        </fieldset>
      <?php endforeach; ?>
    </div>
    <button type="button" class="btn btn-primary btnAddMore"
            onclick="addInputElementSettings('feedback',document.getElementById('feedback').getAttribute('data-id'),null,'ajax_load_feedback')">
      <i class="fa fa-plus"> <?php echo lang('btn_add'); ?>
      </i></button>
  </fieldset>

  <fieldset class="form-group album-contain">
    <legend for="">Đối tác</legend>
    <?php
    $totalPartner = 0;
    if (!empty($partner)):
      $totalPartner = getNumberics($partner);
    endif;
    ?>
    <div data-id="<?php echo $totalPartner ?>" id="partner">
      <?php if (!empty($partner)) foreach ($partner as $i => $item):
        ?>
        <fieldset>
          <div class="col-md-12">
            <div class="tab-content">
              <div class="tab-pane active">
                <div class="row _flex" style="">
                  <div class="col-md-5">
                    <input type="text"
                           name="partner[<?php echo $i; ?>][name]"
                           class="form-control"
                           placeholder="Link"
                           value="<?php echo !empty($item['name']) ? $item['name'] : ''; ?>">
                  </div>
                  <div class="col-md-4">
                    <input type="text"
                           name="partner[<?php echo $i; ?>][link]"
                           class="form-control"
                           placeholder="Link"
                           value="<?php echo !empty($item['link']) ? $item['link'] : ''; ?>">
                  </div>
                  <div class="col-md-3">
                    <input
                        id="partner_<?php echo $i; ?>"
                        name="partner[<?php echo $i; ?>][img]"
                        value="<?php echo !empty($item['img']) ? $item['img'] : ''; ?>"
                        class="form-control col-md-6" type="hidden"
                        style="width: 50%"/>
                    <img onclick="chooseImage('partner_<?php echo $i; ?>')"
                         src="<?php echo isset($item['img']) ? getImageThumb($item['img']) : 'http://via.placeholder.com/100x50'; ?>"
                         alt="" width="100">
                  </div>

                </div>
              </div>

            </div>
          </div>
          <i class="glyphicon glyphicon-trash removeInput" onclick="removeInputImage(this)"></i>
        </fieldset>
      <?php endforeach; ?>
    </div>
    <button type="button" class="btn btn-primary btnAddMore"
            onclick="addInputElementSettings('partner',document.getElementById('partner').getAttribute('data-id'),null,'ajax_load_partner')">
      <i class="fa fa-plus"> <?php echo lang('btn_add'); ?>
      </i></button>
  </fieldset>
</div>