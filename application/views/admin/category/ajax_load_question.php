<?php
$item = !empty($item) ? (object)$item : null;
?>
<?php if (isset($_GET['id'])) {
  $id = $_GET['id'];
} else {
  $id = '';
} ?>
<?php if (isset($_GET['meta_key'])) {
  $meta_key = $_GET['meta_key'];
} else {
  $meta_key = '';
} ?>
<fieldset>
  <label for="">Câu hỏi <?php echo $meta_key . $id ?> :</label>

  <fieldset style="width: 100%">
    <div class="row _flex">
      <div class="col-md-6">
        <div class="form-group">
          <input name="question[<?php echo $meta_key . $id ?>][question]"
                 placeholder="Câu hỏi ?" class="form-control" type="text"
                 value="<?php echo !empty($item->question) ? $item->question : '' ?>"/>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <textarea name="question[<?php echo $meta_key . $id ?>][reply]"rows="5" placeholder="Câu trả lời" class="form-control"><?php echo !empty($item->reply) ? $item->reply : '' ?></textarea>
        </div>
      </div>
    </div>
  </fieldset>

  <i class="fa fa-times removeInput" onclick="removeInputImage(this)"></i>
</fieldset>