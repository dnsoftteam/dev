<?php
$item = !empty($item) ? (object)$item : null;

?>
<?php if (isset($_GET['id'])) {
  $id = $_GET['id'];
} else {
  $id = '';
} ?>
<?php if (isset($_GET['meta_key'])) {
  $meta_key = $_GET['meta_key'];
} else {
  $meta_key = '';
} ?>
<fieldset>
  <label for="">Đánh giá</label>

  <fieldset style="width: 100%">
    <div class="col-md-6">
      <div class="form-group">
        <input name="ratting[<?php echo $meta_key . $id ?>][name]"
               placeholder="Họ và tên" class="form-control" type="text"
               value="<?php echo !empty($item->name) ? $item->name : '' ?>"/>
      </div>
      <div class="form-group">
        <input name="ratting[<?php echo $meta_key . $id ?>][title]"
               placeholder="Tiêu đề" class="form-control" type="text"
               value="<?php echo !empty($item->title) ? $item->title : '' ?>"/>
      </div>
      <div class="form-group">
        <select name="ratting[<?php echo $meta_key . $id ?>][vote]" class="form-control">
          <option value="5" <?php if(!empty($item->vote) && $item->vote==5) echo 'selected'; ?>>5 Sao</option>
          <option value="4" <?php if(!empty($item->vote) && $item->vote==4) echo 'selected'; ?>>4 Sao</option>
          <option value="3" <?php if(!empty($item->vote) && $item->vote==3) echo 'selected'; ?>>3 Sao</option>
          <option value="2" <?php if(!empty($item->vote) && $item->vote==2) echo 'selected'; ?>>2 Sao</option>
          <option value="1" <?php if(!empty($item->vote) && $item->vote==1) echo 'selected'; ?>>1 Sao</option>
          <option value="0" <?php if(!empty($item->vote) && $item->vote==0) echo 'selected'; ?>>0 Sao</option>
        </select>
      </div>
    </div>
    <div class="col-md-6">
          <textarea name="ratting[<?php echo $meta_key . $id ?>][desc]"
                    placeholder="Nội dung"
                    class="form-control" rows="6"><?php echo !empty($item->desc) ? $item->desc : '' ?></textarea>
    </div>
    <input type="hidden" name="ratting[<?php echo $meta_key . $id ?>][date]" value="<?php date('d/m/Y') ?>">
  </fieldset>

  <i class="fa fa-times removeInput" onclick="removeInputImage(this)"></i>
</fieldset>