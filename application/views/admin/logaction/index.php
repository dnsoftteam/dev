<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-filter"></i></span>
                            <form method="get" id="a_form">
                                <select class="form-control" id="category_id" name="category_id">
                                    <option value="0"><?php echo lang('from_category');?></option>
                                    <?php if(!empty($users)) foreach ($users as $user){ ?>
                                       <option value="<?php echo $user->id;?>"><?php echo $user->username;?></option>
                                    <?php } ?>
                                </select>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="select_all" value="1" id="data-table-select-all"></th>
                            <th>ID</th>
                            <th>Action</th>
                            <th>Note</th>
                            <th>Author</th>
                            <th>Ngày thực hiện</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<!-- /.content-wrapper -->
<script>
    var category_id="<?php echo $this->input->get('category_id') ?>";
</script>
