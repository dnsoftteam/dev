

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <div class="col-sm-6">
                        </div>
                        <div class="col-sm-6 text-right">
                            <button class="btn btn-success" title="<?php echo lang('tooltip_export_excel');?>" onclick="window.location.href = '<?php echo site_url('admin/order/export_excel')?>'">
                                <i class="glyphicon glyphicon-floppy-save"></i> <?php echo lang('btn_export_excel');?>
                            </button>
<!--                            <button class="btn btn-success" onclick="add_form()">-->
<!--                                <i class="glyphicon glyphicon-plus"></i> --><?php //echo lang('btn_add');?>
<!--                            </button>-->
<!--                            <button class="btn btn-danger" onclick="delete_multiple()">-->
<!--                                <i class="glyphicon glyphicon-trash"></i> --><?php //echo lang('btn_remove');?>
<!--                            </button>-->
                            <button class="btn btn-default" onclick="reload_table()">
                                <i class="glyphicon glyphicon-refresh"></i> <?php echo lang('btn_reload');?>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body">
                        <table id="data-table" class="table table-responsive table-sm ">
                            <thead>
                            <tr class="text-center">
                                <th  ><input type="checkbox" name="select_all" value="1" id="data-table-select-all"></th>
                                <th width="10%">Đơn hàng</th>
                                <th width="10%"><?php echo lang('col_firstname');?></th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                                <th><?php echo lang('col_shipped_time');?></th>
                                <th>Tổng tiền</th>
                                <th>Trạng thái</th>
                                <th><?php echo lang('col_action');?></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

<!-- /.content-wrapper -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" style="width: 100%; max-width: 850px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="title-form">Chi tiết đơn hàng</h3>
            </div>
            <div class="modal-body form">
                <table class="table">
                    <tr>
                        <th><?php echo lang('col_firstname'); ?>:</th>
                        <td id="fullname"></td>
                    </tr>
                    <tr>
                        <th><?php echo lang('col_address'); ?>:</th>
                        <td id="address"></td>
                    </tr>
                  <tr>
                    <th>Số điện thoại:</th>
                    <td id="phone"></td>
                  </tr>
                  <tr>
                    <th>Email:</th>
                    <td id="email"></td>
                  </tr>
                    <tr>
                        <th><?php echo lang('col_total_amount'); ?>:</th>
                        <td id="total_amount"></td>
                    </tr>
                    <tr>
                        <th><?php echo lang('col_created_or_at'); ?>:</th>
                        <td id="created_time"></td>
                    </tr>
                    <tr>
                        <th><?php echo lang('col_shipped_time'); ?>:</th>
                        <td id="shipped_time"></td>
                    </tr>

                  <tr>
                    <th>Mã giảm giá:</th>
                    <td id="coupon"></td>
                  </tr>
                  <tr>
                    <th>Địa chỉ viết hóa đơn:</th>
                    <td id="bill_address"></td>
                  </tr>
                  <tr>
                    <th>Note:</th>
                    <td id="note"></td>
                  </tr>
                    <tr>
                        <th><?php echo lang('col_status'); ?>:</th>
                        <td id="is_status"></td>
                    </tr>
                </table>
                <style>
                    table.list-detail th,table.list-detail tr td{text-align: center}
                </style>
                <table class="table list-detail">
                    <thead>
                        <tr style="    background: #00588c;color: #fff">
                            <th>Tên sản phẩm</th>
                            <th>Số lượng</th>
                            <th>Giá</th>
                            <th>Giá khuyến mại</th>
                            <th>Thành tiền</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary pull-left"><?php echo lang('btn_save');?></button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo lang('btn_cancel');?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script>
    var url_ajax_list = '<?php echo site_url('admin/order/ajax_list')?>',
        url_ajax_view = '<?php echo site_url('admin/order/ajax_view')?>',
        url_ajax_update = '<?php echo site_url('admin/order/ajax_update')?>',
        url_ajax_add = '<?php echo site_url('admin/order/ajax_add')?>',
        url_ajax_delete = '<?php echo site_url('admin/order/ajax_delete')?>';
</script>