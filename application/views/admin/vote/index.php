<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 14/12/2017
 * Time: 5:10 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <?php $this->load->view($this->template_path."_block/where_datatables") ?>
                    <?php $this->load->view($this->template_path."_block/button") ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="select_all" value="1" id="data-table-select-all"></th>
                            <th>ID</th>
                            <th><?php echo lang('text_name');?></th>
                            <th>Ảnh</th>
                            <th>Tên công ty</th>
                            <th>Vote</th>
                            <th><?php echo lang('text_created_time');?></th>
                            <th><?php echo lang('text_action');?></th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="title-form"><?php echo lang('heading_title_add');?></h3>
            </div>
            <div class="modal-body form">
                <div class="box-body">
                    <?php echo form_open('',['class'=>'form-horizontal']) ?>
                    <input type="hidden" value="" name="id" />
                    <input type="hidden" name="style" value="<?php echo $this->router->fetch_method() === 'nhansu' ? 'nhansu' : 'feedback' ?>" />
                    <div class="form-group">
                        <label>Họ và tên</label>
                        <input name="fullname" placeholder="Họ và tên" class="form-control" type="text" />
                    </div>

                    <?php if($this->router->fetch_method() === 'nhansu'): ?>
                        <div class="form-group">
                            <label>Chức vụ</label>
                            <input name="title" placeholder="Chức vụ" class="form-control" type="text" />
                        </div>
                    <?php else:  ?>
                        <div class="form-group">
                            <label>Tên công ty</label>
                            <input name="title" placeholder="Tên công ty" class="form-control" type="text" />
                        </div>
                        <div class="form-group">
                            <label>Vote</label>
                            <input name="vote" placeholder="Vote" class="form-control" type="number" min="1" max="5" />
                        </div>
                        <div class="form-group">
                            <label>Phản hồi</label>
                            <textarea name="comment" class="form-control" rows="5"></textarea>
                        </div>
                    <?php endif; ?>


                    <?php $this->load->view($this->template_path. '_block/input_media') ?>
                    <?php echo form_close() ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary pull-left"><?php echo lang('btn_save');?></button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo lang('btn_cancel');?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script type="text/javascript">
    var page_type = '<?php echo $this->router->fetch_method() === 'nhansu' ? 'nhansu' : 'feedback' ?>';
</script>