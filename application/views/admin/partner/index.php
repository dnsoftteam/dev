<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 14/12/2017
 * Time: 5:10 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <?php $this->load->view($this->template_path."_block/where_datatables") ?>
                    <?php $this->load->view($this->template_path."_block/button") ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <table id="data-table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="select_all" value="1" id="data-table-select-all"></th>
                            <th><?php echo lang('text_id');?></th>
                            <th><?php echo lang('text_title');?></th>
                            <th><?php echo lang('text_status');?></th>
                            <th><?php echo lang('text_updated_time');?></th>
                            <th><?php echo lang('text_created_time');?></th>
                            <th><?php echo lang('text_action');?></th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog" style="width: 90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="title-form"><?php echo lang('heading_title_add');?></h3>
            </div>
            <div class="modal-body form">
                <?php echo form_open('',['id'=>'form','class'=>'form']) ?>
                <div class="box-body">
                    <input type="hidden" name="id" value="0">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên đại lý</label>
                                <input name="title" placeholder="Tên đại lý" class="form-control" type="text" />
                            </div>
                            <div class="form-group">
                                <label>Tỉnh / Thành phố</label>
                                <select class="form-control select2" name="city_id" style="width: 100%;" tabindex="-1" aria-hidden="true"></select>
                            </div>
                            <div class="form-group">
                                <label>Vùng miền</label>
                                <select class="form-control select2" name="area_id" style="width: 100%;" tabindex="-1" aria-hidden="true"></select>
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ</label>
                                <input name="address" placeholder="Địa chỉ" class="form-control" type="text" />
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label>Latitude (Google maps)</label>
                                <input name="latitude" placeholder="Latitude (Google maps)" class="form-control" type="text" />
                            </div>
                            <div class="form-group">
                                <label>Longitude (Google maps)</label>
                                <input name="longitude" placeholder="Longitude (Google maps)" class="form-control" type="text" />
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input name="phone" placeholder="Phone" class="form-control" type="text" />
                            </div>
                            <?php $this->load->view($this->template_path. '_block/input_media') ?>
                            <!--<div class="form-group">
                                <label>Thời gian mở cửa</label>
                                <input name="open_time" placeholder="Thời gian mở cửa" class="form-control" type="text" />
                            </div>-->

                        </div>
                    </div>
                </div>
                <?php echo form_close() ?>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary pull-left"><?php echo lang('btn_save');?></button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo lang('btn_cancel');?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->

<script>
    var url_ajax_city = '<?php echo site_url('admin/partner/ajax_load_city') ?>';
    var url_ajax_district = '<?php echo site_url('admin/partner/ajax_load_district') ?>';
    var url_ajax_area = '<?php echo site_url('admin/category/ajax_load/partner') ?>';
</script>