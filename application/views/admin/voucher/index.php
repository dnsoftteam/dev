<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 17/12/2017
 * Time: 3:11 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style type="text/css">
  table#data-table tr td:nth-child(3) {
    width: 250px;
  }
</style>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body">
          <div class="col-sm-7"></div>
          <?php $this->load->view($this->template_path . "_block/button") ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body">
          <form action="" id="form-table" method="post">
            <table id="data-table" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th><input type="checkbox" name="select_all" value="1" id="data-table-select-all"></th>
                <th>ID</th>
                <th>Mã giảm giá</th>
                <th>Event</th>
                <th>Ngày bắt đầu</th>
                <th>Ngày kết thúc</th>
                <th>Giảm giá</th>
                <th>Số lần sử dụng</th>
                <th>Còn lại</th>
                <th>Trạng thái</th>
                <th>Hành động</th>
              </tr>
              </thead>
            </table>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog" style="width: 70%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
              aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="title-form">Voucher</h3>
      </div>
      <div class="modal-body form">
        <?php echo form_open('', ['id' => 'form', 'class' => '']) ?>
        <input type="hidden" name="id" value="0">
        <!-- Custom Tabs -->
        <div class="box-body">
          <div class="form-group">
            <label>Tên sự kiện</label>
            <input type="text" name="event" placeholder="Tiêu đề sự kiện" class="form-control">
          </div>
          <div class="form-group">
            <label>Mã voucher</label>
            <div class="input-group">

              <input name="code" placeholder="Mã voucher" class="form-control generator_code" type="text"/>
              <div class="input-group-addon" style="padding: 0">
                <button class="generator btn btn-primary" type="button">Tạo mã</button>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>Phần trăm khuyến mại</label>
                <input type="text" name="percent_sale" class="form-control" placeholder="Phần trăm khuyến mại">
              </div>
              <div class="col-md-6">
                <label>Số tiền khuyến mại</label>
                <input type="text" name="price_sale" class="form-control" placeholder="Số tiền khuyến mại">
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>Áp dụng từ ngày</label>
                <input type="text" name="start_time" class="form-control datepicker" placeholder="Áp dụng từ ngày ">
              </div>
              <div class="col-md-6">
                <label>Đến ngày</label>
                <input type="text" name="end_time" class="form-control datepicker" placeholder="Ngày hết hạn">
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row">
              <div class="col-md-6">
                <label>Tổng số lần sử dụng</label>
                <input type="number" name="total_use" class="form-control" placeholder="">
              </div>
              <div class="col-md-6">
                <label>Trạng thái</label>
                <select name="is_status" class="form-control">
                  <option value="1">Public</option>
                  <option value="2">Hủy</option>
                  <option value="3">Hết hạn</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <!-- nav-tabs-custom -->
        <?php echo form_close() ?>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnSave" onclick="save()"
                class="btn btn-primary pull-left"><?php echo lang('btn_save'); ?></button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo lang('btn_cancel'); ?></button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script>
  var url_ajax_load_category = '<?php echo site_url('admin/category/ajax_load') ?>';
  var url_ajax_load = '<?php echo site_url('admin/category/ajax_load') ?>';
  var ajax_check_code = '<?php echo site_url('admin/voucher/ajax_check_code') ?>';
</script>