<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 15/12/2017
 * Time: 9:12 SA
 */
defined('BASEPATH') OR exit('No direct script access allowed');
$controller = $this->router->fetch_class();
$method = $this->router->fetch_method();
?>
    <div class="col-sm-7 col-xs-12">
<?php if(in_array($controller,['category','post','product','banner','tour','voucher'])): ?>
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-filter"></i></span>
        <select class="form-control select2 filter_category" title="filter_category_id" name="category_id" style="width: 100%;" tabindex="-1" aria-hidden="true">
            <option value="0"><?php echo lang('from_category');?></option>
        </select>
    </div>
<?php endif; ?>
    </div>
