<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 17/12/2017
 * Time: 3:11 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body">
          <?php $this->load->view($this->template_path."_block/where_datatables") ?>
          <?php echo $this->load->view($this->template_path."_block/button",array('display_button'=>array('')),TRUE) ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body">
          <form action="" id="form-table" method="post">
            <input type="hidden" value="0" name="msg" />
            <table id="data-table" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th><input type="checkbox" name="select_all" value="1" id="data-table-select-all"></th>
                <th>ID</th>
                <th>user</th>
                <th></th>
                <th><?php echo lang('text_created_time');?></th>
                <th><?php echo lang('text_updated_time');?></th>
                <th><?php echo lang('text_action');?></th>
              </tr>
              </thead>
            </table>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<!-- Bootstrap modal --><!-- End Bootstrap modal -->