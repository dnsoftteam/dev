<?php //dd($this->session->unset_userdata('compare')) ?>
<section class="banner-primary compare">
  <div class="title-banner-page">
    <h2 class="heading">So sánh tour</h2>
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo base_url() ?>">Trang chủ</a></li>
        <li class="breadcrumb-item active" aria-current="page">so sánh Tour</li>
      </ol>
    </nav>
  </div>
</section>
<section class="page-compare">
  <div class="container">
    <?php
    if (!empty($this->session->userdata['compare'])) {

      $list_id=$link = $title = $code = $price = $included = $excluded = $departure_date = $time = $departure = $from = $featured = $schedule = $company = array();
      foreach ($this->session->userdata['compare'] as $key => $value) {
        $tour = getTourById($value);
        $title[] = '<a href="' . getUrlTour($tour) . '">' . $tour->title . '</a>';
        $code[] = $tour->code;
        $list_id[]=$tour->id;
        $price[] = number_format($tour->price, 0, ',', '.');
        $included[] = $tour->included;
        $excluded[] = $tour->excluded;
        $departure_date[] = $tour->departure_date;
        $time[] = $tour->day.' ngày '.(!empty($tour->nights))?$tour->nights.' đêm':'';
        $departure[] = $tour->departure;
        $from[] = $tour->from;
        $featured[] = $tour->content;
        $schedule[] = $tour->schedule;
        $company[] = $tour->company;
        $hotel[] = (!empty($tour->detail))? json_decode($tour->detail)->hotel:'';
      }
      ?>
      <div class="table-responsive">
        <table class="table table-bordered">
          <tr class="blue">
            <td align="center" colspan="<?php echo count($this->session->userdata['compare']) + 1 ?>">Bảng so sánh tour lựa chọn</td>
          </tr>
          <tr>
            <td align="center"><b>Tên tour</b></td>
            <?php
            foreach ($title as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr class="blue">
            <td align="center"><b>Mã tour</b></td>
            <?php
            foreach ($code as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr>
            <td align="center" rowspan="3"><b>Giá</b></td>
            <?php
            foreach ($price as $item) {
              echo '<td> <strong>' . $item . ' </strong></td>';
            }
            ?>
          </tr>
          <tr class="blue social">
            <?php
            foreach ($included as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr>
            <?php
            foreach ($excluded as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr class="blue">
            <td align="center"><b>Khởi hành</b></td>
            <?php
            foreach ($departure_date as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr>
            <td align="center"><b>Thời gian</b></td>
            <?php
            foreach ($time as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr class="blue">
            <td align="center"><b>Điểm đi</b></td>
            <?php
            foreach ($departure as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr>
            <td align="center"><b>Điểm đến</b></td>
            <?php
            foreach ($from as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr class="blue">
            <td align="center"><b>Nổi bật</b></td>
            <?php
            foreach ($featured as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr>
            <td align="center"><b>Hành trình</b></td>
            <?php
              foreach ($schedule as $item){
                dd(json_decode($item));
                echo '<td>';
                if(!empty($item)) foreach (json_decode($item) as $value){
                  echo 'Ngày '.$value->date.': '.$value->title;
                }
                echo '</td>';
              }
            ?>
          </tr>
          <tr class="blue">
            <td align="center"><b>Công ty</b></td>
            <?php
            foreach ($company as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr class="blue">
            <td align="center"><b>Khách sạn</b></td>
            <?php
            foreach ($hotel as $item) {
              echo '<td>' . $item . '</td>';
            }
            ?>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <?php
            foreach ($list_id as $item) {
              echo '<td class="text-center"><a href="' . site_url('book-tour/'.$item) . '" class="btn">Đặt tour này</a></td>';
            }
            ?>
          </tr>
        </table>
      </div>
      <?php
    }else{
      echo '<p>Chưa có tour nào được so sánh.</p>';
    }
    ?>

  </div>
</section>
<div class="ads-compare">
  <div class="container">
    <a href="<?php echo $this->settings['link_adv'] ?>" title="" target="_blank" rel="nofollow">
      <img src="<?php echo getImageThumb($this->settings['banner_adv']) ?>" alt="Quảng cáo">
    </a>
  </div>
</div>