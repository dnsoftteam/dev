<?php $this->load->view($this->template_path.'parts/slide-home'); ?>
<?php $this->load->view($this->template_path.'parts/tour-vouche'); ?>
<?php $this->load->view($this->template_path.'parts/why-home'); ?>
<div class="ads-home">
  <div class="container">
    <a href="<?php echo $this->settings['link_adv'] ?>" title="" target="_blank" rel="nofollow">
      <img src="<?php echo getImageThumb($this->settings['banner_adv']) ?>" alt="Quảng cáo">
    </a>
  </div>
</div>
<?php $this->load->view($this->template_path.'parts/top-destination'); ?>
<?php $this->load->view($this->template_path.'parts/voucher-home'); ?>
<?php $this->load->view($this->template_path.'parts/news-home'); ?>
<?php $this->load->view($this->template_path.'parts/feedback-home'); ?>
<?php $this->load->view($this->template_path.'parts/partner-home'); ?>
