<?php if (!empty($oneItem)) : 
?>
<section class="page-news-details">
  <div class="top-news-details">
    <img src="<?php echo getImageThumb($oneItem->thumbnail,1600,500,true) ?>" alt="<?php echo $oneItem->title; ?>">
    <div class="heading-news-details">
      <div class="container">
        <div class="wrap-heading">
          <h1><?php echo $oneItem->title ?></h1>
          <div class="time-bre">
            <span class="time"><i class="icon_clock_alt"></i><?php echo date('d', strtotime($oneItem->created_time)).' Tháng ' .date('m', strtotime($oneItem->created_time)).' Năm '.date('Y', strtotime($oneItem->created_time))  ?></span>
            <ul>
              <li><?php echo getTag($oneItem->meta_keyword); ?></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content-primary-newsdt">
    <div class="container">
      <div class="row">
        <div class="col-lg-9">
          <div class="content-left">
            <div>
              <?php echo $oneItem->content ?>
            </div>
            <div class="fb-comments-wrapper">
              <div class="fb-comments" data-href="<?php echo getUrlNews($oneItem); ?>" data-width="100%" data-numposts="5"></div>
            </div>
            <div class="share-post">
              <div class="count-share">
                <i class="fa fa-share-alt" aria-hidden="true"></i> 0
              </div>
              <div class="shareSocials"></div>
            </div>
            <div class="tags-post">
              <?php echo getTag('#'.$oneItem->meta_keyword); ?>
            </div>
          </div>
        </div>
        <div class="col-lg-3 bd-left">
          <div class="sidebar-news">
            <h3 class="title-post-detail">Danh mục tin</h3>
            <ul class="cate-post">
              <?php $danhmuc = danhmuc(); ?>
              <?php if (!empty($danhmuc)) foreach ($danhmuc as $value) : $value = (object) $value; ?>
              <li><a href="<?php echo base_url($value->link) ?>" title="<?php echo $value->title ?>"><?php echo $value->title ?></a></li>
              <?php endforeach; ?>
            </ul>
            <h3 class="title-post-detail">Danh mục tin</h3>
            <div class="list-post-sidebar">
              <?php if (!empty($list_new)) { 
                foreach ($list_new as $value) {
              ?>
              <div class="item-post-detail">
                <a href="<?php echo getUrlNews($value); ?>" class="img"><img src="<?php echo getImageThumb($value->thumbnail,337,270) ?>" alt=""></a>
                <div class="content">
                  <h3 class="heading"><a href="<?php echo getUrlNews($value); ?>" title="<?php echo getTitle($value,$this->settings); ?>"><?php echo $value->title ?></a></h3>
                  <span class="time"><i class="icon_clock_alt"></i><?php echo date('d', strtotime($value->created_time)).' Tháng ' .date('m', strtotime($value->created_time)).' Năm '.date('Y', strtotime($value->created_time)) ?></span>
                </div>
              </div>
              <?php }} ?>
            </div>
            <h3 class="title-post-detail">điểm đến nổi bật</h3>
            <div class="tags-sidebar">
              <?php if (!empty($list_point_to)) { 
                foreach ($list_point_to as $value) { ?>
              <a href="<?php echo getUrlCatTour($value) ?>" title="<?php echo getTitle($value,$this->settings) ?>"><?php echo $value->title; ?></a>
              <?php }} ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="related-post">
    <div class="container">
      <h3 class="title-post-detail">Tin liên quan</h3>
      <div class="row">
        <?php if (!empty($related)) { 
          foreach ($related as $value) {
         ?>
        <div class="col-lg-3 col-md-6">
          <div class="item-post-detail"> 
            <a href="<?php echo getUrlNews($value); ?>" class="img"><img src="<?php echo getImageThumb($value->thumbnail,337,270) ?>" alt="<?php echo $value->title ?>"></a>
            <div class="content">
              <h3 class="heading"><a href="<?php echo getUrlNews($value); ?>" title="<?php echo getTitle($value,$this->settings); ?>"><?php echo $value->title ?></a></h3>
              <span class="time"><i class="icon_clock_alt"></i><?php echo date('d', strtotime($value->created_time)).' Tháng ' .date('m', strtotime($value->created_time)).' Năm '.date('Y', strtotime($value->created_time))  ?> </span>
            </div>
          </div>
        </div>
        <?php } } ?>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>