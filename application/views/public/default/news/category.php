
<section class="banner-primary blog">
  <div class="title-banner-page blog">
    <div class="container">
      <h1 class="heading"><?php echo $category->title ?></h1>
      <nav aria-label="breadcrumb">
        <?php echo $breadcrumb ?>
      </nav>
      <div class="list-des-hot">
        <ul class="clearfix">
          <?php
          $list_cats=$this->settings['cat_featured'];
          if(!empty($list_cats)) foreach ($list_cats as $item){
            ?>
            <li>
              <a href="<?php echo getUrlCateNews(array('id'=>$item['link'])); ?>" title="">
                <img src="<?php echo getImageThumb($item['img'],220,125,false) ?>" alt="<?php echo $item['name'] ?>">
              </a>
            </li>
            <?php
          }
          ?>
        </ul>
      </div>
    </div>
  </div>
</section>
<?php
if (!empty($list_cat_featured)) {
  ?>
  <section class="list-cate-blog">
    <div class="container">
      <div class="row">
        <?php
        foreach ($list_cat_featured as $item) {
          ?>
          <div class="col-lg-4 col-md-6">
            <div class="block-cate-blog">
              <div class="heading">
                <h3><a href="<?php echo getUrlCateNews($item) ?>"><?php echo $item['title'] ?></a></h3>
              </div>
              <?php
              $i = 0;
              if (!empty($item['data'])) foreach ($item['data'] as $value) {
                $i++;
                if ($i == 1) {
                  ?>
                  <div class="item item-hot">
                    <a href="<?php echo getUrlNews($value) ?>" title="<?php echo $value->title ?>" class="img">
                      <img src="<?php echo getImageThumb($value->thumbnail, 460, 350, true) ?>"
                           alt="<?php echo $value->title ?>">
                    </a>
                    <div class="content">
                      <h3 class="title"><a href="" title=""><?php echo $value->title ?></a></h3>
                      <span class="time"><i
                            class="icon_clock_alt"></i><?php echo date('d', strtotime($value->created_time)) ?>
                        tháng <?php echo date('m', strtotime($value->created_time)) ?>
                        năm <?php echo date('Y', strtotime($value->created_time)) ?></span>
                    </div>
                  </div>
                  <?php
                } else {
                  ?>
                  <div class="item item-normal">
                    <a href="<?php echo getUrlNews($value) ?>" title="<?php echo $value->title ?>" class="img">
                      <img src="<?php echo getImageThumb($value->thumbnail, 100, 100, true) ?>"
                           alt="<?php echo $value->title ?>">
                    </a>
                    <div class="content">
                      <h3 class="title"><a href="<?php echo getUrlNews($value) ?>"
                                           title=""><?php echo $value->title ?> </a></h3>
                      <span class="time"><i
                            class="icon_clock_alt"></i><?php echo date('d', strtotime($value->created_time)) ?>
                        tháng <?php echo date('m', strtotime($value->created_time)) ?>
                        năm <?php echo date('Y', strtotime($value->created_time)) ?></span>
                    </div>
                  </div>
                  <?php
                }
              }
              ?>
            </div>
          </div>
          <?php
        }
        ?>

      </div>
    </div>
  </section>
  <?php
}
?>
<?php
if (!empty($post_featured)) {
  ?>
  <section class="blog-hot">
    <div class="container">
      <h3 class="heading">tin nổi bật</h3>
      <div class="slideBlogHot owl-carousel owl-theme">
        <?php
        foreach ($post_featured as $item) {
          ?>
          <div class="item">
            <a href="<?php echo getUrlNews($item) ?>" title="<?php echo $item->title ?>" class="img">
              <img src="<?php echo getImageThumb($item->thumbnail,460,336,true) ?>" alt="<?php echo $item->title ?>">
            </a>
            <div class="content">
              <h3 class="title"><a href="<?php echo getUrlNews($item) ?>" title=""><?php echo $item->title ?></a></h3>
              <span class="time"><i class="icon_clock_alt"></i>
                <?php echo date('d', strtotime($value->created_time)) ?>
                tháng <?php echo date('m', strtotime($value->created_time)) ?>
                năm <?php echo date('Y', strtotime($value->created_time)) ?>
              </span>
            </div>
          </div>
          <?php
        }
        ?>

      </div>
    </div>
  </section>
  <?php

}
?>

<section class="list-blog-cate">
  <div class="container">
    <div class="row">
      <?php
        if(!empty($data)){
          foreach ($data as $item){
            ?>
            <div class="col-lg-4 col-md-6">
              <div class="item-primary">
                <a href="<?php echo getUrlNews($item) ?>" title="<?php echo $item->title; ?>" class="img">
                  <img src="<?php echo getImageThumb($item->thumbnail,460,490,true) ?>" alt="<?php echo $item->title; ?>">
                </a>
                <div class="content">
                  <h3 class="title"><a href="<?php echo getUrlNews($item) ?>" title="<?php echo $item->title; ?>"><?php echo $item->title; ?></a></h3>
                  <span class="time"><i class="icon_clock_alt"></i>
                    <?php echo date('d', strtotime($value->created_time)) ?>
                    tháng <?php echo date('m', strtotime($value->created_time)) ?>
                    năm <?php echo date('Y', strtotime($value->created_time)) ?>
                  </span>
                </div>
              </div>
            </div>
            <?php
          }
        }else{
          echo '<div class="col-12">Dữ liệu đang được cập nhật.</div>';
        }
      ?>

    </div>
    <div class="pagination-primary">
      <?php
        if(!empty($pagination)){
          echo $pagination;
        }
      ?>

    </div>
  </div>
</section>
<section class="blog-cate-bottom">
  <div class="container">
    <div class="tabs-list-destination">
      <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active" id="nav-hotel-tab" data-toggle="tab" href="#nav-hotel" role="tab"
             aria-controls="nav-hotel" aria-selected="true">Danh sách Hotel/Villa/Resort hot nhất</a>
          <a class="nav-item nav-link" id="nav-destination-tab" data-toggle="tab" href="#nav-destination" role="tab"
             aria-controls="nav-destination" aria-selected="false">Địa điểm du lịch thú vị</a>
        </div>
      </nav>
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="nav-hotel" role="tabpanel" aria-labelledby="nav-hotel-tab">
          <div class="list-item">
            <div class="row">
              <?php
              if(!empty($data_tab['data_tab_1'])) foreach ($data_tab['data_tab_1'] as $item){
                ?>
                <div class="col-lg-3 col-6">
                  <a href="<?php echo getUrlNews($item) ?>" title=""><?php echo $item->title; ?></a>
                </div>
                <?php
              }
              ?>

            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="nav-destination" role="tabpanel" aria-labelledby="nav-destination-tab">
          <div class="list-item">
            <div class="row">
              <?php
                if(!empty($data_tab['data_tab_2'])) foreach ($data_tab['data_tab_2'] as $item){
                  ?>
                  <div class="col-lg-3 col-6">
                    <a href="<?php echo getUrlNews($item) ?>" title=""><?php echo $item->title; ?></a>
                  </div>
                  <?php
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>