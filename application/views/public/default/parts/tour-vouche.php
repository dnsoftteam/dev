<section class="tour-voucher-home">
  <div class="container">
    <div class="title-primary">
      <h2 class="heading">tour & voucher giờ chót</h2>
      <span>Ưu đãi tuyệt nhất trong hôm nay !</span>
    </div>
    <div class="tabs-tour-home">
      <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
          <a class="nav-item nav-link active" id="tour-home-tab" data-toggle="tab" href="#tour-home" role="tab" aria-controls="tour-home" aria-selected="true">Tour</a>
          <a class="nav-item nav-link" id="voucher-profile-tab" data-toggle="tab" href="#voucher-home" role="tab" aria-controls="voucher-home" aria-selected="false">Voucher</a>
        </div>
      </nav>
      <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active" id="tour-home" role="tabpanel" aria-labelledby="tour-home-tab">
          <div class="slide-tour-home dots-blue owl-carousel owl-theme">
            <?php
              if(!empty($list_tour)) foreach ($list_tour as $item){
                $location=getCatByIdTour($item->id);
                ?>
                <div class="item-tour">
                  <div class="img-tour">
                    <?php if(!empty($item->percent)): ?><div class="sale-tour"><span>-<?php echo $item->percent ?>%</span></div><?php endif; ?>
                    <a href="<?php echo getUrlTour($item) ?>" title=""><img src="<?php echo getImageThumb($item->thumbnail,337,218,true) ?>" alt="<?php echo $item->title; ?>"></a>
                    <div class="location-time-tour">
                      <a href="<?php echo getUrlTour($item) ?>" title=""><i class="icon_pin"></i><?php echo !empty($location)?character_limiter($location[0]->title,10,'...'):''; ?></a>
                      <span><i class="icon_clock_alt"></i><?php echo formatTimeLeft(date('Y-m-d h:i:s'),$item->departure_date) ?></span>
                    </div>
                  </div>
                  <div class="content-tour">
                    <h3 class="name-tour"><a href="<?php echo getUrlTour($item) ?>" title=""><?php echo $item->title; ?></a></h3>
                    <span class="time-tour"><?php echo date('d-m-Y',strtotime($item->departure_date)) ?> | <?php echo $item->day ?> ngày</span>
                    <span class="company"><?php echo $item->company ?></span>
                    <div class="price-tour"><b><?php echo number_format($item->price,0,',','.') ?> VNĐ</b> <?php if(!empty($item->price_sale)): ?><small><?php echo number_format($item->price_sale,0,',','.'); ?> VNĐ</small><?php endif; ?></div>
                  </div>
                  <div class="bottom-tour">
                    <a href="" title="" class="compare" data-id="<?php echo $item->id ?>"><img src="<?php echo $this->templates_assets ?>images/icons/icon-ss.png" alt="">so sánh</a>
                    <a class="btn book-now" href="<?php echo site_url('book-tour/'.$item->id) ?>">Đặt ngay</a>
                  </div>
                </div>
                <?php
              }
            ?>
          </div>
          <div class="view-all-primary">
            <a href="<?php echo getUrlPage(array('slug'=>'tour')) ?>" title=""><i class="icon_pin"></i>Xem tất cả các điểm đến</a>
          </div>
        </div>
        <div class="tab-pane fade" id="voucher-home" role="tabpanel" aria-labelledby="voucher-profile-tab">
          <div class="slide-tour-home dots-blue owl-carousel owl-theme">
            <?php
              if(!empty($list_voucher)) foreach ($list_voucher as $item){
                $location=getCatByIdVoucher($item->id);
                ?>
                <div class="item-tour">
                  <div class="img-tour">
                    <?php if(!empty($item->percent)): ?><div class="sale-tour"><span>-<?php echo $item->percent ?>%</span></div><?php endif; ?>
                    <a href="<?php getUrlVoucher($item) ?>" title=""><img src="<?php echo getImageThumb($item->thumbnail,337,218,true) ?>" alt="<?php echo $item->title; ?>"></a>
                    <div class="location-time-tour">
                      <a href="<?php echo getUrlTour($item) ?>" title=""><i class="icon_pin"></i><?php echo !empty($location)?character_limiter($location[0]->title,10,'...'):''; ?></a>
                      <span><i class="icon_clock_alt"></i><?php echo formatTimeLeft(date('Y-m-d h:i:s'),$item->expiration_time) ?></span>
                    </div>
                  </div>
                  <div class="content-tour">
                    <h3 class="name-tour"><a href="<?php getUrlVoucher($item) ?>" title=""><?php echo $item->title; ?></a></h3>
                    <span class="star-tour"><?php echo showRatting($item->ratting); ?></span>
                    <span class="properties" title="<?php echo $item->hotel ?>"><b>Khách sạn :</b> <?php echo character_limiter($item->hotel,25,'...') ?></span>
                    <span class="properties" title="<?php echo $item->address ?>"><b>Địa chỉ :</b> <?php echo character_limiter($item->address,25,'...') ?></span>
                    <div class="price-tour"><b><?php echo number_format($item->price,0,',','.') ?> VNĐ</b> <?php if(!empty($item->price_sale)): ?><small><?php echo number_format($item->price_sale,0,',','.'); ?> VNĐ</small><?php endif; ?></div>
                  </div>
                </div>
                <?php
              }
            ?>
          </div>
          <div class="view-all-primary">
            <a href="<?php echo getUrlPage(array('slug'=>'voucher')) ?>" title=""><i class="icon_pin"></i>Xem tất cả các điểm đến</a>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>
