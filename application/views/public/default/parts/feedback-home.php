<section class="feedback-home">
  <div class="container">
    <div class="title-primary">
      <h2 class="heading">ý kiến khách hàng</h2>
      <span>Những chia sẻ phản hồi của khách hàng về Sharetour </span>
    </div>
    <div class="slideFeedback dots-blue owl-carousel owl-theme">
      <?php
        $feedback=$this->settings['feedback'];
        if(!empty($feedback)) foreach ($feedback as $item){
          ?>
          <div class="item">
            <img src="<?php echo getImageThumb($item['img'],150,150,true) ?>" alt="<?php echo $item['name'] ?>">
            <div class="content">
              <a href="javascript:;"><i class="icon_quotations"></i><?php echo $item['name'] ?></a>
              <span><?php echo $item['content'] ?></span>
            </div>
          </div>
          <?php
        }
      ?>

    </div>
  </div>
</section>
