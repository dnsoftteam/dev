<section class="why-home">
  <div class="container">
    <div class="title-primary dark">
      <h2 class="heading">Tại sao nên đặt với sharetourvn?</h2>
      <span><?php echo $this->settings['taisaochon'] ?></span>
    </div>
    <div class="list-why-home clearfix">
      <h3 class="title">5 lý do để chọn Sharetourvn?</h3>
      <div class="wrap-why">
        <div class="row">
          <?php
            $options=$this->settings['option'];
            if(!empty($options)) foreach ($options as $item){
              ?>
              <div class="item-why col">
                <a href="javascript:;" title="">
                  <img src="<?php echo getImageThumb($item['img']); ?>" alt="<?php echo $item['title'] ?>">
                </a>
                <h4><?php echo $item['title'] ?></h4>
                <span><?php echo $item['content'] ?></span>
              </div>
              <?php
            }
          ?>
        </div>
      </div>

    </div>
  </div>
</section>
