<section class="news-home">
  <div class="container">
    <div class="title-primary">
      <h2 class="heading">Sharetour blog</h2>
      <span>Các ý tưởng xu hướng du lịch , Lịch trình của các chuyến đi mẹo du lịch ! </span>
    </div>
    <div class="list-news-home">
      <div class="row">
        <?php
          if(!empty($list_news)) $newFirst=array_shift($list_news);
          if(!empty($newFirst)){
            ?>
            <div class="col-lg-6">
              <div class="item-hot">
                <a href="<?php echo getUrlNews($newFirst) ?>" title="" class="img">
                  <img src="<?php echo getImageThumb($newFirst->thumbnail,704,380,true) ?>" alt="<?php echo $newFirst->title; ?>">
                </a>
                <div class="content">
                  <h3 class="title"><a href="<?php echo getUrlNews($newFirst) ?>" title=""><?php echo $newFirst->title; ?></a></h3>
                  <span class="desc"><?php echo character_limiter($newFirst->description,200,'...') ?></span>
                  <span class="time"><i class="icon_clock_alt"></i><?php echo timeAgo($newFirst->created_time) ?></span>
                </div>
              </div>
            </div>
            <?php
          }
        if(!empty($list_news)){
            ?>
          <div class="col-lg-6">
            <div class="row">
              <?php foreach ($list_news as $item): ?>
              <div class="col-lg-6">
                <div class="item">
                  <a href="<?php echo getUrlNews($item) ?>" title="" class="img"><img src="<?php echo getImageThumb($item->thumbnail,337,251,true) ?>" alt="<?php echo $item->title; ?>"></a>
                  <div class="content">
                    <h3 class="title"><a href="<?php echo getUrlNews($item) ?>" title=""><?php echo $item->title; ?></a></h3>
                    <span class="time"><i class="icon_clock_alt"></i><?php echo date('d/m/Y',strtotime($item->created_time)) ?></span>
                  </div>
                </div>
              </div>
          <?php endforeach; ?>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
    <div class="view-all-primary">
      <a href="<?php echo getUrlCateNews(array('id'=>23,'slug'=>'tin-tuc')) ?>" title="">Xem tất cả Blog</a>
    </div>
  </div>
</section>