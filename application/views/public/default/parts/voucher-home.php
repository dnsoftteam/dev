<section class="voucher-home">
  <div class="container">
    <div class="title-primary">
      <h2 class="heading">voucher du lịch</h2>
      <span>Dù bất cứ nơi đâu, hãy để sharetour  giúp chuyến đi của bạn trở nên hoàn hảo nhất. </span>
    </div>
    <div class="list-voucher">
      <div class="row">
        <?php
        $i=0;
          if(!empty($list_voucher)) foreach ($list_voucher as $item){
            $location=getCatByIdVoucher($item->id);
            $i++;

            if($i<=2){
              $class='col-lg-6';
              $thumb=getImageThumb($item->thumbnail,705,250,true);
            }else{
              $class='col-lg-4';
              $thumb=getImageThumb($item->thumbnail,460,215,true);
            }
            ?>
            <div class="<?php echo $class ?>">
              <div class="item">
                <div class="img-tour">
                  <a href="<?php echo getUrlVoucher($item) ?>" title=""><img src="<?php echo $thumb ?>" alt=""></a>
                  <div class="location-time-tour">
                    <a href="<?php echo getUrlTour($item) ?>" title=""><i class="icon_pin"></i><?php echo !empty($location)?character_limiter($location[0]->title,10,'...'):''; ?></a>
                    <span><i class="icon_clock_alt"></i><?php echo formatTimeLeft(date('Y-m-d h:i:s'),$item->expiration_time) ?></span>
                  </div>
                </div>
                <div class="content-tour">
                  <h3 class="name-tour"><a href="<?php echo getUrlTour($item) ?>" title=""><?php echo $item->title; ?></a></h3>
                  <div class="star-tour">
                    <?php echo showRatting($item->ratting); ?>
                  </div>
                  <span class="properties"><b>Khách sạn :</b> <?php echo $item->hotel ?></span>
                  <span class="properties"><b>Địa chỉ :</b> <?php echo $item->address ?></span>
                  <div class="price-tour"><b><?php echo number_format($item->price,0,',','.') ?> VNĐ</b> <?php if(!empty($item->price_sale)): ?><small><?php echo number_format($item->price_sale,0,',','.'); ?> VNĐ</small><?php endif; ?></div>
                </div>
              </div>
            </div>
            <?php
            if($i==5) break;
          }
        ?>

      </div>
    </div>
    <div class="view-all-primary">
      <a href="<?php echo getUrlPage(array('slug'=>'voucher')) ?>" title="">Xem tất cả Voucher</a>
    </div>
  </div>
</section>
