<section class="banner-home">
  <div class="slide-home owl-carousel owl-theme">
    <?php
    $lang_code=$this->session->public_lang_code;

      $slides=$this->settings['slides'];
      if(!empty($slides)) foreach ($slides as $item){
       ?>
        <div class="item">
          <a href="<?php echo $item[$lang_code]['link'] ?>" title=""><img src="<?php echo getImageThumb($item[$lang_code]['img'],1920,807,true,'100%') ?>" alt="<?php echo $item[$lang_code]['title'] ?>"></a>
        </div>
        <?php
      }
    ?>

  </div>
  <div class="title-banner-home">
    <h2 class="title">
      Bạn muốn đi đâu
    </h2>
    <span>Giá shock cho 1000 tour trên toàn thế giới </span>
    <div class="search-banner">

      <form method="get" class="form-group" action="<?php echo site_url('search-tour') ?>">
        <input type="text" class="form-control" name="search" autocomplete="off" placeholder="Tìm kiếm địa điểm, hoạt động du lịch hoặc điểm tham quan">
        <button class="btn"><i class="icon_search"></i></button>
      </form>
    </div>
  </div>
</section>