<section class="top-destination-home">
  <div class="container">
    <div class="title-primary">
      <h2 class="heading">điểm đến hàng đầu</h2>
      <span>Khám phá các tour , hoạt động du lịch , điểm đến tham quan cho hành trình du lịch của bạn ! </span>
    </div>
    <div class="slideTopDestination dots-blue owl-carousel owl-theme">
      <?php
      $i=0;
        if(!empty($list_cat_featured)) foreach ($list_cat_featured as $item){
          $i++;
          ?>
            <?php if($i%2==1) echo '<div class="block-des">';?>
            <div class="item">
              <a href="<?php echo getUrlCatTour($item) ?>" title="" class="img">
                <img src="<?php echo getImageThumb($item->thumbnail,337,300,true) ?>" alt="<?php echo $item->title; ?>">
              </a>
              <a href="<?php echo getUrlCatTour($item) ?>" title="" class="location"><i class="icon_pin"></i><?php echo $item->title; ?></a>
            </div>
            <?php if($i%2==0) echo '</div>';?>

          <?php
        }
      if($i%2==1) echo '</div>';
      ?>
    </div>
    <div class="view-all-primary">
      <a href="<?php echo getUrlPage(array('slug'=>'tour')) ?>" title=""><i class="icon_pin"></i>Xem tất cả các điểm đến</a>
    </div>
  </div>
</section>