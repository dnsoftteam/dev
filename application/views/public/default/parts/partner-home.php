<section class="partner-home">
  <div class="container">
    <div class="title-primary">
      <h2 class="heading">Đối tác</h2>
      <span>Sharetour Là đối tác của các công ty du lịch để mang đến cho các bạn chương trình ưu đãi độc quyền ! </span>
    </div>
    <div class="slidePartner dots-blue owl-carousel owl-theme">
      <?php
        $partner=$this->settings['partner'];
        if(!empty($partner)) foreach ($partner as $item){
          ?>
          <div class="item">
            <a href="<?php echo $item['link'] ?>" title=""><img src="<?php echo getImageThumb($item['img']) ?>" alt="<?php echo $item['name'] ?>"></a>
          </div>
          <?php
        }
      ?>

    </div>
  </div>
</section>
