<nav id="menu-mobile" class="mm-menu mm-offcanvas mm-pagedim-black">
    <div class="container">
        <?php echo topnavbar();  ?>
    </div>
</nav>
<div class="wrap">
  <!--Begin: header-->
  <header>
    <div class="wrap-header">
      <div class="top-header">
        <div class="container">
          <div class="wrap-top-hd">
            <div class="socials">
              <ul>
                <li><a href="<?php echo $this->settings['social_fb'] ?>" target="_blank" title="" class="fa fa-facebook"></a></li>
                <li><a href="<?php echo $this->settings['social_twitter'] ?>" target="_blank" title="" class="fa fa-twitter"></a></li>
                <li><a href="<?php echo $this->settings['social_google'] ?>" target="_blank" title="" class="fa fa-google-plus"></a></li>
                <li><a href="<?php echo $this->settings['social_youtube'] ?>" target="_blank" title="" class="fa fa-youtube-play"></a></li>
              </ul>
            </div>
            <div class="info-top-header">
              <ul>
                <li><a href="mailTo:<?php echo $this->settings['email'] ?>" title=""><i class="icon_mail_alt"></i><span><?php echo $this->settings['email'] ?></span></a></li>
                <li><a href="tel:<?php echo $this->settings['hotline'] ?>" title=""><i class="icon_phone"></i><span><?php echo $this->settings['hotline'] ?></span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="bottom-header">
        <div class="container">
          <div class="wrap-bottom-hd">
            <a href="<?php echo base_url() ?>" class="logo"><img src="<?php echo $this->templates_assets ?>images/logo.png" alt="<?php echo $this->settings['name'] ?>"></a>
            <nav>
              <?php echo topnavbar();  ?>
            </nav>
            <a class="book-tour" href="" title=""><i class="fa fa-plane"></i>đặt tour</a>
            <a href="#menu-mobile" class="icon-reponsive">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!--End: header-->
  <main>