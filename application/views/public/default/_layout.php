<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 05/12/2017
 * Time: 4:19 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
$controller = $this->router->fetch_class();
$method = $this->router->fetch_method();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <?php if(!empty($SEO)): ?>
        <title><?php echo isset($SEO['meta_title']) ? $SEO['meta_title'].' - '.$this->settings['name'] :'';?></title>
        <meta name="description" content="<?php echo isset($SEO['meta_description']) ? $SEO['meta_description'] :'';?>" />
        <meta name="keywords" content="<?php echo isset($SEO['meta_keyword']) ? $SEO['meta_keyword'] :'';?>" />
        <!--Meta Facebook Page Other-->
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="<?php echo isset($SEO['meta_title']) ? $SEO['meta_title'].' - '.$this->settings['name'] : '';?>" />
        <meta property="og:description"   content="<?php echo isset($SEO['meta_description']) ? $SEO['meta_description'] :'';?>" />
        <meta property="og:image"         content="<?php echo isset($SEO['image']) ? $SEO['image'] : '';?>" />
        <meta property="og:url"           content="<?php echo isset($SEO['url'])?$SEO['url']:base_url(); ?>" />
        <!--Meta Facebook Page Other-->
        <link rel="canonical" href="<?php echo isset($SEO['url'])?$SEO['url']:base_url(); ?>" />
    <?php else: ?>
        <title><?php echo isset($SEO['meta_title']) ? $SEO['meta_title'].' - '.$this->settings['title'] : isset($this->settings['title'])?$this->settings['title']. ' - ' .$this->settings['name']:'';?></title>
        <meta name="description" content="<?php echo isset($SEO['meta_description']) ? $SEO['meta_description'] : isset($this->settings['meta_desc'])?$this->settings['meta_desc']:'';?>" />
        <meta name="keywords" content="<?php echo isset($SEO['meta_keyword']) ? $SEO['meta_keyword'] : isset($this->settings['meta_keyword'])?$this->settings['meta_keyword']:'';?>" />
        <!--Meta Facebook Homepage-->
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="<?php echo isset($this->settings['title'])?$this->settings['title'].' | '.$this->settings['name']:'';?>" />
        <meta property="og:description"   content="<?php echo isset($this->settings['meta_desc'])?$this->settings['meta_desc']:'';?>" />
        <meta property="og:image"         content="<?php echo getImageThumb('',400,200);?>" />
        <meta property="og:url"           content="<?php echo base_url(); ?>" />
        <!--Meta Facebook Homepage-->
        <link rel="canonical" href="<?php echo base_url(); ?>" />
    <?php endif; ?>
    <?php $asset_css[] = 'bootstrap.min.css'; ?>
    <?php $asset_css[] = 'jquery-ui.css'; ?>
  <?php $asset_css[] = '../plugins/jssocials/dist/jssocials.css'; ?>
  <?php $asset_css[] = '../plugins/jssocials/dist/jssocials-theme-flat.css'; ?>

    <?php $asset_css[] = '../fonts/font-awesome/css/font-awesome.min.css'; ?>
    <?php $asset_css[] = '../fonts/themify-icons/themify-icons.css'; ?>
    <?php $asset_css[] = '../fonts/elegantIcon/elegantIcon.css'; ?>
    <?php $asset_css[] = 'owl.carousel.min.css'; ?>
    <?php $asset_css[] = 'owl.theme.default.min.css'; ?>
    <?php $asset_css[] = 'jquery.mmenu.all.css'; ?>
    <?php $asset_css[] = 'jquery.mmenu.themes.css'; ?>
<!--    --><?php //$asset_css[] = 'jquery.fancybox.min.css'; ?>
  <?php $asset_css[] = '../plugins/select2/dist/css/select2.min.css'; ?>
  <?php $asset_css[] = 'slick.css'; ?>
  <?php $asset_css[] = 'slick-theme.css'; ?>
  <?php $asset_css[] = 'animate.css'; ?>
    <?php $asset_css[] = '../plugins/toastr/toastr.min.css'; ?>
    <?php $asset_css[] = 'main.css'; ?>
    <?php $asset_css[] = 'custom.css'; ?>

    <?php
    $this->minify->css($asset_css);
    echo $this->minify->deploy_css(TRUE);
    ?>
<!--    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=vietnamese" rel="stylesheet">-->
    <link rel="icon" href="<?php echo !empty($this->settings['favicon'])?getImageThumb($this->settings['favicon'],32,32):base_url("/public/favicon.ico");?>" sizes="32x32">
    <link rel="icon" href="<?php echo !empty($this->settings['favicon'])?getImageThumb($this->settings['favicon'],192,192):base_url("/public/favicon.ico");?>" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="<?php echo !empty($this->settings['favicon'])?getImageThumb($this->settings['favicon'],180,180):base_url("/public/favicon.ico");?>">
    <meta name="msapplication-TileImage" content="<?php echo !empty($this->settings['favicon'])?getImageThumb($this->settings['favicon'],270,270):base_url("/public/favicon.ico");?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        var urlCurrentMenu = window.location.href,
            urlCurrent = window.location.href,
            base_url = '<?php echo base_url(); ?>',
            media_url = '<?php echo MEDIA_URL.'/'; ?>',
            currency_code = '<?php echo $this->session->currency_code; ?>',
            csrf_cookie_name = '<?php echo $this->config->item('csrf_cookie_name') ?>',
            csrf_token_name = '<?php echo $this->security->get_csrf_token_name() ?>',
            url_load_compase='<?php echo site_url('home/loadcompase') ?>',
            url_add_compase='<?php echo site_url('home/addcompase') ?>',
            csrf_token_hash = '<?php echo $this->security->get_csrf_hash() ?>';
    </script>
    <?php echo !empty($this->settings['script_head']) ? $this->settings['script_head'] : '' ?>
</head>
<body>

    <div class="wrap">
        <?php $this->load->view($this->template_path.'_header') ?>
        <!-- Content Wrapper. Contains page content -->
        <main>
            <?php echo !empty($main_content)?$main_content:'' ?>
        </main>
        <!-- /.content-wrapper -->
        <?php $this->load->view($this->template_path.'_footer') ?>
    </div>
<!-- ./wrapper -->
<!--    <script src="https://maps.google.com/maps/api/js?key=--><?php //echo GG_KEY; ?><!--&sensor=false" type="text/javascript"></script>-->
    <?php $asset_js[] = 'jquery.js'; ?>
    <?php $asset_js[] = 'jquery-ui.js'; ?>
    <?php $asset_js[] = 'tether.min.js'; ?>
    <?php $asset_js[] = 'bootstrap.min.js'; ?>
    <?php $asset_js[] = 'jquery-migrate-1.2.1.min.js';?>
    <?php $asset_js[] = 'jquery.mmenu.all.js';?>
    <?php $asset_js[] = 'owl.carousel.min.js'; ?>
    <?php $asset_js[] = 'wow.min.js'; ?>
    <?php $asset_js[] = 'scrollspy.js'; ?>
    <?php $asset_js[] = '../plugins/toastr/toastr.min.js'; ?>
    <?php $asset_js[] = 'jquery.form.min.js'; ?>
    <?php $asset_js[] = '../plugins/jssocials/dist/jssocials.min.js'; ?>
    <?php $asset_js[] = 'jquery.sticky-kit.js'; ?>
    <?php $asset_js[] = '../plugins/select2/dist/js/select2.js'; ?>
    <?php $asset_js[] = 'slick.min.js'; ?>
    <?php $asset_js[] = 'common.js'; ?>
    <?php $asset_js[] = 'slide.js'; ?>
    <?php $asset_js[] = 'custom.js'; ?>

<?php $this->minify->js($asset_js); echo $this->minify->deploy_js();  ?>

    <script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript">
    $(document).ready(function() {
        toastr.options.escapeHtml = true;
        toastr.options.closeButton = true;
        toastr.options.positionClass = "toast-bottom-right";
        toastr.options.timeOut = 5000;
        toastr.options.showMethod = 'fadeIn';
        toastr.options.hideMethod = 'fadeOut';
        <?php if(!empty($this->_message)): ?>
        console.log('<?php echo json_encode($this->_message) ?>');
        toastr.<?php echo $this->_message['type']; ?>('<?php echo trim(strip_tags($this->_message['message'])); ?>');
        <?php endif; ?>
        setTimeout(sdkInit(),1000);
    });

    function sdkInit(){
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.10";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }
</script>
    <?php echo !empty($this->settings['script_body']) ? $this->settings['script_body'] : '' ?>
</body>
</html>