<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 13/01/2018
 * Time: 2:07 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
if(!empty($oneItem)):
?>
    <section class="page-banner">
        <div class="page-banner__bg">
            <img src="<?php echo $this->templates_assets.'images/bg-product-cate.jpg' ?>" alt="<?php echo getTitle($oneItem,$this->settings) ?>">
        </div>
        <div class="page-banner__title-wrap">
            <div class="container">
                <h2 class="page-banner__title">Search: <?php echo $oneItem->title ?></h2>
            </div>
        </div>
    </section>
    <section class="main-page bg-white">
        <nav class="breadcrumb-wrapper" aria-label="breadcrumb">
            <div class="container">
                <?php echo !empty($breadcrumb) ? $breadcrumb : '' ?>
            </div>
        </nav>
        <div class="container page-details-wrap">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs mb-3">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home"><?php echo lang('head_product_search') ?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#menu1"><?php echo lang('head_news_search') ?></a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active container project-cate clearfix" id="home">
                    <div class="list-project-cate project-home">
                        <div class="row">
                            <?php if(!empty($list_product)) foreach ($list_product as $item): ?>
                                <div class="col-lg-4 col-12">
                                    <div class="item item-hot">
                                        <a href="<?php echo getUrlProduct($item) ?>" title="<?php echo getTitle($item,$this->settings) ?>" class="img">
                                            <img src="<?php echo getImageThumb($item->thumbnail,368,276); ?>" alt="<?php echo getTitle($item,$this->settings) ?>">
                                        </a>
                                        <div class="title">
                                            <h3><a href="<?php echo getUrlProduct($item) ?>" title="<?php echo getTitle($item,$this->settings) ?>"><?php echo $item->title ?></a></h3>
                                            <div class="content">
                                                <a href="<?php echo getUrlProduct($item) ?>" title="<?php echo getTitle($item,$this->settings) ?>" class="view-details">
                                                    <?php echo lang('text_view_detail') ?> <i class="arrow_right"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;else echo "<div class='text-center col-md-12'>".lang('text_data_empty')."</div>"; ?>
                        </div>
                        <div class="pagination-primary">
                            <?php echo !empty($pagination) ? $pagination : '' ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane container" id="menu1">
                    <div class="news-home page-news-cate">
                        <div class="row">
                            <?php if(!empty($list_post)) foreach ($list_post as $item): ?>
                                <div class="col-lg-3 col-md-6">
                                    <div class="item item-normal">
                                        <a href="<?php echo getUrlNews($item) ?>" title="<?php echo getTitle($item,$this->settings) ?>" class="img">
                                            <img src="<?php echo getImageThumb($item->thumbnail,268,274) ?>" alt="<?php echo getTitle($item,$this->settings) ?>">
                                        </a>
                                        <div class="title">
                                            <h3><a href="<?php echo getUrlNews($item) ?>" title="<?php echo getTitle($item,$this->settings) ?>"><?php echo $item->title ?></a></h3>
                                            <span class="desc"><?php echo $item->description ?></span>
                                        </div>
                                        <div class="time-tag">
                                            <div class="clearfix">
                                                <span><i class="icon_clock_alt"></i><?php echo timeAgo($item->created_time, 'd/m/Y H:i:s') ?></span>
                                            </div>
                                            <a href="<?php echo getUrlNews($item) ?>" title="<?php echo getTitle($item,$this->settings) ?>" class="view"><i class="ti-angle-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;else echo "<div class='text-center col-md-12'>".lang('text_data_empty')."</div>"; ?>
                        </div>
                        <div class="pagination-primary">
                            <?php echo !empty($pagination) ? $pagination : '' ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif ?>