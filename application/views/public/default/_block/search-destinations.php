<?php
  if(!empty($home)){
    $class='home goIn';
    $row='row5';
  }else{
    $class='';
    $row='';
  }
?>
<div class="search-destinations <?php echo $class ?>">
  <div class="row <?php echo $row ?>">
    <div class="col-lg-3">
      <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">

        <a class="nav-link active" id="des-home-tab" data-toggle="pill" href="#des-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Điểm đến hàng đầu</a>
        <?php
          $list_cat=getCatByParent(0,'national');
          $listFeatured=getCatTourFeatured();
          $list_id_cat=array();
        ?>
        <?php
        if(!empty($list_cat)) foreach ($list_cat as $item):
          $list_id_cat[]=$item->id;
          ?>
        <a class="nav-link" id="des-<?php echo $item->id ?>-tab" data-toggle="pill" href="#des-<?php echo $item->id ?>" role="tab" aria-controls="v-pills-messages" aria-selected="false"><?php echo $item->title ?><span><?php echo $item->description ?></span></a>
        <?php endforeach; ?>
      </div>
    </div>
    <div class="col-lg-9">
      <div class="tab-content search-des" id="v-pills-tabContent">
        <div class="tab-pane show active" id="des-home" role="tabpanel" aria-labelledby="des-home">
          <div class="destinations-hot">
            <div class="row <?php echo $row ?>">
              <?php
                if(!empty($listFeatured)) foreach ($listFeatured as $item){
                  ?>
                  <div class="col-lg-3 col-md-4 col-6">
                    <div class="item">
                      <a href="<?php echo getUrlCatTour($item) ?>" title="" class="img"><img src="<?php echo getImageThumb($item->thumbnail,245,121,true) ?>" alt=""></a>
                      <a href="<?php echo getUrlCatTour($item) ?>" title="" class="location"><?php echo $item->title ?></a>
                    </div>
                  </div>
                  <?php
                }
              ?>

            </div>
          </div>
        </div>
        <?php
        if(!empty($list_id_cat)) foreach ($list_id_cat as $item){
          $list_cat_child_1=getCatByParent($item,'national');
          ?>
          <div class="tab-pane" id="des-<?php echo $item ?>" role="tabpanel" aria-labelledby="des-<?php echo $item ?>-tab">
            <div class="list-des-area">
              <?php
                if(!empty($list_cat_child_1)) foreach ($list_cat_child_1 as $item1){
                  $list_cat_child_2=getCatByParent($item1->id,'national');
                  ?>
                  <div class="item">
                    <div class="left-item">
                      <?php echo $item1->title; ?>
                    </div>
                    <div class="right-item">
                      <?php if(!empty($list_cat_child_2)) foreach ($list_cat_child_2 as $item2){
                        echo '<a href="'.getUrlCatTour($item2).'" title="">'.$item2->title.'</a>';
                      } ?>
                    </div>
                  </div>
                  <?php
                }
              ?>

            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</div>