<div class="popup-compare-tour goIn">
  <h4>So sánh tour</h4>
  <div id="k-info-compare">
    <?php
    if (!empty($this->session->userdata['compare'])) {
      foreach ($this->session->userdata['compare'] as $key => $value) {
        $tour = getTourById($value);
        if(!empty($tour)) echo '<a href="' . getUrlTour($tour) . '" title="">' . $tour->title . '</a>';
      }
    }else{
      echo '<span><b>Chưa có tour nào được so sánh.</b></span>';
    }
    ?>
  </div>


  <span>(Bạn có thể so sánh tối đa 3 tour cho mỗi lần.)</span>
  <div class="bt-compare">
    <?php

      if(!empty($this->input->post('id'))){
        $id=$this->input->post('id');
      }else{
        $id='';
      }
      if(!empty($this->session->userdata['compare']) && in_array($id,$this->session->userdata['compare'])){
        ?>
        <a class="btn viewCompare" href="<?php echo site_url('compare') ?>">Chi tiết</a>
        <?php
      }else{
        ?>
        <a class="btn addCompare" href="javascript:;" data-id="<?php echo $id ?>">So sánh</a>
        <?php
      }
    ?>
    <button class="btn close-popup">Hủy</button>
  </div>
</div>