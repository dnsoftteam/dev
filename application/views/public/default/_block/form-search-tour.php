<div class="search-tour-banner">
  <div class="form-group item1">
    <label>Điểm bắt đầu</label>
    <div>
      <select name="departure" class="form-control" onchange="search_tour(this)">
        <option value="<?php echo remove_query_arg('departure') ?>">Chọn điểm bắt đầu</option>
        <option value="<?php echo add_query_arg(array('departure'=>'Hà Nội')) ?>" <?php echo show_selected('departure','Hà Nội') ?>>Hà Nội</option>
        <option value="<?php echo add_query_arg(array('departure'=>'TP.HCM')) ?>" <?php echo show_selected('departure','TP.HCM') ?>>TPHCM</option>
        <option value="<?php echo add_query_arg(array('departure'=>'Đà Nẵng')) ?>" <?php echo show_selected('departure','Đà Nẵng') ?>>Đà Nẵng</option>
      </select>
    </div>
    <i class="icon_pin"></i>
    <span class="arrow_triangle-down"></span>
  </div>
  <div class="form-group item2">
    <label>Độ dài tour</label>
    <div>
      <select name="tour_length" class="form-control" onchange="search_tour(this)">
        <option value="<?php echo remove_query_arg('tour_length') ?>">Chọn độ dài tour</option>
        <option value="<?php echo add_query_arg(array('tour_length'=>6)) ?>" <?php echo show_selected('tour_length',6) ?>>6 ngày 5 đêm</option>
        <option value="<?php echo add_query_arg(array('tour_length'=>5)) ?>" <?php echo show_selected('tour_length',5) ?>>5 ngày 4 đêm</option>
        <option value="<?php echo add_query_arg(array('tour_length'=>4)) ?>" <?php echo show_selected('tour_length',4) ?>>4 ngày 3 đêm</option>
        <option value="<?php echo add_query_arg(array('tour_length'=>3)) ?>" <?php echo show_selected('tour_length',3) ?>>3 ngày 2 đêm</option>
        <option value="<?php echo add_query_arg(array('tour_length'=>2)) ?>" <?php echo show_selected('tour_length',2) ?>>2 ngày 1 đêm</option>
      </select>
    </div>
    <i class="icon_clock_alt"></i>
    <span class="arrow_triangle-down"></span>
  </div>
  <div class="form-group item3">
    <label>tháng</label>
    <div>
      <select name="departure_date" class="form-control" onchange="search_tour(this)">
        <option value="<?php echo remove_query_arg('departure_date') ?>">Chọn tháng</option>
        <?php
        echo '<option value="'.add_query_arg(array('departure_date'=>date('Y-m'))).'" '.show_selected('departure_date',date('Y-m')).'>'.date('m-Y').'</option>';

        $date=date_create(date('Y-m-d'));
          for ($i=1;$i<=12; $i++){
            $nextDate=date_format(date_modify($date, "+1 month"),'m-Y');
            $nextDateVal=date_format(date_modify($date, "+0 month"),'Y-m');
            $lelected=show_selected('departure_date',$nextDateVal);
            echo '<option value="'.add_query_arg(array('departure_date'=>$nextDateVal)).'" '.$lelected.'>'.$nextDate.'</option>';
          }
        ?>
      </select>
    </div>
    <i class="icon_calendar"></i>
    <span class="arrow_triangle-down"></span>
  </div>
  <div class="form-group item4">
    <label>mức giá</label>
    <div>
      <select name="order_price"  class="form-control" onchange="search_tour(this)">
        <option value="<?php echo add_query_arg(array('order_price'=>'DESC')) ?>" <?php echo show_selected('order_price','DESC') ?>>Từ thấp đến cao</option>
        <option value="<?php echo add_query_arg(array('order_price'=>'ASC')) ?>" <?php echo show_selected('order_price','ASC') ?>>Từ cao đến thấp</option>
      </select>
    </div>
    <i class="icon_wallet"></i>
    <span class="arrow_triangle-down"></span>
  </div>
</div>