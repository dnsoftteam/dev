<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 10/01/2018
 * Time: 3:53 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $listFooter = $this->settings['block']['footer'][$this->session->public_lang_code]; ?>
<section class="page-bottom bg-white">
    <div class="container">
        <div class="row icon-box-row">
            <?php if(!empty($listFooter)) foreach ($listFooter as $k => $item): $item = (object) $item ?>
                <div class="col-12 col-md-4 col-lg icon-box-col">
                    <div class="icon-box">
                        <div class="icon-box__icon">
                            <img src="<?php echo getImageThumb($item->icon,37,37) ?>" alt="<?php echo $item->title ?>">
                        </div>
                        <div class="icon-box__text">
                            <div class="icon-box__title"><?php echo $item->title ?></div>
                            <span class="icon-box__desc"><?php echo $item->desc ?></span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
