<section class="register-home">
  <div class="container">
    <div class="form-register">
      <span>Nhận thông tin Các deal du lịch giảm giá sẽ được gửi đến hòm thư của bạn</span>
      <form action="<?php echo site_url('contact/subscriber') ?>" class="form-group" method="post">
        <input type="text" name="email" class="form-control" placeholder="Nhập địa chỉ email của bạn">
        <button class="btn" type="submit"><i class="fa fa-paper-plane"></i></button>
      </form>
    </div>
  </div>
</section>
</main>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <div class="ft-item">
          <a href="" title="" class="logo-ft"><img src="<?php echo $this->templates_assets ?>images/logo-ft.png" alt="<?php echo $this->settings['name'] ?>"></a>
          <h2 class="name-company"><?php echo $this->settings['name_company'] ?></h2>
          <p><i class="icon_pin"></i>Trụ sở : <?php echo $this->settings['truso'] ?></p>
          <p><i class="icon_phone"></i>Hotline (24/7): <?php echo $this->settings['hotline'] ?></p>
          <p><i class="icon_mail_alt"></i>Email: <?php echo $this->settings['email'] ?></p>
          <p><i class="icon_link"></i>Website: <a href="<?php echo $this->settings['website'] ?>" title=""><?php echo $this->settings['website'] ?></a></p>
          <p><i class="icon_map"></i><a href="<?php echo $this->settings['link_map'] ?>" title="">Xem bản đồ</a></p>
          <a href="<?php echo getUrlPage(array('id'=>5)) ?>" title="" class="bt-chht">
            <img src="<?php echo $this->templates_assets ?>images/bt-chht.png" alt="cơ hội hợp tác">
            cơ hội hợp tác
          </a>
        </div>
      </div>
      <div class="col-lg-2 col-6">
        <div class="ft-item">
          <h3 class="heading-ft">Tour nước ngoài</h3>
          <?php echo footer1() ?>
        </div>
      </div>
      <div class="col-lg-2 col-6">
        <div class="ft-item">
          <h3 class="heading-ft">Tour khu vực</h3>
          <?php echo footer2() ?>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="ft-item ft-item-last">
          <h3 class="heading-ft">Tìm chúng tôi trên:</h3>
          <div class="social-ft">
            <a href="" title=""><i class="social_facebook"></i></a>
            <a href="" title=""><i class="social_twitter"></i></a>
            <a href="" title=""><i class="social_youtube"></i></a>
          </div>
          <?php
            echo $this->settings['fanpage'];
          ?>
          <div class="copy-right">
            © 2018 Sharetourvn. All Rights Reserved. Designed by <a href="http://apecsoft.asia/" target="_blank" title="">Apecsoft</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
</div>
