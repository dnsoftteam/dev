<section class="banner-primary voucher">
  <div class="title-banner-page">
    <h1 class="heading"><?php echo $oneItem->title ?></h1>
    <nav aria-label="breadcrumb">
      <?php echo $breadcrumb; ?>
    </nav>
  </div>
</section>
<section class="page-voucher">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <div class="list-des-voucher">
          <ul>
            <?php
              $data_cat=getCatVoucher();
              if(!empty($data_cat)) foreach ($data_cat as $item){
                ?>
                <li class=""><a href="<?php echo getUrlCatVoucher($item) ?>" title=""><?php echo $item->title ?></a></li>
                <?php
              }
            ?>
          </ul>
        </div>
        <div class="list-voucher" id="list-voucher">
          <?php
            if(!empty($data_voucher['data']))
              echo $this->load->view($this->template_path.'voucher/_content',array('data'=>$data_voucher['data']),TRUE);
          ?>
        </div>
        <?php
        if($data_voucher['total'] > 1):
          ?>
          <div class="view-all-primary">
            <a href="javascript:;" title="" onclick="panigation_review('list-voucher')" data-page="2" id="k_panigation" data-total="<?php echo $data_voucher['total'] ?>" data-cat="">
              Hiển thị thêm <i class="arrow_down"></i>
              <span class="icon_load" style="margin-left: 10px; display: none;"><i class="fa fa-spinner fa-spin" style=""></i></span>
            </a>
          </div>
        <?php endif; ?>
      </div>
      <div class="col-lg-4">
        <?php $this->load->view($this->template_path.'/voucher/block-featured'); ?>
      </div>
    </div>
  </div>
</section>
<script !src="">
  var ajax_load_panigation='<?php echo site_url('voucher/ajax_panigation') ?>';
</script>