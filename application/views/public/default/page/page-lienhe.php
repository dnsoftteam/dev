<?php if (!empty($oneItem)): ?>
    <section class="page-banner">
        <div class="page-banner__bg">
            <img src="<?php echo !empty($oneItem->banner) ? getImageThumb($oneItem->banner) : $this->templates_assets.'images/bg-contact.jpg' ?>" alt="<?php echo getTitle($oneItem,$this->settings) ?>">
        </div>
        <div class="page-banner__title-wrap">
            <div class="container">
                <h2 class="page-banner__title"><?php echo $oneItem->title ?></h2>
            </div>
        </div>
    </section>

    <section class="bg-white">
        <nav class="breadcrumb-wrapper" aria-label="breadcrumb">
            <div class="container">
                <?php echo !empty($breadcrumb) ? $breadcrumb : '' ?>
            </div>
        </nav>
        <div class="page-contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="info-contact">
                            <div class="title-primary contact">
                                <h2><?php echo lang('head_company_info') ?></h2>
                                <small></small>
                            </div>
                            <div id="accordion" class="accordion-contact">
                                <?php $showroom = $this->settings['showroom'][$this->session->public_lang_code]; ?>
                                <?php if(!empty($showroom)) foreach ($showroom as $k => $item): $item = (object) $item; ?>
                                    <div class="card">
                                        <div class="card-header">
                                            <a class="card-link" onclick="showMap(<?php echo $k ?>)" data-toggle="collapse" href="#add<?php echo $k + 1 ?>">
                                                <img src="<?php echo $this->templates_assets ?>images/icon-add<?php echo $k + 1 ?>.png" alt="<?php echo $item->title ?>"><?php echo $item->title ?>
                                            </a>
                                        </div>
                                        <div id="add<?php echo $k + 1 ?>" class="collapse <?php echo $k == 0 ? 'show' : '' ?>" data-parent="#accordion">
                                            <div class="card-body">
                                                <p><strong><?php echo lang('form_text_address') ?>: </strong><?php echo $item->address ?></p>
                                                <p><strong><?php echo lang('form_text_phone') ?>: </strong><?php echo $item->phone ?></p>
                                                <p><strong>Fax: </strong><?php echo $item->fax ?></p>
                                                <p><strong>Email: </strong><?php echo !empty($item->email) ? $item->email :'' ?></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <div class="form-contact">
                                <h2><?php echo lang('head_contact_with') ?></h2>
                                <?php echo form_open(''); ?>
                                <div class="row row8">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="text" name="fullname" value="<?php echo set_value('fullname'); ?>" class="form-control" placeholder="<?php echo lang('text_fullname') ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input type="text"name="email" value="<?php echo set_value('email'); ?>" class="form-control" placeholder="Email*">
                                            <?php echo form_error('email'); ?>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <input type="text" name="title" value="<?php echo set_value('title'); ?>" class="form-control" placeholder="<?php echo lang('form_text_title') ?>*">
                                            <?php echo form_error('title'); ?>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea name="content" class="form-control" rows="4" placeholder="<?php echo lang('form_text_content') ?>*"><?php echo set_value('fullname'); ?></textarea>
                                            <?php echo form_error('content'); ?>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="captcha">
                                            <div class="g-recaptcha" data-sitekey="<?php echo GG_CAPTCHA_SITE_KEY ?>"></div>
                                            <?php echo form_error('g-recaptcha-response'); ?>
                                        </div>
                                        <button class="view-all" type="submit">
                                            <span><?php echo lang('form_button_send') ?></span>
                                        </button>
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div id="page-map" class="map-contact"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
    var google_maps_lat = '<?php echo $this->settings['contact']['maps_latitude'] ?>';
    var google_maps_long = '<?php echo $this->settings['contact']['maps_longitude'] ?>';
    var dataMap = {};
</script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
<?php endif ?>