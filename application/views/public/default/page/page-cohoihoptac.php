<?php if (!empty($oneItem)) : ?>
<main>
    <section class="banner-primary chht">
        <div class="title-banner-page">
            <h2 class="heading"><?php echo $oneItem->title ?></h2>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page"><?php echo $oneItem->title ?></li>
                </ol>
            </nav>
        </div>
    </section>
    <section class="chht">
        <div class="chht-slogan">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 offset-lg-1">
                        <div class="item">
                            <?php echo $oneItem->content ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="chht-facility">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 bd-right">
                        <div class="item">
                            <div class="img">
                                <img src="<?php echo site_url()?>public/images/chht1.png" alt="">
                            </div>

                            <div class="content">
                                <b>Miễn phí quảng cáo, không có điều kiện rắc rối</b>
                                <span>Không phí đăng ký, không phí thành viên, không cắt giá phòng của bạn. Thật sự là không</span>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 bd-right">
                        <div class="item">
                            <div class="img">
                                <img src="<?php echo site_url()?>public/images/chht2.png" alt="">
                            </div>
                            <div class="content">
                                <b>Độ phủ sóng rộng</b>
                                <span>Chúng tôi nhận được hàng triệu đơn đặt phòng mỗi tháng từ du khách tại hơn 200 quốc gia, tất cả đều tìm kiếm những chỗ ở độc đáo như của bạn</span>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="item">
                            <div class="img">
                                <img src="<?php echo site_url()?>public/images/chht3.png" alt="">
                            </div>
                            <div class="content">
                                <b>Trợ giúp 24/7 bằng chính ngôn ngữ của bạn</b>
                                <span>Đội ngũ nhân viên hỗ trợ của chúng tôi luôn sẵn sàng trợ giúp cho bạn và khách hàng của bạn bằng 17 thứ tiếng khác nhau</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="chht-youdo">
            <div class="container">
                <h3>Tất cả những gì bạn cần làm là?</h3>
                <div class="row">
                    <div class="col-md-3 bd-right">
                        <div class="item">
                            <div class="icon">
                                <img src="<?php echo site_url()?>public/images/icon-chht1.png" alt="">
                            </div>
                            <strong>Liên hệ với chúng tôi</strong>
                        </div>
                    </div>
                    <div class="col-md-3 bd-right">
                        <div class="item">
                            <div class="icon">
                                <img src="<?php echo site_url()?>public/images/icon-chht2.png" alt="">
                            </div>
                            <strong>Gửi thông tin tour cho chúng tôi hàng tuần </strong>
                        </div>
                    </div>
                    <div class="col-md-3 bd-right">
                        <div class="item">
                            <div class="icon">
                                <img src="<?php echo site_url()?>public/images/icon-chht3.png" alt="">
                            </div>
                            <strong>Cập nhật tour - thông báo số lượng khách </strong>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="item">
                            <div class="icon">
                                <img src="<?php echo site_url()?>public/images/icon-chht4.png" alt="">
                            </div>
                            <strong>Xem dịch vụ của bạn được quảng cáo đến hàng triệu người .</strong>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
        <div class="chht-contact">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 offset-lg-1">
                        <div class="info-contact">
                            <h4>thông tin liên hệ</h4>
                            <p><strong>Công ty TNHH dịch vụ Sharetour</strong></p>
                            <p><i class="icon_pin"></i>Trụ sở : T1-18-05 458 Minh khai Hà Nội</p>
                            <p><i class="icon_phone"></i>Hotline: 0888.886.038</p>
                            <p><i class="icon_mail"></i>Email: Manhthangvu888@gmaill.com</p>
                            <p>
                                <i class="icon_link"></i>
                                Mạng xã hội: 
                                <a href="" title=""><span class="social_facebook"></span></a>
                                <a href="" title=""><span class="social_twitter"></span></a>
                                <a href="" title=""><span class="social_youtube"></span></a>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <?php echo form_open('contact/submit',['class'=>'sidebar-advisory-home']) ?>
                        <div class="form-contact">
                            <h4>Gửi email tới chúng tôi</h4>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="addresscity" placeholder="Tên công ty">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="tel" class="form-control" name="telephone" placeholder="Số điện thoại">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="address" placeholder="Địa chỉ">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="title" placeholder="Tiêu đề">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea rows="3" class="form-control" name ="contact" placeholder="Nội dung"></textarea>
                                    </div>
<!--                                    <div class="captcha">-->
<!--                                            <div class="g-recaptcha" data-sitekey="--><?php //echo GG_CAPTCHA_SITE_KEY ?><!--"></div>-->
<!--                                            --><?php //echo form_error('g-recaptcha-response'); ?>
<!--                                        </div><br><br>-->
                                </div>

                                <div class="col-12">
                                    <button type="submit" class="btn">Gửi</button>
                                </div>
                            </div>

                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="chht-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.852585318181!2d105.86597091544357!3d20.998545494183876!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac04389d339d%3A0x7a1fd794153de5d0!2zNDU4IE1pbmggS2hhaSwgVsSpbmggVHV5LCBIYWkgQsOgIFRyxrBuZywgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1528340528637" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </section>

</main>
<?php endif; ?>
