<section class="banner-primary">
  <div class="title-banner-page">
    <h1 class="heading"><?php echo $oneItem->title ?></h1>
    <nav aria-label="breadcrumb">
      <?php echo $breadcrumb ?>
    </nav>
  </div>
</section>
<section class="page-tour-cate">
  <div class="container">
    <div class="title-tour">
      <h2 class="heading-tour">Du lịch nước ngoài</h2>
      <span>
        <?php echo $oneItem->content ?>
      </span>
      <div class="social-tour" id="shareNative">
      </div>
    </div>
    <?php $this->load->view($this->template_path.'_block/search-destinations'); ?>
  </div>
</section>
<section class="tour-great">
  <div class="container">
    <div class="title-primary">
      <h2 class="heading">tour tiêu biểu</h2>
      <span>Những tour tiêu biểu được nhiều người lựa chọn</span>
    </div>
    <div class="list-tour-great">
      <div class="row">
        <?php
          if(!empty($data_tour)) foreach ($data_tour as $item){
            $location=getCatByIdTour($item->id);
            ?>
            <div class="col-lg-3 col-md-6">
              <div class="item-tour">
                <div class="img-tour">
                  <?php if(!empty($item->percent)): ?><div class="sale-tour"><span>-<?php echo $item->percent ?>%</span></div><?php endif; ?>
                  <a href="<?php echo getUrlTour($item) ?>" title=""><img src="<?php echo getImageThumb($item->thumbnail,337,218,true) ?>" alt="<?php echo $item->title; ?>"></a>
                  <div class="location-time-tour">
                    <a href="<?php echo getUrlTour($item) ?>" title=""><i class="icon_pin"></i><?php echo !empty($location)?$location[0]->title:''; ?></a>
                  </div>
                </div>
                <div class="content-tour">
                  <h3 class="name-tour"><a href="<?php echo getUrlTour($item) ?>" title=""><?php echo $item->title; ?></a></h3>
                  <span class="time-tour"><?php echo date('d-m-Y',strtotime($item->departure_date)) ?> | <?php echo $item->day ?> ngày</span>
                  <span class="company"><?php echo $item->company ?></span>
                  <div class="price-tour"><b><?php echo number_format($item->price,0,',','.') ?> VNĐ</b> <?php if(!empty($item->price_sale)): ?><small><?php echo number_format($item->price_sale,0,',','.'); ?> VNĐ</small><?php endif; ?></div>
                </div>
                <div class="bottom-tour">
                  <a href="javascript:;" title="" class="compare" data-id="<?php echo $item->id ?>"><img src="<?php echo $this->templates_assets ?>images/icons/icon-ss.png" alt="">so sánh</a>
                  <a class="btn book-now" href="<?php echo site_url('book-tour/'.$item->id) ?>">Đặt ngay</a>
                </div>
              </div>
            </div>
            <?php
          }
        ?>

      </div>
    </div>
  </div>
</section>