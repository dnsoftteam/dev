<section class="banner-primary">
  <div class="title-banner-page">
    <h1 class="heading"><?php echo $oneItem->title ?></h1>
    <nav aria-label="breadcrumb">
      <?php echo $breadcrumb ?>
    </nav>
  </div>
</section>
<?php if(!empty($oneItem)):
    $url = getUrlPage($oneItem);
    ?>
    <section class="bg-white">

        <div class="container">
            <?php echo $oneItem->id != 8 ? $oneItem->description : '' ?>
            <?php echo $oneItem->content ?>
        </div>
    </section>
<?php endif; ?>
<script src="https://apis.google.com/js/platform.js" async defer></script>