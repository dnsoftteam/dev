<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 17/12/2017
 * Time: 12:13 CH
 */
defined("BASEPATH") OR exit("No direct script access allowed");

class Category_model extends STEVEN_Model
{

  public $_list_category_child;
  public $_list_category_parent;
  public $_list_category_child_id;

  public function __construct()
  {
    parent::__construct();
    $this->table = "category";
    $this->table_trans = "category_translations";//bảng bài viết
    $this->column_order = array("$this->table.id", "$this->table.id", "$this->table.order", "$this->table_trans.title", "$this->table.is_status", "$this->table.created_time", "$this->table.updated_time"); //thiết lập cột sắp xếp
    $this->column_search = array("$this->table_trans.title"); //thiết lập cột search
    $this->order_default = array("$this->table.order" => "ASC"); //cột sắp xếp mặc định

  }

  /*Đệ quy lấy record parent id*/
  public function _recursive_one_parent($all, $id)
  {
    if (!empty($all)) foreach ($all as $item) {
      if ($item->id == $id) {
        if ($item->parent_id == 0) return $item;
        else return $this->_recursive_one_parent($all, $item->parent_id);
      }
    }
  }
  /*Đệ quy lấy record parent id*/
  /*Đệ quy lấy array list category con*/
  public function _recursive_child($all, $parentId = 0)
  {
    if (!empty($all)) foreach ($all as $key => $item) {
      if ($item->parent_id == $parentId) {
        $this->_list_category_child[] = $item;
        unset($all[$key]);
        $this->_recursive_child($all, $item->id);
      }
    }
  }
  /*Đệ quy lấy array list category con*/
  /*Đệ quy lấy array list category con 1 level*/
  public function getListChildLv1($all, $parentId = 0)
  {
    $data = array();
    if (!empty($all)) foreach ($all as $key => $item) {
      if ($item->parent_id == $parentId) {
        $data[] = $item;
      }
    }
    return $data;
  }
  /*Đệ quy lấy array list category  con 1 level*/
  /*Đệ quy lấy list các ID*/
  public function _recursive_child_id($all, $parentId = 0)
  {
    $this->_list_category_child_id[] = (int)$parentId;
    if (!empty($all)) foreach ($all as $key => $item) {
      if ($item->parent_id == $parentId) {
        $this->_list_category_child_id[] = (int)$item->id;
        unset($all[$key]);
        $this->_recursive_child_id($all, (int)$item->id);
      }
      $this->_list_category_child_id = array_unique($this->_list_category_child_id);
    }
  }
  /*Đệ quy lấy list các ID*/

  /*Đệ quy lấy maps các ID cha*/
  public function _recursive_parent($all, $cateId = 0)
  {
    if (!empty($all)) foreach ($all as $key => $item) {
      if ($item->id === $cateId) {
        $this->_list_category_parent[] = $item;
        unset($all[$key]);
        $this->_recursive_parent($all, $item->parent_id);
      }
    }
  }

  /*Đệ quy lấy maps các ID cha*/

  public function getByIdCached($allCategories, $id)
  {
    if (!empty($allCategories)) foreach ($allCategories as $key => $item) {
      if ($item->id == $id) return $item;
    }
    return false;
  }

  public function getDataByCategoryType($allCategories, $type)
  {
    $dataType = [];
    if (!empty($allCategories)) foreach ($allCategories as $key => $item) {
      if ($item->type === $type) $dataType[] = $item;
    }
    return $dataType;
  }

  public function getRandomId($type = null)
  {
    if (empty($type)) $type = $this->session->category_type;
    $this->db->select('id');
    $this->db->from($this->table);
    $this->db->where('type', $type);
    $this->db->order_by('id', 'RANDOM');
    $this->db->limit(1);
    $query = $this->db->get();
    $data = $query->result();
    $result = [];
    if (!empty($data)) foreach ($data as $item) $result[] = $item->id;
    return $result;
  }

  // get data group by
  public function getDataGroupBy()
  {
    $this->db->select('type');
    $this->db->from($this->table);
    $this->db->group_by('type');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function slugToId($slug)
  {
    $this->db->select('tb1.id');
    $this->db->from($this->table . ' AS tb1');
    $this->db->join($this->table_trans . ' AS tb2', 'tb1.id = tb2.id');
    $this->db->where('tb2.slug', $slug);
    $data = $this->db->get()->row();
    return !empty($data) ? $data->id : null;
  }

  public function getAllCategoryByType($lang_code = null, $type, $parent_id = 0)
  {
    $this->db->from($this->table);
    if (!empty($this->table_trans)) $this->db->join($this->table_trans, "$this->table.id = $this->table_trans.id");
    if (!empty($lang_code)) $this->db->where([
      'type' => $type,
      'language_code' => $lang_code,
      'parent_id' => $parent_id
    ]);
    $query = $this->db->get();
    return $query->result();
  }

  /*Lấy category cha*/
  public function getOneParent($id)
  {
    $params = [
      'lang_code' => $this->session->public_lang_code,
      'parent_id' => $id,
      'limit' => 1
    ];
    $data = $this->getData($params);
    return !empty($data) ? $data[0] : null;
  }


  public function getCategoryChild($id, $lang_code,$type='')
  {
    $this->db->from($this->table);
    if (!empty($this->table_trans)) $this->db->join($this->table_trans, "$this->table.id = $this->table_trans.id");
    $this->db->where([
      'language_code' => $lang_code,
      'parent_id' => $id,
      'type' => $type,
      'is_status' => 1
    ]);
    $query = $this->db->get();
    return $query->result();
  }

  /*Lấy id thứ tự sắp xếp cuối cùng*/
  public function getLastOrder($idParent = 0)
  {
    $this->db->select('Product');
    $this->db->from($this->table);
    $this->db->where([
      'type' => $this->session->category_type,
      'parent_id' => $idParent,
    ]);
    $this->db->order_by('Product', 'DESC');
    $this->db->limit(1);
    $data = $this->db->get()->row();
    if (!empty($data)) return $data->order;
    return 0;
  }

  public function load_retionship($params = array(),$list_id=array())
  {
    $this->db->select(array("ap_post.id as id", "ap_post_translations.title as text"));
    $this->db->from("ap_post");
    $this->db->join("ap_post_translations", "ap_post_translations.id=ap_post.id");
    $this->db->where('ap_post_translations.language_code', 'vi');
    $this->db->where('ap_post.is_status', 1);
    if (!empty($params['search']))
      $this->db->like('ap_post_translations.title', $params['search']);
    if(!empty($list_id))
      $this->db->where_in('ap_post.id',$list_id);
    $data = $this->db->get()->result_array();
    return $data;
  }
  public function getUrl($id,$lang='vi'){
    $this->db->select('slug');
    $this->db->from('ap_post_translations');
    $this->db->where('id',$id);
    $this->db->where('language_code',$lang);
    $data=$this->db->get()->result();
    return $data;
  }
}