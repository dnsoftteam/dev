<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 02/01/2018
 * Time: 10:48 SA
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner_model extends STEVEN_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table            = "partner";
        $this->column_order     = array("$this->table.id","$this->table.id","$this->table.title","$this->table.address","$this->table.phone","$this->table.open_time","$this->table.is_status","$this->table.created_time","$this->table.updated_time"); //thiết lập cột sắp xếp
        $this->column_search    = array("$this->table.title"); //thiết lập cột search
        $this->order_default    = array("$this->table.created_time" => "DESC"); //cột sắp xếp mặc định
    }

    public function getDataPartner($cityId = null, $search = null){
        $this->db->select();
        $this->db->from($this->table);
        $this->db->where('is_status', 1);
        if(!empty($cityId)) $this->db->where("city_id",$cityId);
        if(!empty($search)){
            $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
            $this->db->like("$this->table.title", $search);
            $this->db->or_like("$this->table.address", $search);
            $this->db->group_end(); //close bracket
        }
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataCity($area_id = array()){
        $this->db->select('city_id, area_id');
        $this->db->from($this->table);
        $this->db->group_by('city_id');
        if(!empty($area_id)) $this->db->where("area_id",$area_id);
        $query = $this->db->get();
        return $query->result();
    }
}