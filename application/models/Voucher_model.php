<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 10/01/2018
 * Time: 11:31 SA
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher_model extends STEVEN_Model
{
  public function __construct()
  {
    parent::__construct();
    $this->table = 'voucher';
    $this->column_order = array('id', 'id', 'event', 'code', 'percent_sale', 'created_time'); //thiết lập cột sắp xếp
    $this->column_search = array('event', 'code'); //thiết lập cột search
    $this->order_default = array('id' => 'desc'); //cột sắp xếp mặc định
  }

  // kiểm tra mã voucher tồn tại hay chưa

  public function check_code($code)
  {
    $this->db->select('1');
    $this->db->from($this->table);
    $this->db->where('code', $code);
    $data = $this->db->get()->num_rows();

    return $data;
  }
}