<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 14/12/2017
 * Time: 12:59 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Users_model extends STEVEN_Model {

    protected $table_device_logged;
    public function __construct(){
        parent::__construct();
        $this->table = 'users';
        $this->table_device_logged = 'logged_device';//bảng logged device
        $this->column_order = array('id','id','username','email','first_name','active'); //thiết lập cột sắp xếp
        $this->column_search = array('username','email','first_name','last_name'); //thiết lập cột search
        $this->order_default = array('id' => 'desc'); //cột sắp xếp mặc định
    }
}