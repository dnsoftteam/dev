<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 17/12/2017
 * Time: 12:13 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Product_model extends STEVEN_Model{

    public function __construct()
    {
        parent::__construct();
        $this->table            = "product";
        $this->table_trans      = "product_translations";//bảng bài viết
        $this->table_category   = "product_category";//bảng bài viết
        $this->table_property = "product_property";
        $this->column_order     = array("$this->table.id","$this->table.id","$this->table_trans.title","$this->table.price","$this->table.is_featured","$this->table.is_status","$this->table.created_time","$this->table.updated_time",); //thiết lập cột sắp xếp
        $this->column_search    = array("$this->table_trans.title"); //thiết lập cột search
        $this->order_default    = array("$this->table.created_time" => "DESC"); //cột sắp xếp mặc định

    }
  //Xử lý tham số truyền vào. Tham số truyền vào phải dạng Array
  public function _where_product($args, $typeQuery = null,$select)
  {
    $is_status = '';
    //$lang_code = $this->session->admin_lang; //Mặc định lấy lang của Admin
    $page = 1; //Page default
    $limit = 10;
    extract($args);
    //$this->db->distinct();
    $this->db->select($select);
    $this->db->from($this->table);
//        if ($this->table === 'product') $this->db->select('IF(`price_sale` = 0, `price`, `price_sale`) AS `price_sort`');

    if (!empty($this->table_trans)) {
      $this->db->join($this->table_trans, "$this->table.id = $this->table_trans.id");
      if (empty($lang_code)) $lang_code = $this->session->admin_lang;
      if (!empty($lang_code)) $this->db->where("$this->table_trans.language_code", $lang_code);


    }

    if (!empty($this->table_category) && !empty($category_id)) {
      $nameModel = str_replace('_model', '', $this->table);
      $this->db->join($this->table_category, "$this->table.id = $this->table_category.{$nameModel}_id");
      $this->db->where_in("$this->table_category.category_id", $category_id);
      //  dd('ok');
    }

    /*Lọc các thuộc tính của sản phẩm*/
    if (isset($filter_price_min) && !empty($filter_price_max)) {
      $this->db->where("IF(`price_sale` != 0, `price_sale`, `price`) >=", $filter_price_min);
      $this->db->where("IF(`price_sale` != 0, `price_sale`, `price`) <=", $filter_price_max);
    }

    if(!empty($percent_sale))
      $this->db->where($this->table.'.percent_sale >=',$percent_sale);

    if(!empty($code))
      $this->db->where($this->table.'.code',$code);


    if (!empty($filter_property_product)) {
      $this->db->join($this->table_property, "$this->table.id = $this->table_property.product_id");
      $this->db->where_in("$this->table_property.property_id", $filter_property_product);
    }

    if(!empty($account_by_district))
      $this->db->where_in($this->table.'.account_id',$account_by_district);
    if (!empty($cate_not_id)) {
      $this->db->join($this->table_category, "$this->table.id = $this->table_category.post_id");
      // $this->db->where_in("$this->table_property.property_id",$filter_property);
      $this->db->where("$this->table_category.category_id !=", $cate_not_id);
    }


    if (!empty($cate_id_one)) {
      $this->db->join($this->table_category, "$this->table.id = $this->table_category.post_id");
      // $this->db->where_in("$this->table_property.property_id",$filter_property);
      $this->db->where("$this->table_category.category_id ", $cate_id_one);
    }

    /*Lọc các thuộc tính của sản phẩm*/
    if (!empty($parent_id))
      $this->db->where("$this->table.parent_id", $parent_id);

    if (!empty($category_type))
      $this->db->where("$this->table.type", $category_type);

    if (!empty($property_type))
      $this->db->where("$this->table.type", $property_type);


    if (!empty($type_id))
      $this->db->where("$this->table.type_id", $type_id);

    if (!empty($account_id))
      $this->db->where("$this->table.account_id", $account_id);

    if (!empty($parent_order))
      $this->db->order_by('parent_id', $parent_order);

    if (isset($is_status) && $is_status != '') {
      $this->db->where("$this->table.is_status", $is_status);
    }

    if (!empty($in))
      $this->db->where_in("$this->table.id", $in);

    if (!empty($or_in))
      $this->db->or_where_in("$this->table.id", $or_in);

    if (!empty($not_in))
      $this->db->where_not_in("$this->table.id", $not_in);

    if (!empty($or_not_in))
      $this->db->or_where_not_in("$this->table.id", $or_not_in);

    if(!empty($featured_order))
      $this->db->order_by("$this->table.is_featured",$featured_order);

    if (!empty($group_by))
      $this->db->group_by("$this->table.$group_by");
    //query for datatables jquery
    $this->_get_datatables_query();

    if (!empty($search)) {
      if (empty($this->table_trans)) $this->table_trans = $this->table;
      $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
      $this->db->like("$this->table_trans.title", $search);
      $this->db->group_end(); //close bracket
    }

    if ($typeQuery === null) {
      if (!empty($order) && is_array($order)) {
        foreach ($order as $k => $v)
          $this->db->order_by($k, $v);
      } else if (isset($this->order_default)) {
        $order = $this->order_default;
        $this->db->order_by(key($order), $order[key($order)]);
      }
      $offset = ($page - 1) * $limit;
      $this->db->limit($limit, $offset);
    }
  }
  public function getDataCrawlerApi($params = array())
  {

    $params['type_data']=2;
    $this->column_search = array('title'); //thiết lập cột search
    $urlApi = API_CRAWLER_URL . '/api/getProjectByCrawler';
    $data = $this->cUrl($urlApi, $params);
    //log_message('error','LOG_API_GET_ALL_PROJECT=> '.json_encode($params));
    $response = json_decode($data);
    if (isset($response->status) && $response->status == true) {
      return $response;
    } else return null;
  }

  public function getDataProduct($args = array(), $returnType = "object",$select='*')
  {
    $this->_where_product($args,$returnType,$select);
    $query = $this->db->get();

    //ddQuery($this->db);
    if ($returnType !== "object") return $query->result_array(); //Check kiểu data trả về
    else return $query->result();
  }
  /*Id của mongo có 24 ký tự ko phải id của mysql*/
  public function getByIdApi($idProject)
  {
    $urlApi = API_CRAWLER_URL . '/api/infoProject';
    $data = $this->cUrl($urlApi, array('id_project' => $idProject));
    $response = json_decode($data);
    if (isset($response->status) && $response->status == true) {
      return $response->data;
    } else return null;
  }


  public function getCategoryByProductId($productId, $lang_code = null){
        if(empty($lang_code)) $lang_code = $this->session->admin_lang ? $this->session->admin_lang : $this->session->public_lang_code;
        $this->db->select();
        $this->db->from($this->table_category);
        $this->db->join("category_translations","$this->table_category.category_id = category_translations.id");
        $this->db->join("category","$this->table_category.category_id = category.id");
        $this->db->where('category_translations.language_code', $lang_code);
        $this->db->where($this->table_category.".product_id", $productId);
        $data = $this->db->get()->result();
        //ddQuery($this->db);
        return $data;
    }
    public function getPropertySelect2($productId, $type)
    {
        $this->db->select("$this->table_property.property_id AS id, property.title AS text");
        $this->db->from($this->table_property);
        $this->db->join("property", "$this->table_property.property_id = property.id");
        $this->db->where_in($this->table_property . ".product_id", $productId);
        $this->db->where("property" . ".type", $type);
        $data = $this->db->get()->result();
        //ddQuery($this->db);
        return $data;
    }

    public function getCategorySelect2($productId, $lang_code = null){
        if(empty($lang_code)) $lang_code = $this->session->admin_lang ? $this->session->admin_lang : $this->session->public_lang_code;
        $this->db->select("$this->table_category.category_id AS id, category_translations.title AS text");
        $this->db->from($this->table_category);
        $this->db->join("category_translations","$this->table_category.category_id = category_translations.id");
        $this->db->where('category_translations.language_code', $lang_code);
        $this->db->where($this->table_category.".product_id", $productId);
        $data = $this->db->get()->result();
        //ddQuery($this->db);
        return $data;
    }
    
    public function listIdByCategory($category_id){
        $this->db->from($this->table_category);
        $this->db->where('category_id',$category_id);
        $result = $this->db->get()->result();
        $listPostId = [];
        if(!empty($result)) foreach ($result as $item){
            $listPostId[] = $item->product_id;
        }
        return $listPostId;
    }

    public function getOneCateIdById($id)
    {
        $data = $this->getCategoryByProductId($id);
        return !empty($data)?$data[0]:null;
    }

    public function getProductSelect2($params){
        $data = $this->getData($params);
        return $data;
    }

    public function getCateIdById($id)
    {
        $this->db->select('category_id');
        $this->db->from($this->table_category);
        $this->db->where('product_id', $id);
        $data = $this->db->get()->result();
        $listId = [];
        if (!empty($data)) foreach ($data as $item) {
            $listId[] = $item->category_id;
        }
        return $listId;
    }

    public function countPostByCate($cateId,$reset=FALSE){
        $key='countPostByCate'.$cateId;
        $data=$this->getCache($key);
        if(empty($data) || $reset==TRUE){
            $this->db->from($this->table_category);
            $this->db->where('category_id',$cateId);
            $query = $this->db->get();
            $data=$query->num_rows();
            $this->setCache($key,$data);
        }

        //dumpQuery($this->db);
        return $data;
    }

    public function updateProperty($imageId, $data)
    {
        $data = array_merge(array('product_id' => $imageId), $data);
        return $this->insertOnUpdate($data, $this->table_property);
    }
    public function countProductByAccount($id, $status = '')
    {
        $this->db->from($this->table);
        $this->db->where($this->table . '.account_id', $id);
        if (!empty($status)) $this->db->where($this->table . '.is_status', $status);
        return $this->db->count_all_results();
    }

    public function getPropertyProductId($productId, $type = '',$rsCache=false)
    {
        $key = 'getPropertyProjectId_' . $productId . '_' . $type;
        $data = $this->getCache($key);
        if (empty($data) || $rsCache==true) {
            $this->db->from($this->table_property);
            $this->db->join('property', "property.id = $this->table_property.property_id");
            $this->db->where('product_id', $productId);
            if (!empty($type)) $this->db->where($this->table_property . '.type', $type);
            $query = $this->db->get();
            $data = $query->result();
            $this->setCache($key, $data, 60 * 60 * 24 * 7);
        }
        return $data;
    }
    public function getIdProductByAccount($account_id,$reset=FALSE){
        $key='getIdProductByAccount'.$account_id;
        $data=$this->getCache($key);
        if(empty($data) || $reset==TRUE){
            $this->db->select('id');
            $this->db->from($this->table);
            $this->db->where("$this->table.account_id",$account_id);
            $result = $this->db->get();
            $arr= $result->result_array();
            $data = array_map (function($value){
                return $value['id'];
            } , $arr);
            $this->setCache($key,$data);
        }
        return $data;
    }
    public function getCateByProductAcount($account_id,$limit='',$list_id=array()){
        if(empty($list_id)){
            $list_id=$this->getIdProductByAccount($account_id);
        }
        $this->db->select("category_id");
        $this->db->from($this->table_category);
        if(!empty($list_id)) $this->db->where_in("product_id",$list_id);
        if(!empty($limit)) $this->db->limit($limit);
        $result = $this->db->get();
        $arr=$result->result_array();
        $data = array_map (function($value){
            return $value['category_id'];
        } , $arr);

        return $data;
    }
  public function getDataSelect2($args=array()){
    $limit=10;
    extract($args);
    $this->db->select("$this->table.id as id,$this->table_trans.title as text");
    $this->db->from($this->table);
    $this->db->join($this->table_trans,"$this->table_trans.id=$this->table.id");
    $this->db->where("$this->table_trans.language_code",$this->session->admin_lang);
    if(!empty($is_status)) $this->db->where("$this->table.is_status",$is_status);
    if(!empty($account_id)) $this->db->where("$this->table.account_id",$account_id);
    if(!empty($in)) $this->db->where_in("$this->table.id",$in);
    if(!empty($search)) $this->db->like("$this->table_trans.title",$search);
    $this->db->limit($limit);
    return $this->db->get()->result_array();
  }
  /*Check xem đã biên tập link này chưa*/
  public function checkExistCrawlerLink($link_md5)
  {
    $this->db->from($this->table);
    $this->db->where('crawler_link', $link_md5);
    if ($this->db->get()->num_rows() > 0) return true;
    else return false;
  }

  // Lấy url site dựa vào id product
  public function getUrlProduct($id){
    $this->db->select(array('title','slug','id'));
    $this->db->where($this->table_trans);
    $this->db->where("$this->table_trans.id",$id);
    $data=$this->db->get()->row();
    return $data;
  }
}
