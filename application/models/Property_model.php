<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 17/12/2017
 * Time: 12:13 CH
 */
defined("BASEPATH") OR exit("No direct script access allowed");

class Property_model extends STEVEN_Model
{

  public $_list_property_child;
  public $_list_property_parent;
  public $_list_property_child_id;
  public $_all_property;

  public function __construct()
  {
    parent::__construct();
    $this->table = "property";
    $this->column_order = array("$this->table.id", "$this->table.id", "$this->table.order", "$this->table.title", "$this->table.is_status", "$this->table.created_time", "$this->table.updated_time"); //thiết lập cột sắp xếp
    $this->column_search = array("$this->table.title"); //thiết lập cột search
    $this->order_default = array("$this->table.order" => "ASC"); //cột sắp xếp mặc định
    $this->getAllProperty();
  }

  public function getAllProperty()
  {
    if (empty($this->_all_property)) {
      $this->_all_property = $allProperty = $this->getAll();
    }
  }

  /*Đệ quy lấy record parent id*/
  public function _recursive_one_parent($all, $id)
  {
    if (!empty($all)) foreach ($all as $item) {
      if ($item->id == $id) {
        if ($item->parent_id == 0) return $item;
        else return $this->_recursive_one_parent($all, $item->parent_id);
      }
    }
  }
  /*Đệ quy lấy record parent id*/
  /*Đệ quy lấy array list property con*/
  public function _recursive_child($all, $parentId = 0)
  {
    if (!empty($all)) foreach ($all as $key => $item) {
      if ($item->parent_id == $parentId) {
        $this->_list_property_child[] = $item;
        unset($all[$key]);
        $this->_recursive_child($all, $item->id);
      }
    }
  }
  /*Đệ quy lấy array list property con*/
  /*Đệ quy lấy list các ID*/
  public function _recursive_child_id($all, $parentId = 0)
  {
    $this->_list_property_child_id[] = $parentId;
    if (!empty($all)) foreach ($all as $key => $item) {
      if ($item->parent_id == $parentId) {
        $this->_list_property_child_id[] = (int)$item->id;
        unset($all[$key]);
        $this->_recursive_child_id($all, (int)$item->id);
      }
      $this->_list_property_child_id = array_unique($this->_list_property_child_id);
    }
  }
  /*Đệ quy lấy list các ID*/

  /*Đệ quy lấy maps các ID cha*/
  public function _recursive_parent($all, $cateId = 0)
  {
    if (!empty($all)) foreach ($all as $key => $item) {
      if ($item->id === $cateId) {
        $this->_list_property_parent[] = $item;
        unset($all[$key]);
        $this->_recursive_parent($all, $item->parent_id);
      }
    }
  }

  /*Đệ quy lấy maps các ID cha*/

  public function getByIdCached($allProperty, $id)
  {
    if (!empty($allProperty)) foreach ($allProperty as $key => $item) {
      if ($item->id == $id) return $item;
    }
    return false;
  }

  public function countAllProjectByProperty($property_id)
  {
    $this->db->select("ap_project_property.project_id");
    $this->db->from('ap_project_property');
    $this->db->where('ap_project_property.property_id', $property_id);
    $arr = $this->db->get()->result_array();

    $data = array_map(function ($value) {
      return $value['project_id'];
    }, $arr);
    $params = array(
      'is_status' => 1,
      'in' => $data
    );

    $this->load->model('project_model');
    $projectModel = new Project_model();
    $reults = $projectModel->getTotalProject($params);
    return $reults;
  }

  public function countAllImagesByProperty($property)
  {
    $this->db->select("1");
    $this->db->from("ap_images_property");
    $this->db->where("ap_images_property.property_id", $property);
    $resuls = $this->db->get();
    return $resuls->num_rows();
  }

  public function getDataByPropertyType($allProperty, $type)
  {
    $dataType = array();
    if ($type == 'type_flooring' || $type == 'type_wall') {
      if (!empty($allProperty)) foreach ($allProperty as $key => $item) {
        $data_new = (array)$item;
//        $count = $this->countAllImagesByProperty($item->id);
//        $data_new['count'] = $count;
        if ($data_new['type'] === $type) $dataType[] = (object)$data_new;
      }
    } else {
      if (!empty($allProperty)) foreach ($allProperty as $key => $item) {
        $data_new = (array)$item;
//        $count = $this->countAllProjectByProperty($item->id);
//        $data_new['count'] = $count;
        if ($data_new['type'] === $type) $dataType[] = (object)$data_new;

      }
    }

    return $dataType;
  }

  // Product
  public function countAllProductByProperty($property_id)
  {
    $this->db->select('1');
    $this->db->from('ap_product_property');
    $this->db->where('ap_product_property.property_id', $property_id);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function getDataByPropertyTypeProduct($allProperty, $type)
  {

    $dataType = [];
    if (!empty($allProperty)) foreach ($allProperty as $key => $item) {
      $data_new = (array)$item;
      $count = $this->countAllProductByProperty($item->id);
      $data_new['count'] = $count;
      if ($data_new['type'] === $type) $dataType[] = (object)$data_new;

    }
    return $dataType;
  }

  public function getRandomId($type = null)
  {
    if (empty($type)) $type = $this->session->property_type;
    $this->db->select('id');
    $this->db->from($this->table);
    $this->db->where('type', $type);
    $this->db->order_by('id', 'RANDOM');
    $this->db->limit(1);
    $query = $this->db->get();
    $data = $query->result();
    $result = [];
    if (!empty($data)) foreach ($data as $item) $result[] = $item->id;
    return $result;
  }

  // get data group by
  public function getDataGroupBy()
  {
    $this->db->select('type');
    $this->db->from($this->table);
    $this->db->group_by('type');
    $query = $this->db->get();
    return $query->result_array();
  }

  public function getAllPropertyByType($type)
  {
    $this->db->from($this->table);
    $this->db->where([
      'type' => $type
    ]);
    $query = $this->db->get();
    return $query->result();
  }

  /*Lấy id thứ tự sắp xếp cuối cùng*/
  public function getLastOrder()
  {
    $this->db->select('Product');
    $this->db->from($this->table);
    $this->db->where([
      'type' => $this->session->property_type,
    ]);
    $this->db->order_by('Product', 'DESC');
    $this->db->limit(1);
    $data = $this->db->get()->row();
    if (!empty($data)) return $data->order;
    return 0;
  }

  public function hasTagsByProperty($property)
  {
    $this->db->select('1');
    $this->db->from('ap_images_property');
    $this->db->where('ap_images_property.property_id', $property);
    $this->db->limit(1);
    $query = $this->db->get();
    return $query->row();
  }

}