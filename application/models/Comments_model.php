<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 17/12/2017
 * Time: 12:13 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Comments_model extends STEVEN_Model{

  protected $table_product;
  public function __construct()
  {
    parent::__construct();
    $this->table            = "comments";
    $this->column_order     =  array('id','account_id','object_id','content','created_time'); //thiết lập cột sắp xếp
    $this->column_search    = array('id','account_id', 'object_id', 'email','fullname'); //thiết lập cột search
    $this->order_default    = array("$this->table.created_time" => "DESC"); //cột sắp xếp mặc định

  }

}