<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 02/01/2018
 * Time: 10:48 SA
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_model extends STEVEN_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table            = "banner";
        $this->table_trans      = "banner_translations";
        $this->column_order     = array("$this->table.id","$this->table.id"/*,"$this->table.position_id"*/,"$this->table_trans.title","$this->table.thumbnail","$this->table.is_status","$this->table.created_time","$this->table.updated_time"); //thiết lập cột sắp xếp
        $this->column_search    = array("$this->table_trans.title"); //thiết lập cột search
        $this->order_default    = array("$this->table.created_time" => "DESC"); //cột sắp xếp mặc định
    }
}