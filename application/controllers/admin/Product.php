<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 17/12/2017
 * Time: 12:08 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends Admin_Controller
{
  protected $_data;
  protected $_name_controller;
  protected $category_tree;

  const STATUS_CANCEL = 0;
  const STATUS_ACTIVE = 1;
  const STATUS_DRAFT = 2;

  public function __construct()
  {
    parent::__construct();
    //tải thư viện
    //$this->load->library(array('ion_auth'));
    $this->lang->load('product');
    $this->load->model('product_model');
    $this->_data = new Product_model();
    $this->_name_controller = $this->router->fetch_class();
    $this->session->category_type = $this->_name_controller;
  }

  public function index()
  {
    $data['heading_title'] = ucfirst($this->_name_controller);
    $data['heading_description'] = "Danh sách $this->_name_controller";
    /*Breadcrumbs*/
    $this->breadcrumbs->push('Home', base_url());
    $this->breadcrumbs->push($data['heading_title'], '#');
    $data['breadcrumbs'] = $this->breadcrumbs->show();

    /*Breadcrumbs*/
    $data['main_content'] = $this->load->view($this->template_path . $this->_name_controller . '/index', $data, TRUE);
    $this->load->view($this->template_main, $data);
  }

  /*Project lấy từ Crawler chờ biên tập*/
  public function approve()
  {
    $data['heading_title'] = "Quản lý sản phẩm";
    $data['heading_description'] = "Danh sách sản phẩm chờ biên tập chỉnh sửa";
    $this->load->model('crawler_model');
    $crawlerModel = new Crawler_model();
    $data['list_crawler'] = $crawlerModel->getAll();
    /*Breadcrumbs*/
    $this->breadcrumbs->push('Home', base_url());
    $this->breadcrumbs->push($data['heading_title'], '#');
    $data['breadcrumbs'] = $this->breadcrumbs->show();
    /*Breadcrumbs*/
    $data['main_content'] = $this->load->view($this->template_path . $this->_name_controller . '/approve', $data, TRUE);
    $this->load->view($this->template_main, $data);
  }

  public function _queue_select($categories, $parent_id = 0, $char = '')
  {
    foreach ($categories as $key => $item) {
      if ($item->parent_id == $parent_id) {
        $tmp['name'] = $parent_id ? $char . '|----' . $item->title : $char . $item->title;
        $tmp['id'] = $item->id;
        $this->category_tree[] = $tmp;
        unset($categories[$key]);
        $this->_queue_select($categories, $item->id, $char . '----');
      }
    }
  }

  public function ajax_load_property($type = '')
  {
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      $this->load->model('property_model');
      $propertyModel = new Property_model();
      $data = $propertyModel->getDataByPropertyTypeProduct($propertyModel->_all_property, $type);
      echo json_encode($data);
      exit;
    }
  }

  /*
   * Ajax trả về datatable
   * */
  public function ajax_list()
  {
    if (!empty($this->input->post('category_id'))) {
      $this->load->model('category_model');
      $categoryModel = new Category_model();
      $allCategory = $categoryModel->getAll($this->session->admin_lang_code);
      $categoryModel->_recursive_child_id($allCategory, $this->input->post('category_id'));
      $listCateId = $categoryModel->_list_category_child_id;
//            ddQuery($listCateId);
    }
    if ($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      $length = $this->input->post('length');
      $no = $this->input->post('start');
      $page = $no / $length + 1;
      $params['category_id'] = !empty($listCateId) ? $listCateId : null;
      $params['page'] = $page;
      $params['limit'] = $length;
      $list = $this->_data->getData($params);
      $data = array();
      if (!empty($list)) foreach ($list as $item) {
        $no++;
        $status = (button_action_change_approve() == 1) ? 'btnUpdateStatus' : '';
        $row = array();
        $row[] = $item->id;
        $row[] = $item->id;
        $row[] = $item->title;
        $row[] = !empty($item->price_sale) ? formatMoney($item->price_sale) . ' <span style="text-decoration: line-through;">' . formatMoney($item->price) . '</span>' : $item->price;
        $row[] = ($item->is_featured == true) ? '<i data-value="1" class="text-primary fa fa-lg fa-star btnUpdateFeatured"></i>' : '<i data-value="0" class="text-primary fa fa-lg fa-star-o btnUpdateFeatured"></i>';
        switch ($item->is_status) {
          case self::STATUS_ACTIVE:
            $row[] = '<span class="label label-success ' . $status . '" data-value="' . self::STATUS_CANCEL . '">' . $this->lang->line('text_status_' . self::STATUS_ACTIVE) . '</span>';
            break;
          case self::STATUS_DRAFT:
            $row[] = '<span class="label label-default ' . $status . '" data-value="' . self::STATUS_ACTIVE . '">' . $this->lang->line('text_status_' . self::STATUS_DRAFT) . '</span>';
            break;
          default:
            $row[] = '<span class="label label-danger ' . $status . '" data-value="' . self::STATUS_DRAFT . '">' . $this->lang->line('text_status_' . self::STATUS_CANCEL) . '</span>';
            break;
        }
        $row[] = date('d/m/Y H:i', strtotime($item->created_time));
        $row[] = date('d/m/Y H:i', strtotime($item->updated_time));
        //thêm action
        $action = '<div class="text-center">';
        $action .= '<a class="btn btn-sm btn-primary" href="' . site_url('admin/') . $this->uri->segment(2) . '?edit=' . $item->id . '" title="' . $this->lang->line('btn_edit') . '" onclick="event.preventDefault();edit_form(' . "'" . $item->id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i></a>';
        $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="' . $this->lang->line('btn_remove') . '" onclick="delete_item(' . "'" . $item->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>';
        $action .= '</div>';
        $row[] = $action;
        $data[] = $row;
      }

      $output = array(
        "draw" => $this->input->post('draw'),
        "recordsTotal" => $this->_data->getTotalAll(),
        "recordsFiltered" => $this->_data->getTotal($params),
        "data" => $data,
      );
      //trả về json
      echo json_encode($output);
    }
    exit;
  }
  /*
   * Ajax xử lý thêm mới
   * */
  public function ajax_add()
  {
    $data_store = $this->_convertData();

//    dd($data_store);
    $properties = (!empty($data_store['property'])) ? $data_store['property'] : '';
    unset($data_store['property']);
//    dd($data_store);
    $id_last = $this->_data->save($data_store);
    if ($id_last != false) {
      // update redis key ap_list_post_frontend
      $action = 'add';
      if ($data_store['is_status'] != 1) {
        $action = 'remove';
      }
      // update redis key ap_list_post_frontend
      if (!empty($properties)) {
        $this->_data->delete(array('product_id' => $id_last), $this->_data->table_property);
        foreach ($properties as $type => $property) foreach ($property as $proper) {
          $tmpProperty['type'] = $type;
          $tmpProperty['property_id'] = $proper;
          $this->_data->updateProperty($id_last, $tmpProperty);
          unset($tmpProperty);
        }
      }
      /*Call API update status project*/
      // log action
      $action = $this->router->fetch_class();
      $note = "Insert $action: " . $this->db->insert_id();
      $this->addLogaction($action, $note);
      $message['type'] = 'success';
      $message['message'] = $this->lang->line('mess_add_success');
    } else {
      $message['type'] = 'error';
      $message['message'] = $this->lang->line('mess_add_unsuccess');
    }
    die(json_encode($message));
  }

  /*
   * Ajax copy
   * */
  function ajax_copy($id)
  {
    $this->load->model('category_model');
    $categoryModel = new Category_model();
    $data = $this->_data->getById($id);
    $data_store = array();
    $data_trans = array();
    $randId = rand(1000, 9999);
    foreach ($data as $key => $item) {
      unset($item->created_time);
      unset($item->updated_time);
      unset($item->id);
      unset($item->order);

      if (!empty($item)) foreach ($item as $k => $v) {
        $data_store[$k] = $v;
      }
      if (!empty($this->config->item('cms_language'))) foreach ($this->config->item('cms_language') as $lang_code => $lang_name) {
        if ($item->language_code === $lang_code) {
          $data_trans['title'][$lang_code] = $item->title;
          $data_trans['meta_title'][$lang_code] = !empty($item->meta_title) ? $item->meta_title : '';
          $data_trans['language_code'][$lang_code] = $lang_code;
          $data_trans['slug'][$lang_code] = $item->slug . "-$randId";
          $data_trans['description'][$lang_code] = $item->description;
          $data_trans['meta_description'][$lang_code] = !empty($item->meta_description) ? $item->meta_description : '';
          $data_trans['meta_keyword'][$lang_code] = !empty($item->meta_keyword) ? $item->meta_keyword : '';
          $data_trans['content'][$lang_code] = !empty($item->content) ? $item->content : '';
        }
      }
    }

    $data_store = array_merge($data_store, $data_trans);
    $data_store['category_id'] = $categoryModel->getRandomId();
    $response = $this->_data->save($data_store);
    if ($response !== false) {
      $message['type'] = 'success';
      $message['message'] = "Nhân bản thành công !";
    } else {
      $message['type'] = 'error';
      $message['message'] = "Nhân bản thất bại !";
      $message['error'] = $response;
      log_message('error', $response);
    }
    print json_encode($message);

    exit;
  }

  /*
   * Ajax lấy thông tin
   * */
  public function ajax_edit($id)
  {
    $this->load->model('category_model');
//
    $oneItem = $this->_data->getById($id);
    $data['category_id'] = $this->_data->getCategorySelect2($id);
//    $data['data'] = $oneItem = $this->_data->getById($id);
    $data['data'] = $oneItem;

    $data['style_product'] = $this->_data->getPropertySelect2($id, 'style_product');
    $data['branch'] = $this->_data->getPropertySelect2($id, 'branch');
    $data['made_in'] = $this->_data->getPropertySelect2($id, 'made_in');
    $data['unit'] = $this->_data->getPropertySelect2($id, 'unit');
    $data['material'] = $this->_data->getPropertySelect2($id, 'material');
    die(json_encode($data));
  }

  /*
   * Ajax xử lý thêm mới
   * */
  public function ajax_update()
  {
    $data_store = $this->_convertData();
    $properties = (!empty($data_store['property'])) ? $data_store['property'] : '';
    unset($data_store['property']);
    $response = $this->_data->update(array('id' => $this->input->post('id')), $data_store, $this->_data->table);
    if ($response != false) {
      // update redis key ap_list_post_frontend
      $action = 'add';
      if ($data_store['is_status'] != 1) {
        $action = 'remove';
      }
      // update redis key ap_list_post_frontend

      if (!empty($properties)) {
        $this->_data->delete(array('product_id' => $this->input->post('id')), $this->_data->table_property);
        foreach ($properties as $type => $property) foreach ($property as $proper) {
          $tmpProperty['type'] = $type;
          $tmpProperty['property_id'] = $proper;
          $this->_data->updateProperty($this->input->post('id'), $tmpProperty);
          unset($tmpProperty);
        }
      }
      // log action
      $action = $this->router->fetch_class();
      $note = "Update $action: " . $data_store['id'];
      $this->addLogaction($action, $note);
      $message['type'] = 'success';
      $message['message'] = $this->lang->line('mess_update_success');
    } else {
      $message['type'] = 'error';
      $message['message'] = $this->lang->line('mess_update_unsuccess');
    }
    die(json_encode($message));
  }

  public function ajax_update_field()
  {
    if ($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      $id = $this->input->post('id');
      $field = $this->input->post('field');
      $value = $this->input->post('value');
      $response = $this->_data->update(['id' => $id], [$field => $value]);
      if ($response != false) {
        $dataPostAPI['id_project'] = $id;
        $dataPostAPI['status'] = $value;
        $message['type'] = 'success';
        $message['message'] = $this->lang->line('mess_update_success');
        // update redis key ap_list_post_frontend
        if ($field == 'is_status') {
          $action = 'add';
          if ($value != 1) {
            $action = 'remove';
          }
        }
        // update redis key ap_list_post_frontend
      } else {
        $message['type'] = 'error';
        $message['message'] = $this->lang->line('mess_update_unsuccess');
      }
      print json_encode($message);
    }
    exit;
  }

  public function ajax_delete($id)
  {
    $response = $this->_data->delete(['id' => $id]);
    if ($response != false) {
      //Xóa translate của post
      $this->_data->delete(["id" => $id], $this->_data->table_trans);
      //Xóa category của post
      $this->_data->delete(["{$this->_name_controller}_id" => $id], $this->_data->table_category);

      $this->_data->delete(["{$this->_name_controller}_id" => $id], $this->_data->table_property);
      // log action
      $action = $this->router->fetch_class();
      $note = "Update $action: $id";
      $this->addLogaction($action, $note);
      $message['type'] = 'success';
      $message['message'] = $this->lang->line('mess_delete_success');
    } else {
      $message['type'] = 'error';
      $message['message'] = $this->lang->line('mess_delete_unsuccess');
      $message['error'] = $response;
      log_message('error', $response);
    }
    die(json_encode($message));
  }

  /*
   * Kiêm tra thông tin post lên
   * */
  private function _validate()
  {
    if ($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      if (!empty($this->config->item('cms_language'))) foreach ($this->config->item('cms_language') as $lang_code => $lang_name) {
        if ($lang_code === $this->config->item('default_language')) {
          $this->form_validation->set_rules('title[' . $lang_code . ']', $this->lang->line('error_title') . ' - ' . $lang_name, 'required|trim|min_length[5]|max_length[300]');
          $this->form_validation->set_rules('description[' . $lang_code . ']', $this->lang->line('error_description') . ' - ' . $lang_name, 'required');
        }
        $this->form_validation->set_rules('thumbnail', $this->lang->line('error_image'), 'required');
      }
      if ($this->form_validation->run() === false) {
        $message['type'] = "warning";
        $message['message'] = $this->lang->line('mess_validation');
        $valid = [];
        if (!empty($this->config->item('cms_language'))) foreach ($this->config->item('cms_language') as $lang_code => $lang_name) {
          if ($lang_code === $this->config->item('default_language')) {
            $valid["title[$lang_code]"] = form_error("title[$lang_code]");
            $valid["description[$lang_code]"] = form_error("description[$lang_code]");

          }
        }
        $message['validation'] = $valid;
        die(json_encode($message));
      }
    }
  }

  private function _convertData()
  {
    $this->_validate();
    $data = $this->input->post();
    $data_store = array();

    if (!empty($data)) foreach ($data as $key => $item) {
      if (in_array($item, $this->config->item('cms_language'))) foreach ($this->config->item('cms_language') as $lang_code => $lang_name) {
        $data_store[$key][$lang_code] = trim($item[$lang_code]);
      } else {
        switch ($key) {
          case "album":
            $data_store[$key] = json_encode($item);
            break;
          default:
            $data_store[$key] = $item;
        }
      }
    }
    $data_store['price'] = (int)str_replace(',', '', str_replace('.', '', $data_store['price']));
    if (!empty($data_store['price']) && !empty($data_store['price_sale'])) {
      $data_store['percent_sale'] = round(100 - ($data_store['price_sale'] / $data_store['price']) * 100);
    } else {
      $data_store['percent_sale'] = 0;
    }
    unset($data_store['id_project']);
    return $data_store;
  }

  public function ajax_upload_gallery()
  {
    $data_store = array();
    if (!empty($_FILES['upload_album'])) {
      if (is_array($_FILES['upload_album']['name'])) for ($i = 0; $i < count($_FILES['upload_album']['name']); $i++) {
        $file_e = array(
          'name' => $_FILES['upload_album']['name'][$i],
          'type' => $_FILES['upload_album']['type'][$i],
          'tmp_name' => $_FILES['upload_album']['tmp_name'][$i],
          'error' => $_FILES['upload_album']['error'][$i],
          'size' => $_FILES['upload_album']['size'][$i],
        );
        $response1 = $this->uploadImageByLocalToServer($file_e);
        $data_store['album'][] = !empty($response1[0]->path) ? $response1[0]->path : '';
      }
    }
    $data_store['type'] = 'success';
    die(json_encode($data_store));
  }

  /*
  * Ajax lấy thông tin crawler qua API
  * */
  public function ajax_show($id)//ID mongo ko phải ID mysql
  {
    $this->load->model('crawler_model');
    $crawlerModel = new Crawler_model();
    $data = $this->_data->getByIdApi($id);
//    dd($data);
    $oneCrawler = $crawlerModel->single(array('id' => $data->id_crawler));
    $arrImage = $data->images;
    $arrImage = array_unique($arrImage);
    $data_editor['data'][] = array(
      'language_code' => 'vi',
      'title' => trim($data->title),
      'slug' => $this->toSlug($data->title),
      'description' => trim($data->title),
      'meta_title' => trim($data->title),
      'meta_description' => trim($data->title),
      'content' => $data->content,
      'crawler_link' => $data->title_md5,
      'url_source' => $data->url,
      'id_project' => $id,
      'price' => $data->price,
    );
    if (!empty($oneCrawler->account_id)) {
      $this->load->model('account_model');
      $accountModel = new Account_model();
      $account = $accountModel->getById($oneCrawler->account_id);
      $data_editor['account_id'] = array('id' => $account->id, 'text' => !empty($account->display_name) ? $account->display_name : $account->username);
    }

    $catLink = $crawlerModel->getById($data->id_crawler, 'category_id', 'vi');
    if (!empty($catLink)) {
      $this->load->model('category_model');
      $category = new Category_model();
      $data_editor['category_id'] = array($category->getSelect2($catLink->category_id));
    }

    //$data_editor['thumbnail'] = isset($arrImage[0]) ? $arrImage[0] : '';
    $data_editor['album'] = $arrImage;
    die(json_encode($data_editor));
  }
}
