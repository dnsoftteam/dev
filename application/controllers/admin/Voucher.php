<?php
/**
 * Developer: linhth
 * Controller bài viết
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Voucher extends Admin_Controller
{
  protected $_data;

  public function __construct()
  {
    parent::__construct();

    //tải file ngôn ngữ//tải model
    $this->load->model('voucher_model');
    $this->_data = new Voucher_model();
  }

  public function index()
  {
    //chỉ lấy ra những category thuộc mục product
    $data = [];
    $data['main_content'] = $this->load->view($this->template_path . 'voucher/index', $data, TRUE);
    $this->load->view($this->template_main, $data);
  }


  public function ajax_add()
  {
    $data_store = $this->_convertData();
    if ($this->_data->save($data_store)) {
      // log action
      $action = $this->router->fetch_class();
      $note = "Insert $action: " . $this->db->insert_id();
      $this->addLogaction($action, $note);
      $message['type'] = 'success';
      $message['message'] = $this->lang->line('mess_add_success');
    } else {
      $message['type'] = 'error';
      $message['message'] = $this->lang->line('mess_add_unsuccess');
    }
    die(json_encode($message));
  }

  /*
   * Ajax trả về datatable
   * */
  public function ajax_list()
  {
    if ($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      $length = $this->input->post('length');
      $no = $this->input->post('start');
      $page = $no / $length + 1;
      $params['page'] = $page;
      $params['limit'] = $length;
      $list = $this->_data->getData($params);
      $data = array();
//      dd($list);
      foreach ($list as $item) {
        $no++;
        $row = array();
        $row[] = $item->id;
        $row[] = $item->id;
        $row[] = $item->code;
        $row[] = $item->event;
        $row[] = date('d/m/Y', strtotime($item->start_time));
        $row[] = date('d/m/Y', strtotime($item->end_time));
        $row[] = (!empty($item->percent_sale)) ? $item->percent_sale : $item->price_sale;
        $row[] = !empty($item->total_use) ? $item->total_use : '_';
        $row[] = !empty($item->total_use) ? $item->remaining_use : '_';
        if ($item->is_status === 1) {
          $row[] = '<span class="label label-success">Áp dụng</span>';
        } else {
          $row[] = '<span class="label label-danger">Hủy</span>';
        }
        //thêm action
        $action = '<div class="text-center">';
        $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="'.$this->lang->line('btn_edit').'" onclick="edit_form('."'".$item->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
        $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="' . $this->lang->line('btn_remove') . '" onclick="delete_item(' . "'" . $item->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>';
        $action .= '</div>';
        $row[] = $action;
        $data[] = $row;
      }

      $output = array(
        "draw" => $this->input->post('draw'),
        "recordsTotal" => $this->_data->getAll(),
        "recordsFiltered" => $this->_data->getTotal($params),
        "data" => $data,
      );
      //trả về json
//            dd($output);
      echo json_encode($output);
    }
    exit;
  }

  public function ajax_update()
  {
    $id = !empty($this->input->post('id')) ? $this->input->post('id') : 0;
    $params['id'] = $id;

    $key = $this->input->post('key');
    $val = $this->input->post('val');

    if ($key == 'payment_status') {
      if ($val == 1) $valNew = 0;
      else $valNew = 1;
      $data[$key] = $valNew;
    } else {
      $data[$key] = $val;
    }
    $this->_data->update($params, $data);


    $output = [
      'message' => 'update success'
    ];
    echo json_encode($output);
  }

  public function ajax_edit($id)
  {

    $data['post'] = (array) $this->_data->getById($id);
    die(json_encode($data));
  }


  /*
   * Xóa một bản ghi
   * */
  public function ajax_delete($id)
  {
    if ($id == 1) return;
    $this->_data->delete_by_id($id);
    die(json_encode(array("error" => 0)));
  }

//  Check xem mã đã tồn tại hay chưa
  function ajax_check_code()
  {
    if ($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

      $code = $this->input->post('code');
      if (!empty($code)) $result = $this->_data->check_code($code);
      echo $result;
      exit();
    }
  }

  /*
     * Kiêm tra thông tin post lên
     * */
  private function _validate()
  {
    if ($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      if (!empty($this->config->item('cms_language'))) foreach ($this->config->item('cms_language') as $lang_code => $lang_name) {
        $this->form_validation->set_rules('event', 'Tiêu đề sự kiện', 'required');
        $this->form_validation->set_rules('code', 'Mã voucher', 'required');
        $this->form_validation->set_rules('price_sale', 'Giá khuyến mại', 'numeric');
        $this->form_validation->set_rules('percent_sale', 'Phần trăm khuyến mại', 'numeric');
      }
      if ($this->form_validation->run() === false) {
        $message['type'] = "warning";
        $message['message'] = $this->lang->line('mess_validation');
        $valid = [];
        if (!empty($this->config->item('cms_language'))) foreach ($this->config->item('cms_language') as $lang_code => $lang_name) {
          if ($lang_code === $this->config->item('default_language')) {
            $valid["event"] = form_error("event");
            $valid["code"] = form_error("code");
            $valid["price_sale"] = form_error("price_sale");
            $valid["percent_sale"] = form_error("percent_sale");
          }
        }
        //$valid["thumbnail"] = form_error("thumbnail");
        $message['validation'] = $valid;
        die(json_encode($message));
      }
    }
  }

  private function _convertData()
  {
    $this->_validate();
    $data = $this->input->post();

    return $data;
  }
}
