<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 17/12/2017
 * Time: 3:10 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Property extends Admin_Controller
{
    protected $_data;
    protected $_name_controller;
    protected $property_tree;

    const STATUS_CANCEL = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DRAFT = 2;

    public function __construct()
    {
        parent::__construct();
        //tải thư viện
        //$this->load->library(array('ion_auth'));
        $this->lang->load('property');
        $this->load->model('property_model');
        $this->_data = new Property_model();
        $this->_name_controller = $this->router->fetch_class();
    }

    public function index()
    {

    }

    public function get_list($data)
    {
        /*Breadcrumbs*/
        $this->breadcrumbs->push('Home', base_url());
        $this->breadcrumbs->push($data['heading_title'], current_url());
        $this->breadcrumbs->push($data['heading_description'], '#');
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*Breadcrumbs*/
        $data['main_content'] = $this->load->view($this->template_path . $this->_name_controller . '/index', $data, TRUE);
        $this->load->view($this->template_main, $data);
    }

    public function ajax_load($type = ''){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $term = $this->input->get("q");
            $id = $this->input->get('id')?$this->input->get('id'):0;
            $params = array(
              'property_type' => !(empty($type)) ? $type : $this->session->property_type,
              'is_status'=> 1,
              'not_in' => ['id' => $id],
              'search' => $term,
              'limit'=> 20
            );
            $list = $this->_data->getData($params);
            $json = [];
            if(!empty($list)) foreach ($list as $item) {
                $item = (object) $item;
                $json[] = ['id'=>$item->id, 'text'=>$item->title];
            }
            print json_encode($json);
        }
        exit;
    }

    public function style(){
        $data['heading_title'] = "Quản lý thuộc tính";
        $data['heading_description'] = "Danh sách thuộc tính phong cách";
        $this->session->property_type = $this->router->fetch_method();
        $this->get_list($data);
    }


    public function size(){
        $data['heading_title'] = "Quản lý thuộc tính";
        $data['heading_description'] = "Danh sách thuộc tính kích thước";
        $this->session->property_type = $this->router->fetch_method();
        $this->get_list($data);
    }
    public function color(){
        $data['heading_title'] = "Quản lý thuộc tính";
        $data['heading_description'] = "Danh sách thuộc tính màu sắc";
        $this->session->property_type = $this->router->fetch_method();
        $this->get_list($data);
    }
    public function material(){
        $data['heading_title'] = "Quản lý thuộc tính";
        $data['heading_description'] = "Danh sách thuộc tính chất liệu";
        $this->session->property_type = $this->router->fetch_method();
        $this->get_list($data);
    }
    public function time(){
        $data['heading_title'] = "Quản lý thuộc tính";
        $data['heading_description'] = "Danh sách thuộc tính thời gian tham gia hệ thống";
        $this->session->property_type = $this->router->fetch_method();
        $this->get_list($data);
    }

/// Thuộc tính sản phẩm.
///
    public function branch(){
        $data['heading_title'] = "Quản lý nhãn hiệu";
        $data['heading_description'] = "Danh sách thuộc tính nhãn hiệu";
        $this->session->property_type = $this->router->fetch_method();
        $this->get_list($data);
    }
    public function made_in(){
        $data['heading_title'] = "Quản lý xuất xứ";
        $data['heading_description'] = "Danh sách thuộc tính xuất xứ";
        $this->session->property_type = $this->router->fetch_method();
        $this->get_list($data);
    }
    public function unit(){
        $data['heading_title'] = "Quản lý đơn vị tính";
        $data['heading_description'] = "Danh sách thuộc tính đơn vị tính";
        $this->session->property_type = $this->router->fetch_method();
        $this->get_list($data);
    }

    /*
     * Ajax trả về datatable
     * */
    public function ajax_list()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $data = array();
            //$length =  $this->input->post('length');
            $length =  20;
            $no = $this->input->post('start');
            $page = $no/$length + 1;
            if(!empty($this->input->post('parent_id'))) $params['parent_id'] = $this->input->post('parent_id');
            $params['property_type'] = $this->session->property_type;
            $params['page'] = $page;
            $params['limit'] = $length;
            $list = $this->_data->getData($params);
//            ddQuery($this->db);
            $title_delete='';
            if(!empty($list)) foreach ($list as $item) {

                if(in_array($this->session->property_type,array('type_wall','type_flooring'))){
                    if(!empty($this->_data->hasTagsByProperty($item->id))){
                        $title_delete='Thuộc tính đã có tags ảnh được chọn. Bạn có chắc chắn muốn xóa?';
                    }

                }
                $no++;
                $row = array();
                $status=(button_action_change_approve()==1)?'btnUpdateStatus':'';
                $row[] = $item->id;
                $row[] = $item->id;
                $row[] = $item->order;
                $row[] = $item->title;
                switch ($item->is_status){
                    case self::STATUS_ACTIVE:
                        $row[] = '<span class="label label-success '.$status.'" data-value="'.self::STATUS_CANCEL.'">'.$this->lang->line('text_status_'.self::STATUS_ACTIVE).'</span>';
                        break;
                    case self::STATUS_DRAFT:
                        $row[] = '<span class="label label-default '.$status.'" data-value="'.self::STATUS_ACTIVE.'">'.$this->lang->line('text_status_'.self::STATUS_DRAFT).'</span>';
                        break;
                    default:
                        $row[] = '<span class="label label-danger '.$status.'" data-value="'.self::STATUS_DRAFT.'">'.$this->lang->line('text_status_'.self::STATUS_CANCEL).'</span>';
                        break;
                }
                $row[] = $item->created_time;
                $row[] = $item->updated_time;
                //thêm action
                $action = '<div class="text-center">';
                $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="'.$this->lang->line('btn_edit').'" onclick="edit_form('."'".$item->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
                $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="' . $this->lang->line('btn_remove') . '" onclick="delete_item(' . "'" . $item->id . "','".$title_delete."'" . ')"><i class="glyphicon glyphicon-trash"></i></a>';
                $action .= '</div>';
                $row[] = $action;
                $data[] = $row;
            }

            $output = array(
                "draw" => $this->input->post('draw'),
                "recordsTotal" => $this->_data->getTotalAll(),
                "recordsFiltered" => $this->_data->getTotal($params),
                "data" => $data,
            );
            //trả về json
            die(json_encode($output));
        }
        exit;
    }

    /*
     * Ajax xử lý thêm mới
     * */
    public function ajax_add()
    {
        $data_store = $this->_convertData();
        $data_store['Product'] = $this->_data->getLastOrder() + 1;
        if($this->_data->save($data_store)){
            // log action
            $action = $this->router->fetch_class();
            $note = "Insert $action: ".$this->db->insert_id();
            $this->addLogaction($action,$note);
            $message['type'] = 'success';
            $message['message'] = $this->lang->line('mess_add_success');
        }else{
            $message['type'] = 'error';
            $message['message'] = $this->lang->line('mess_add_unsuccess');
        }
        die(json_encode($message));
    }

    /*
     * Ajax copy
     * */
    function ajax_copy($id){
        $data = $this->_data->getById($id);
        $data_store = array();
        $randId = rand(1000,9999);
        foreach ($data as $key => $item){
            unset($item->created_time);
            unset($item->updated_time);
            unset($item->id);
            unset($item->order);

            if(!empty($item)) foreach ($item as $k => $v){
                $data_store[$k] = $v;
            }
            $data_store['title'] = $item->title." $randId";
            $data_store['description'] = $item->description." $randId";
        }

        $response = $this->_data->save($data_store);
        if($response !== false){
            $message['type'] = 'success';
            $message['message'] = "Nhân bản thành công !";
        }else{
            $message['type'] = 'error';
            $message['message'] = "Nhân bản thất bại !";
            $message['error'] = $response;
            log_message('error',$response);
        }
        print json_encode($message);
        exit;
    }

    /*
     * Ajax lấy thông tin
     * */
    public function ajax_edit($id)
    {
        $data['data'] = $this->_data->getById($id);
        die(json_encode($data));
    }

    /*
     * Ajax xử lý thêm mới
     * */
    public function ajax_update()
    {
        $data_store = $this->_convertData();
        $response = $this->_data->update(array('id' => $this->input->post('id')), $data_store, $this->_data->table);
        if($response != false){
            // log action
            $action = $this->router->fetch_class();
            $note = "Update $action: ".$data_store['id'];
            $this->addLogaction($action,$note);
            $message['type'] = 'success';
            $message['message'] = $this->lang->line('mess_update_success');
        }else{
            $message['type'] = 'error';
            $message['message'] = $this->lang->line('mess_update_unsuccess');
        }
        die(json_encode($message));
    }

    public function ajax_update_field()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $id = $this->input->post('id');
            $field = $this->input->post('field');
            $value = $this->input->post('value');
            $response = $this->_data->update(['id' => $id], [$field => $value]);
            if($response != false){
                $message['type'] = 'success';
                $message['message'] = $this->lang->line('mess_update_success');
            }else{
                $message['type'] = 'error';
                $message['message'] = $this->lang->line('mess_update_unsuccess');
            }
            print json_encode($message);
        }
        exit;
    }

    public function ajax_delete($id)
    {
        $response = $this->_data->delete(['id'=>$id]);
        if($response != false){
            // log action
            $action = $this->router->fetch_class();
            $note = "Update $action: $id";
            $this->addLogaction($action,$note);
            $message['type'] = 'success';
            $message['message'] = $this->lang->line('mess_delete_success');
        }else{
            $message['type'] = 'error';
            $message['message'] = $this->lang->line('mess_delete_unsuccess');
            $message['error'] = $response;
            log_message('error',$response);
        }
        die(json_encode($message));
    }

    /*
     * Kiêm tra thông tin post lên
     * */
    private function _validate()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $this->form_validation->set_rules('title', $this->lang->line('error_title'), 'required|trim|min_length[2]|max_length[255]');
            if ($this->form_validation->run() === false) {
                $message['type'] = "warning";
                $message['message'] = $this->lang->line('mess_validation');
                $valid["title"] = form_error("title");
                $message['validation'] = $valid;
                die(json_encode($message));
            }
        }
    }

    private function _convertData(){
        $this->_validate();
        $data = $this->input->post();
        $data['type'] = $this->session->property_type;
        return $data;
    }
}