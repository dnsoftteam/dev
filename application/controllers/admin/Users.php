<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 14/12/2017
 * Time: 12:58 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends Admin_Controller {
    protected $_data;
    protected $_data_group;
    protected $_name_controller;
    public function __construct()
    {
        parent::__construct();
        //tải thư viện
        $this->load->library(array('ion_auth'));
        $this->lang->load('user');
        $this->load->model(['users_model','groups_model']);
        $this->_data = new Users_model();
        $this->_data_group = new Groups_model();
        $this->_name_controller = $this->router->fetch_class();
    }

    public function index()
    {
        $data['heading_title'] = "Quản lý $this->_name_controller";
        $data['heading_description'] = "Thông tin user";
        /*Breadcrumbs*/
        $this->breadcrumbs->push('Home', base_url());
        $this->breadcrumbs->push($data['heading_title'], '#');
        $data['breadcrumbs'] = $this->breadcrumbs->show();
        /*Breadcrumbs*/
        $data['group_user']=$this->_data_group->get_all_group();
        $data['main_content'] = $this->load->view($this->template_path.$this->_name_controller.'/index', $data, TRUE);
        $this->load->view($this->template_main, $data);
    }

    public function logged(){
        $data = [];
        $data['main_content'] = $this->load->view($this->template_path.$this->_name_controller.'/logged', $data, TRUE);
        $this->load->view($this->template_main, $data);
    }

    public function profile()
    {
        $data = (array) $this->_data->getById($this->session->user_id);
        if ($this->input->post()) {
            if($this->input->post('first_name')) {
                $data['first_name'] = $this->input->post('first_name');
                $data_store['first_name'] = strip_tags(trim($this->input->post('first_name')));
                $this->form_validation->set_rules('first_name', $this->lang->line('error_first_name'), 'trim|min_length[3]|max_length[15]');
            }

            if($this->input->post('last_name')) {
                $data['last_name'] = $this->input->post('last_name');
                $data_store['last_name'] = strip_tags(trim($this->input->post('last_name')));
                $this->form_validation->set_rules('last_name', $this->lang->line('error_last_name'), 'trim|min_length[3]|max_length[15]');
            }

            if($this->input->post('company')) {
                $data['company'] = $this->input->post('company');
                $data_store['company'] = strip_tags(trim($this->input->post('company')));
                $this->form_validation->set_rules('company', $this->lang->line('error_company'), 'trim|min_length[3]|max_length[30]');
            }

            if($this->input->post('phone')) {
                $data['phone'] = $this->input->post('phone');
                $data_store['phone'] = strip_tags(trim($this->input->post('phone')));
                $this->form_validation->set_rules('phone', $this->lang->line('error_phone'), 'trim|min_length[6]|max_length[20]');
            }

            if ($this->input->post('password')) {
                $data['password'] = $this->input->post('password');
                $data['repassword'] = $this->input->post('repassword');
                $data_store['password'] = strip_tags(trim($this->input->post('password')));
                $this->form_validation->set_rules('password', $this->lang->line('error_password'), 'trim|min_length[8]|max_length[20]|matches[repassword]');
                $this->form_validation->set_rules('repassword', $this->lang->line('error_repassword'), 'trim|min_length[8]|max_length[20]');
            }
            if ($this->form_validation->run() != false) {
                if($this->ion_auth->update($this->session->user_id, $data_store)){
                    $message['type'] = 'success';
                    $message['message'] = lang('mess_update_success');
                }else{
                    $message['type'] = 'error';
                    $message['message'] = lang('mess_update_unsuccess');
                }
                $data['message'] = $message;
                $this->session->set_flashdata($message);
                //redirect(current_url());
            }
        }
        $data['main_content'] = $this->load->view($this->template_path.$this->_name_controller.'/profile', $data, TRUE);
        $this->load->view($this->template_main, $data);
    }

    /*
     * Ajax trả về datatable
     * */
    public function ajax_list()
    {
        $list = $this->_data->getData();
        $data = array();
        $no = $this->input->post('start');
        if(!empty($list)) foreach ($list as $item) {
            $no++;
            $row = array();
            $row[] = $item->id;
            $row[] = $item->id;
            $row[] = $item->username;
            $row[] = $item->email;
            $row[] = $item->last_name.' '.$item->first_name;
            $row[] = $item->active ? '<div class="text-center"><span class="label label-success">'.$this->lang->line('status_active').'</span></div>' : '<div class="text-center"><span class="label label-danger">'.$this->lang->line('status_deactive').'</span></div>';
            //thêm action
            $action = '<div class="text-center">';
            $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="'.$this->lang->line('btn_edit').'" onclick="edit_form('."'".$item->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
            if($item->id == 1) {
                $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="' . $this->lang->line('btn_remove') . '"><i class="glyphicon glyphicon-lock"></i></a>';
            }else{
                $action .= '&nbsp;<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="' . $this->lang->line('btn_remove') . '" onclick="delete_item(' . "'" . $item->id . "'" . ')"><i class="glyphicon glyphicon-trash"></i></a>';
            }
            $action .= '</div>';
            $row[] = $action;
            $data[] = $row;
        }

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->_data->getTotalAll(),
            "recordsFiltered" => $this->_data->getTotal(),
            "data" => $data,
        );
        //trả về json
        die(json_encode($output));
    }
    //Load user logged devide recently
    public function ajax_list_logged(){
        if($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $length = $this->input->post('length');
            $no = $this->input->post('start');
            $page = $no/$length + 1;
            $params['page'] = $page;
            $params['Product'] = ['updated_time'=>'DESC'];
            $params['limit'] = $length;
            $list = $this->_data->getDataLoggedDevice($params);
            $data = array();
            foreach ($list as $item) {
                $oneUser = getUser($item->user_id);
                $no++;
                $row = array();
                $row[] = $item->id;
                $row[] = isset($oneUser->username)?$oneUser->username:'';
                $row[] = $item->ip_address;
                $row[] = $item->user_agent;
                $row[] = date('d/m/Y H:i',strtotime($item->created_time));
                $row[] = date('d/m/Y H:i',strtotime($item->updated_time));
                $data[] = $row;
            }

            $output = array(
                "draw" => $this->input->post('draw'),
                "recordsTotal" => $this->_data->getTotalAllLoggedDevice(),
                "recordsFiltered" => $this->_data->getTotalLoggedDevice($params),
                "data" => $data,
            );
            //trả về json
            die(json_encode($output));
        }
        exit;
    }

    /*
     * Ajax lấy thông tin
     * */
    public function ajax_edit($id)
    {
        $data = (array) $this->_data->getById($id);
        $data['group_id'] = $this->ion_auth->get_users_groups($id)->row()->id;
        die(json_encode($data));
    }

    /*
     * Ajax xử lý thêm mới
     * */
    public function ajax_add()
    {
        $this->_validate();
        $identity = strip_tags(trim($this->input->post('username')));
        $password = strip_tags(trim($this->input->post('password')));
        $email = strip_tags(trim($this->input->post('email')));
        $data_store = array();
        if($this->input->post('first_name')){
            $data_store['first_name'] = strip_tags(trim($this->input->post('first_name')));
        }

        if($this->input->post('last_name')){
            $data_store['last_name'] = strip_tags(trim($this->input->post('last_name')));
        }

        if($this->input->post('company')){
            $data_store['company'] = strip_tags(trim($this->input->post('company')));
        }

        if($this->input->post('phone')){
            $data_store['phone'] = strip_tags(trim($this->input->post('phone')));
        }
        $group_id = 2;
        if($this->input->post('group_id')){
            $group_id = strip_tags(trim($this->input->post('group_id')));
        }
        $data_store['active']=1;
        if($this->ion_auth->register($identity, $password, $email, $data_store, array('group_id' => $group_id)) !== false){
            // log action
            $action = $this->router->fetch_class();
            $note = "Insert $action: ".$this->db->insert_id();
            $this->addLogaction($action,$note);
            $message['type'] = 'success';
            $message['message'] = $this->lang->line('mess_add_success');
        }else{
            $message['type'] = 'error';
            $message['message'] = $this->lang->line('mess_add_unsuccess');
        }
        die(json_encode($message));
    }

    /*
     * Cập nhật thông tin
     * */
    public function ajax_update()
    {
        $this->_validate();
        $data_store = [];
        if($this->input->post('first_name')) {
            $data_store['first_name'] = strip_tags(trim($this->input->post('first_name')));
        }

        if($this->input->post('last_name')) {
            $data_store['last_name'] = strip_tags(trim($this->input->post('last_name')));
        }

        if($this->input->post('company')) {
            $data_store['company'] = strip_tags(trim($this->input->post('company')));
        }

        if($this->input->post('phone')) {
            $data_store['phone'] = strip_tags(trim($this->input->post('phone')));
        }

        if ($this->input->post('password')) {
            $data_store['password'] = strip_tags(trim($this->input->post('password')));
        }

        $response = $this->ion_auth->update($this->input->post('id'), $data_store);
        if($response != false){
            // log action
            $action = $this->router->fetch_class();
            $note = "Update $action: ".$this->input->post('id');
            $this->addLogaction($action,$note);
            $message['type'] = 'success';
            $message['message'] = $this->lang->line('mess_update_success');
        }else{
            $message['type'] = 'error';
            $message['message'] = $this->lang->line('mess_update_unsuccess');
        }
        die(json_encode($message));
    }

    /*
     * Xóa một bản ghi
     * */
    public function ajax_delete($id)
    {
        if($id == 1) return;
        $response = $this->_data->delete(['id'=>$id]);
        if($response != false){
            // log action
            $action = $this->router->fetch_class();
            $note = "Update $action: $id";
            $this->addLogaction($action,$note);
            $message['type'] = 'success';
            $message['message'] = $this->lang->line('mess_delete_success');
        }else{
            $message['type'] = 'error';
            $message['message'] = $this->lang->line('mess_delete_unsuccess');
            $message['error'] = $response;
            log_message('error',$response);
        }
        die(json_encode($message));
    }


    private function _validate(){
        if(empty($this->input->post('id'))){
            $this->form_validation->set_rules('username', $this->lang->line('error_username'), 'required|trim|min_length[6]|max_length[15]|is_unique[users.email]');
            $this->form_validation->set_rules('email', $this->lang->line('error_email'), 'required|trim|min_length[5]|max_length[50]|valid_email');
        }

        if(!empty($this->input->post('first_name'))) {
            $this->form_validation->set_rules('first_name', $this->lang->line('error_first_name'), 'trim|min_length[3]|max_length[15]');
        }

        if(!empty($this->input->post('last_name'))) {
            $this->form_validation->set_rules('last_name', $this->lang->line('error_last_name'), 'trim|min_length[3]|max_length[15]');
        }

        if(!empty($this->input->post('company'))) {
            $this->form_validation->set_rules('company', $this->lang->line('error_company'), 'trim|min_length[3]|max_length[30]');
        }

        if(!empty($this->input->post('phone'))) {
            $this->form_validation->set_rules('phone', $this->lang->line('error_phone'), 'trim|min_length[6]|max_length[20]');
        }

        if(empty($this->input->post('id')) || !empty($this->input->post('password'))){
            $this->form_validation->set_rules('password', $this->lang->line('error_password'), 'required|trim|min_length[8]|max_length[20]|matches[repassword]');
            $this->form_validation->set_rules('repassword', $this->lang->line('error_repassword'), 'required|trim|min_length[8]|max_length[20]');
        }

        if($this->input->post('group_id')) {
            $group_id = strip_tags(trim($this->input->post('group_id')));
            $this->ion_auth->remove_from_group(NULL,$this->input->post('id'));
            $this->ion_auth->add_to_group($group_id,$this->input->post('id'));
        }

      if ($this->form_validation->run() == false){
        $message['type'] = "warning";
        $message['message'] = $this->lang->line('mess_validation');
        $message['validation'] = validation_errors('<p>','</p>');
        die(json_encode($message));
      }
    }
}