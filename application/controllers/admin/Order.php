<?php
/**
 * Developer: linhth
 * Controller bài viết
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends Admin_Controller
{
  protected $_data;
  protected $_data_product;

  public function __construct()
  {
    parent::__construct();

    $this->load->helper('status_order');
    //tải file ngôn ngữ
    $this->lang->load(array('order', 'column'));
    //tải model
    $this->load->model('product_model');
    $this->load->model('order_model');
    $this->_data = new Order_model();
    $this->_data_product = new Product_model();
  }

  public function index()
  {
    //chỉ lấy ra những category thuộc mục product
    $data = [];
    $data['main_content'] = $this->load->view($this->template_path . 'order/index', $data, TRUE);
    $this->load->view($this->template_main, $data);
  }

  /*
   * Ajax trả về datatable
   * */
  public function ajax_list()
  {
    if ($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      $length = $this->input->post('length');
      $no = $this->input->post('start');
      $page = $no / $length + 1;
      $params['page'] = $page;
      $params['limit'] = $length;
      $list = $this->_data->getDataOrder($params);
      $data = array();
//      dd($list);
      foreach ($list as $item) {
        $no++;
        $row = array();
        $row[] = $item->id;
        $row[] = '<a href="javascript:;" onclick="view_item(' . $item->id . ')">Đơn hàng #'.$item->id.'</a>';
        $row[] = $item->fullname;
        $row[] = $item->phone;
        $row[] = $item->email;
//                $row[] = '';
        $row[] = $item->shipped_time ? date('d/m/Y', strtotime($item->shipped_time)) : '';
        $row[] = formatMoney($item->total_amount);
        switch ($item->is_status) {
          case 1:
            $row[] = '<span class="label label-default">Chưa xác nhận đơn hàng</span>';
            break;
          case 2:
            $row[] = '<span class="label label-primary">Xác nhận đơn hàng</span>';
            break;
          case 3:
            $row[] = '<span class="label label-success">Hoàn thành</span>';
            break;
          default:
            $row[] = '<span class="label label-danger">Hủy đơn hàng</span>';
            break;
        }
        //thêm action
        $action = '<div class="text-center">';
        $action .= '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="' . $this->lang->line('btn_view') . '" onclick="view_item(' . $item->id . ')"><i class="fa fa-search-plus" aria-hidden="true"></i></a>&nbsp;';
        $action .= '</div>';
        $row[] = $action;
        $data[] = $row;
      }

      $output = array(
        "draw" => $this->input->post('draw'),
        "recordsTotal" => $this->_data->count_all(),
        "recordsFiltered" => $this->_data->getTotal($params),
        "data" => $data,
      );
      //trả về json
//            dd($output);
      echo json_encode($output);
    }
    exit;
  }

  public function ajax_view($id)
  {
    $item = $this->_data->get_by_id($id);
//dd($item);
    $data = array();
    if (!empty($item)) {
      $data['fullname'] = $item->fullname;
      $data['address'] = $item->address;
      $data['phone'] = $item->phone;
      $data['email'] = $item->email;
      $data['note'] = $item->note;
      $data['bill_address'] = $item->bill_address;
      $data['coupon'] = $item->coupon;

      $data['total_amount'] = number_format($item->total_amount) . ' VNĐ';

      $data['created_time'] = date('d/m/Y H:i:s', strtotime($item->created_time));

      $data['shipped_time'] = $item->shipped_time ? date('d/m/Y H:i:s', strtotime($item->shipped_time)) : '';
      switch ($item->is_status) {
        case 1:
          $data['is_status'] = '<span class="label label-default">Chưa xác nhận đơn hàng</span>';
          break;
        case 2:
          $data['is_status'] = '<span class="label label-primary">Xác nhận đơn hàng</span>';
          break;
        case 3:
          $data['is_status'] = '<span class="label label-success">Hoàn thành</span>';
          break;
        default:
          $data['is_status'] = '<span class="label label-danger">Hủy đơn hàng</span>';
          break;
      }
    }
    //trả về json
    echo json_encode($data);
  }

  // Chi tiết order
  private function orderDetail($id){
    $this->_data->delete_by_id();
  }
  public function ajax_update()
  {
    $id = !empty($this->input->post('id')) ? $this->input->post('id') : 0;
    $params['id'] = $id;

    $key = $this->input->post('key');
    $val = $this->input->post('val');

    if ($key == 'payment_status') {
      if ($val == 1) $valNew = 0;
      else $valNew = 1;
      $data[$key] = $valNew;
    } else {
      $data[$key] = $val;
    }
    $this->_data->update($params, $data);


    $output = [
      'message' => 'update success'
    ];
    echo json_encode($output);
  }


  /*
   * Xóa một bản ghi
   * */
  public function ajax_delete($id)
  {
    if ($id == 1) return;
    $this->_data->delete_by_id($id);
    die(json_encode(array("error" => 0)));
  }

  public function export_excel()
  {
    $this->load->library(array('PHPExcel'));
    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator($this->config->item('cms_title'))
      ->setTitle($this->lang->line('heading_title'));


    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A1', 'ID')
      ->setCellValue('B1', lang('col_name'))
      ->setCellValue('C1', lang('col_address'))
      ->setCellValue('D1', lang('col_product_name'))
      ->setCellValue('E1', lang('col_total_amount'))
      ->setCellValue('F1', lang('col_created_at'))
      ->setCellValue('G1', lang('col_transport'))
      ->setCellValue('H1', lang('col_shipped_time'))
      ->setCellValue('I1', lang('col_order_status'))
      ->setCellValue('J1', lang('col_payment'))
      ->setCellValue('K1', lang('col_payment_status'));

    $list = $this->_data->getDataArr();

    $i = 2;
    foreach ($list as $item) {
      $oneTransport = $this->_data->getTransport($item->transport_id);
      $transports = !empty($oneTransport->name) ? $oneTransport->name : 'Chưa chọn nhà vận chuyển';

      switch ($item->is_status) {
        case '0':
          $status = $this->lang->line('status_or_0');
          break;
        case '1':
          $status = $this->lang->line('status_or_1');
          break;
        case '2':
          $status = $this->lang->line('status_or_2');
          break;
        case '3':
          $status = $this->lang->line('status_or_3');
          break;
        case '4':
          $status = $this->lang->line('status_or_4');
          break;
        default:
          $status = $this->lang->line('col_status_1');
          break;
      }

      switch ($item->payment) {
        case 0:
          $payment = $this->lang->line('col_payment_0');
          break;
        case 1:
          $payment = $this->lang->line('col_payment_1');
          break;
        case 2:
          $payment = $this->lang->line('col_payment_2');
          break;
        default:
          $payment = $this->lang->line('col_payment_0');
          break;
      }

      switch ($item->payment_status) {
        case 1:
          $payment_status = $this->lang->line('col_payment_status_1');
          break;
        default:
          $payment_status = $this->lang->line('col_payment_status_0');
          break;
      }

      $listProduct = $this->_data->get_by_order_id($item->id);
      $productName = '';
      if (!empty($listProduct)) foreach ($listProduct as $k => $product) {
        $productModel = new Product_model();
        $oneProduct = $productModel->getById($product->product_id, $this->session->admin_lang, 'tb2.title');
        if ($k != 0) $productName .= ',';
        $productName .= $oneProduct->title;
      }
      // Miscellaneous glyphs, UTF-8
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $i, $item->id)
        ->setCellValue('B' . $i, $item->fullname)
        ->setCellValue('C' . $i, $item->address)
        ->setCellValue('D' . $i, $productName)
        ->setCellValue('E' . $i, number_format($item->total_amount))
        ->setCellValue('F' . $i, date('d/m/Y', strtotime($item->created_time)))
        ->setCellValue('G' . $i, $transports)
        ->setCellValue('H' . $i, date('d/m/Y', strtotime($item->shipped_time)))
        ->setCellValue('I' . $i, $status)
        ->setCellValue('J' . $i, $payment)
        ->setCellValue('K' . $i, $payment_status);
      $i++;
    }
    $nameFile = $this->lang->line('heading_title') . '_' . time();
    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle($nameFile);


    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);


    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

    header('Content-Disposition: attachment;filename="' . $nameFile . '.xlsx"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;
  }

}