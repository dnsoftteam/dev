<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 13/01/2018
 * Time: 2:06 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Search extends Public_Controller {

    protected $_data_post;
    protected $_lang_code;
    protected $_list_category_title_queue;
    protected $_link;
    public function __construct()
    {
        parent::__construct();
        //tải model
        $this->load->model(['product_model','post_model']);

        $this->_data_post = new Post_model();
        //Check xem co chuyen lang hay khong thi set session lang moi
        if($this->input->get('lang'))
            $this->_lang_code = $this->input->get('lang');
        else
            $this->_lang_code = $this->session->public_lang_code;
    }

    public function index($keyword = ''){
        if(empty($keyword)) show_404();
        $keyword = xss_clean($keyword);
        $this->_link = getUrlSearch($keyword);
        $oneItem['title'] = str_replace('-',' ',$keyword);
        $oneItem['url'] =  $this->_link;
        $oneItem = (object) $oneItem;
        $data['oneItem'] = $oneItem;
        $limit = 12;
        $page = $this->input->get('page') ? $this->input->get('page') : 1;
        $data = array_merge($data,$this->loadPost($keyword, $limit, $page));
        $total = $data['total_post'];
        /*Pagination*/
        $this->load->library('pagination');
        $paging['page_query_string'] = TRUE;
        $paging['first_url'] = getUrlSearch($keyword);
        $paging['total_rows'] = $total;
        $paging['per_page'] = $limit;
        $paging['attributes'] = array('class'=>"page-link");
        $this->pagination->initialize($paging);
        $data['pagination'] = $this->pagination->create_links();
        /*Pagination*/

        //SEO Meta
        $data['SEO'] = array(
            'meta_title'        => $oneItem->title,
            'meta_description'  => "Search result: $oneItem->title",
            'meta_keyword'      => "Keyword $oneItem->title",
            'url'               => $oneItem->url,
            'image'             => getImageThumb('',400,200)
        );
        $data['main_content'] = $this->load->view($this->template_path.'search/index', $data, TRUE);
        $this->load->view($this->template_main, $data);
    }

    public function ajax_load_more(){
        if($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $type = $this->input->post('type');
            $page = $this->input->post('page');
            $keyword = $this->input->post('keyword');
            $limit = 3;
            if($type === 'product') $data = $this->loadProduct($keyword, $limit, $page);
            else $data = $this->loadPost($keyword, $limit, $page);

            // Kiểm tra nếu là ajax request thì trả kết quả
            $html = $this->load->view($this->template_path.'search/_ajax_load_more', $data , TRUE);
            print $html;exit;
        }
    }

    public function ajax_autoComplete(){
        if($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $data['keyword'] = $keyword = $this->input->post('keyword');
            $limit = 3;
            $page = 1;
            $listProduct =  $this->loadProduct($keyword, $limit, $page);
            $listPost =$this->loadPost($keyword, $limit, $page);
            $dataMerge = array_merge($listPost['list_post'],$listProduct['list_product']);
            $this->sortArrayByKey($dataMerge,'title',TRUE);
            $data['data'] = $dataMerge;
            $data['total'] = $listProduct['total_product'] + $listPost['total_post'];
            // Kiểm tra nếu là ajax request thì trả kết quả
            $html = $this->load->view($this->template_path.'search/_ajax_load_item', $data , TRUE);
            print $html;exit;
        }
    }

    private function sortArrayByKey(&$array,$key,$string = false,$asc = true){
        $array = json_decode(json_encode($array),TRUE);
        if($string){
            usort($array,function ($a, $b) use(&$key,&$asc)
            {
                if($asc)    return strcmp(strtolower($a{$key}), strtolower($b{$key}));
                else        return strcmp(strtolower($b{$key}), strtolower($a{$key}));
            });
        }else{
            usort($array,function ($a, $b) use(&$key,&$asc)
            {
                if($a[$key] == $b{$key}){return 0;}
                if($asc) return ($a{$key} < $b{$key}) ? -1 : 1;
                else     return ($a{$key} > $b{$key}) ? -1 : 1;

            });
        }
    }


    private function loadPost($keyword, $limit ,$page){
        $this->_data_post = new Post_model();
        //Get data category current
        $params = [
            'is_status'     => 1, //0: Huỷ, 1: Hiển thị, 2: Nháp
            'lang_code'     => $this->_lang_code,
            'search'        => $keyword,
            'limit'         => $limit,
            'page'          => $page
        ];
        $data['list_post'] = $this->_data_post->getData($params);
        $data['total_post'] = $this->_data_post->getTotal($params);

        return $data;
    }

}
