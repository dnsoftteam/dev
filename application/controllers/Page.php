<?php
/**
 * Created by PhpStorm.
 * User: ducto
 * Date: 15/12/2017
 * Time: 2:34 CH
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Public_Controller
{
  protected $_data;
  protected $_data_category;
  protected $_lang_code;
  protected $_all_category;

  public function __construct()
  {
    parent::__construct();
    //tải model
    $this->load->model(['category_model', 'page_model']);
    $this->_data = new Page_model();
    $this->_data_category = new Category_model();
    $this->session->category_type = 'post';
    //Check xem co chuyen lang hay khong thi set session lang moi
    if ($this->input->get('lang'))
      $this->_lang_code = $this->input->get('lang');
    else
      $this->_lang_code = $this->session->public_lang_code;

    if (!$this->cache->get('_all_category_' . $this->session->public_lang_code)) {
      $this->cache->save('_all_category_' . $this->session->public_lang_code, $this->_data_category->getAll($this->session->public_lang_code), 60 * 60 * 30);
    }
    $this->_all_category = $this->cache->get('_all_category_' . $this->session->public_lang_code);
  }

  public function index($slug = null)
  { //Slug là của category page
    //$this->submitContact();
    $id = $this->_data->slugToId($slug);
    $oneItem = $this->_data->getById($id, '', $this->session->public_lang_code);
    if (empty($oneItem)) show_404();
    if ($this->input->get('lang')) redirect(getUrlPage($oneItem));
    $data['oneItem'] = $oneItem;
    switch ($oneItem->style) {
      case 'voucher':
        $data['data_voucher'] = $this->getDataVoucher();
      case 'tour':
        $data['data_tour']=$this->getTour();
    }
    //add breadcrumbs
    $this->breadcrumbs->push($this->lang->line('home'), base_url());
    $this->breadcrumbs->push($oneItem->title, getUrlPage($oneItem));
    $data['breadcrumb'] = $this->breadcrumbs->show();

    $data['SEO'] = array(
      'meta_title' => $oneItem->meta_title,
      'meta_description' => !empty($oneItem->meta_title) ? $oneItem->meta_description : '',
      'meta_keyword' => !empty($oneItem->meta_title) ? $oneItem->meta_keyword : '',
      'url' => getUrlPage($oneItem),
      'image' => getImageThumb($oneItem->thumbnail, 400, 200)
    );
    if (!empty($oneItem->style)) $layoutView = '-' . $oneItem->style;
    else $layoutView = '';
    $data['main_content'] = $this->load->view($this->template_path . 'page/page' . $layoutView, $data, TRUE);
    $this->load->view($this->template_main, $data);
  }

  // lấy dữ liệu voucher
  private function getDataVoucher()
  {
    $this->load->model('voucher_model');
    $voucherModel = new Voucher_model();
    $limit=10;
    $params = array(
      'is_status' => 1,
      'limit' => $limit,
    );
    $data['data'] = $voucherModel->getDataVoucher($params);

    $total = $voucherModel->getTotalVoucher($params);
    $data['total']=ceil($total/$limit);

    return $data;
  }

  private function getTour(){
    $limit=16;
    $this->load->model('tour_model');
    $tourModel = new Tour_model();
    $params = array(
      'is_status' => 1,
      'limit' => $limit,
      'order_by'=>'is_featured'
    );
    $data = $tourModel->getDataTour($params);
    return $data;
  }
  public function _404()
  {
    $data['main_content'] = $this->load->view($this->template_path . 'page/_404', NULL, TRUE);
    $this->load->view($this->template_main, $data);
  }

}