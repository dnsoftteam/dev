<?php
/**
 * Created by PhpStorm.
 * User: askeyh3t
 * Date: 6/20/2018
 * Time: 11:19 AM
 */

class Order extends API_Controller
{
  protected $_data;

  public function __construct()
  {
    parent::__construct();
    $this->load->model('order_model');
    $this->_data = new Order_model();

  }

  public function status_order()
  {
    if ($this->input->server('REQUEST_METHOD') === 'POST') {
      $code = $this->input->post('code');
      $status = $this->input->post('status');
      $rules = array(
        array(
          'field' => 'code',
          'label' => 'Mã đơn hàng',
          'rules' => 'required|trim'
        ),
        array(
          'field' => 'status',
          'label' => 'Trạng thái đơn hàng',
          'rules' => 'required|trim'
        )
      );
      $this->form_validation->set_rules($rules);
      if ($this->form_validation->run() != FALSE) {
        $order = $this->_data->getOrderByCode($code, 'id');
        if (!empty($order)) {
          $response = $this->_data->update(array('code' => $code), array('is_status' => $status));
          if ($response == true) {
            $message['message'] = 'Cập nhật đơn hàng thành công!.';
            $message['type'] = true;
          } else {
            $message['message'] = 'Cập nhật không thành công!.';
            $message['type'] = false;
          }
        } else {
          $message['message'] = 'Không tồn tại đơn hàng có mã ERP là: ' . $code;
          $message['type'] = false;
        }
      } else {
        $message['message'] = validation_errors('<p>','<p>');
        $message['type'] = false;
      }

      $message['code'] = self::RESPONSE_SUCCESS;
      die(json_encode($message));
    }

  }

}