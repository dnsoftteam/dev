<?php
/**
 * Created by PhpStorm.
 * User: askeyh3t
 * Date: 6/20/2018
 * Time: 11:19 AM
 */

class Product extends API_Controller
{
  public function __construct()
  {
    parent::__construct();
  }


  // Cập nhật kho hàng của 1 sản phẩm
  public function updateProduct()
  {
    if ($this->input->server('REQUEST_METHOD') === 'POST') {

      $code = $this->input->post('code');
      $store = $this->input->post('store');
      $rules=array(
        array(
          'field' => 'code',
          'label' => 'Mã sản phẩm',
          'rules' => 'required|trim'
        ),
        array(
          'field' => 'store',
          'label' => 'Số lượng sản phẩm',
          'rules' => 'required|trim'
        )
      );
      $this->form_validation->set_rules($rules);
      if ($this->form_validation->run() != FALSE) {
        $this->load->model('product_model');
        $productModel = new Product_model();
        $params = array(
          'code' => $code,
          'limit' => 1
        );
        $message = array();
        $data = $productModel->getDataProduct($params);
        if (!empty($data)) {
          $field = array(
            'store' => $store
          );
          $responsive = $productModel->update(array('id' => $data[0]->id), $field, $productModel->table);

          if ($responsive != false) {
            $message['message'] = 'Cập nhật thành công';
            $message['type'] = true;

          } else {
            $message['message'] = 'Cập nhật không thành công';
            $message['type'] = false;
          }

        } else {
          $message['message'] = 'Không có sản phẩm nào có mã là ' . $code;
          $message['type'] = false;
        }
      } else {
        $message['message'] = validation_errors('<p>','<p>');
        $message['type'] = false;
      }
    }
    $message['code'] = self::RESPONSE_SUCCESS;
    die(json_encode($message));
  }
}