<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Public_Controller
{
  protected $_all_category;
  protected $_data_category;

  public function __construct()
  {
    parent::__construct();
    $this->load->model('category_model');
    $this->_data_category = new Category_model();
    if (!$this->cache->get('_all_category_' . $this->session->public_lang_code)) {
      $this->cache->save('_all_category_' . $this->session->public_lang_code, $this->_data_category->getAll($this->session->public_lang_code), 60 * 60 * 30);
    }
    $this->_all_category = $this->cache->get('_all_category_' . $this->session->public_lang_code);
  }

  public function index()
  {
    $data['list_tour'] = $this->getTour();
    $data['list_voucher'] = $this->getVoucher();
    $data['list_cat_featured'] = $this->getCategoryFeatured();
    $data['list_news'] = $this->listNews();
    $data['main_content'] = $this->load->view($this->template_path . 'home/index', $data, TRUE);
    $this->load->view($this->template_main, $data);
  }

  private function getTour()
  {
    $this->load->model('tour_model');
    $tourModel = new Tour_model();
    $params = array(
      'is_status' => 1,
      'limit' => 8,
      'curent_date' => 1
    );
    $data = $tourModel->getDataTour($params, 'object', array('ap_tour.id',
      'ap_tour.price',
      'ap_tour.price_sale',
      'ap_tour.company',
      'ap_tour.thumbnail',
      'ap_tour.day',
      'ap_tour.departure_date',
      'ap_tour.percent',
      'ap_tour.from',
      'ap_tour_translations.title',
      'ap_tour_translations.slug'));
    return $data;
  }

  private function getVoucher()
  {
    $this->load->model('voucher_model');
    $voucherModel = new Voucher_model();
    $params = array(
      'is_status' => 1,
      'limit' => 8,
      'curent_date' => 1
    );
    $data = $voucherModel->getDataVoucher($params, 'object', array('ap_voucher.id',
      'ap_voucher.price',
      'ap_voucher.price_sale',
      'ap_voucher.company',
      'ap_voucher.thumbnail',
      'ap_voucher.hotel',
      'ap_voucher.address',
      'ap_voucher.expiration_time',
      'ap_voucher.percent',
      'ap_voucher.ratting',
      'ap_voucher_translations.title',
      'ap_voucher_translations.slug'));
    return $data;
  }

  private function getCategoryFeatured($type = 'national')
  {
    $this->load->model('category_model');
    $categoryModel = new Category_model();
    $params = array(
      'limit' => 16,
      'category_type' => $type,
      'is_status' => 1,
      'is_featured' => 1
    );
    $data = $categoryModel->getData($params);
    return $data;
  }

  private function listNews()
  {
    $this->load->model(['post_model']);
    $postModel = new Post_model();
    /*Lay list id con của category*/
    $params = array(
      'lang_code' => $this->session->public_lang_code,
      'is_status' => 1,
      'order_by' => 'is_featured',
      'limit' => 5
    );
    $data = $postModel->getData($params);
    return $data;
  }

  public function loadCompase()
  {
    if ($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      $data = $this->input->post();
      echo $this->load->view($this->template_path . '_block/modal-compase', $data, TRUE);
      exit();
    }
  }

  public function addCompase()
  {
    if ($this->input->server('REQUEST_METHOD') == 'POST' && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      $data = $this->input->post();
      $message = array();
      $list_compare = array();
      if (!empty($data)) {
        if (!empty($this->session->userdata['compare'])) {
          $list_compare = $this->session->userdata['compare'];
//          $this->session->unset_userdata('compare');
//          dd(count($list_compare));
          if (count($list_compare) >= 3) {
            $message['type'] = 'warning';
            $message['message'] = 'Bạn chỉ so sánh 3 tour cùng 1 lúc.';
            die(json_encode($message));
          } else {
            if (in_array($data['id'], $this->session->userdata['compare'])) {
              $message['type'] = 'warning';
              $message['message'] = 'Bạn đã thêm tour rồi.';
              die(json_encode($message));
            } else {
              $message['type'] = 'success';
              $message['message'] = 'Bạn đã thêm thành công.';

              $list_compare[] = $data['id'];
              $this->session->userdata['compare'] = $list_compare;
              die(json_encode($message));
            }
          }
        } else {
          $list_compare[] = $data['id'];
          $this->session->userdata['compare'] = $list_compare;
          $message['type'] = 'success';
          $message['message'] = 'Bạn đã thêm thành công.';
        }
        die(json_encode($message));
      }

    }
  }

  public function compare()
  {
    $data = array();
    $data['main_content'] = $this->load->view($this->template_path . 'compare', $data, TRUE);
    $this->load->view($this->template_main, $data);
  }

  public function load_list_compare()
  {
    echo $this->load->view($this->template_path . '_block/_content_compare', '', TRUE);
    exit();
  }
  public function load_destinations()
  {
    echo $this->load->view($this->template_path . '_block/search-destinations', array('home'=>'show'), TRUE);
    exit();
  }
}
