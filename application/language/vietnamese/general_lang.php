<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Danh sách text
$lang['text_home']              = 'Trang chủ';
$lang['text_dash']              = 'Bảng điều khiển';
$lang['text_user']              = 'Thành viên';
$lang['text_user_logged']              = 'Đăng nhập thiết bị gần đây';
$lang['text_group']             = 'Nhóm thành viên';
$lang['text_profile']           = 'Hồ sơ';
$lang['text_setting']           = 'Cài đặt';
$lang['text_category']          = 'Danh mục';
$lang['text_newsletter']        = 'Danh sách email';
$lang['text_media_manager']     = 'Quản lý media';
$lang['text_post']              = 'Bài viết';
$lang['text_partner']              = 'Đối tác';
$lang['text_showroom']          = 'Showroom';
$lang['text_more_info']         = 'Xem chi tiết';
$lang['text_id']         = 'ID';
$lang['text_title']         = 'Tên';
$lang['text_description']         = 'Mô tả';
$lang['text_content']         = 'Mô tả';
$lang['text_meta_title']         = 'Thẻ meta title';
$lang['text_meta_description']         = 'Thẻ meta description';
$lang['text_keyword']         = 'Từ khóa';
$lang['text_username']         = 'Username';
$lang['text_lastname']         = 'Họ';
$lang['text_firstname']         = 'Tên';
$lang['text_fullname']         = 'Họ và tên';
$lang['text_sort']         = 'Sắp xếp';
$lang['text_price']         = 'Giá';
$lang['text_banner_position']         = 'Vị trí banner';
$lang['text_thumbnail']         = 'Ảnh banner';
$lang['text_permission']='Phân quyền';
$lang['text_email']         = 'Email';
$lang['text_status']         = 'Trạng thái';
$lang['text_featured']         = 'Nổi bật';
$lang['text_created_time']         = 'Ngày tạo';
$lang['text_updated_time']         = 'Ngày cập nhật';
$lang['text_action']         = 'Hành động';

$lang['text_view'] = 'Xem';
$lang['text_add'] = 'Thêm';
$lang['text_edit'] = 'Sửa';
$lang['text_delete'] = 'Xóa';
$lang['text_import'] = 'Import';
$lang['text_export'] = 'Export';

//Tab panel
$lang['tab_language']       = 'Ngôn ngữ';


//Button
$lang['btn_add']               = 'Thêm';
$lang['btn_update']               = 'Cập nhật';
$lang['btn_edit']              = 'Sửa';
$lang['btn_remove']            = 'Xóa';
$lang['btn_copy']              = 'Nhân bản';
$lang['btn_reload']            = 'Tải lại';
$lang['btn_save']              = 'Lưu';
$lang['btn_saving']            = 'Đang lưu';
$lang['btn_cancel']            = 'Hủy';
$lang['btn_profile']           = 'Hồ sơ';
$lang['btn_logout']            = 'Thoát';
$lang['btn_yes']               = 'Đồng ý';
$lang['btn_no']                = 'Không';
$lang['btn_export_excel']      = 'Xuất Excel';
$lang['btn_select_items']      = 'Chọn';
$lang['btn_select_image']      = 'Chọn ảnh';
$lang['btn_select_document']      = 'Chọn tài liệu';
$lang['btn_select_video']      = 'Chọn video';


//Danh sách nút media
$lang['btn_upload']             = 'Tải lên';
$lang['btn_creat_folder']       = 'Tạo thư mục';
$lang['btn_rename']             = 'Đổi tên';

//Trạng thái
$lang['text_status_0']               = 'Hủy';
$lang['text_status_1']               = 'Hiển thị';
$lang['text_status_2']               = 'Nháp';

// Trạng thái đơn hàng
$lang['text_status_or_1']               = 'Chờ xác nhận';
$lang['text_status_or_2']               = 'Chờ giao hàng';
$lang['text_status_or_3']               = 'Đang giao hàng';
$lang['text_status_or_4']               = 'Đơn hàng hoàn thành';
$lang['text_status_or_0']               = 'Hủy đơn hàng';

// Trạng thái thanh toán đơn hàng
$lang['text_status_pay_0']               = 'Chưa thanh toán';
$lang['text_status_pay_1']               = 'Đã thanh toán';

// Phương pháp thanh toán
$lang['payment_0']               = 'COD';
$lang['payment_1']               = 'Chuyển khoản';
$lang['payment_2']               = 'Thẻ ATM/VISA/MASTER';

//Thông báo chung
$lang['mess_alert_title']               = 'Thông báo';
$lang['mess_alert_waiting']             = 'Hệ thống đang xử lý vui lòng chờ';
$lang['mess_confirm_delete']            = 'Bạn có chắc chắng muốn xóa không!';
$lang['mess_confirm_copy']              = 'Bạn có muốn thực hiện nhân bản không!';
$lang['mess_add_success']               = 'Thêm thành công';
$lang['mess_edit_success']              = 'Sửa thành công';
$lang['mess_delete_success']            = 'Xóa thành công';
$lang['mess_update_success']            = 'Cập nhật thành công';
$lang['mess_update_unsuccess']          = 'Cập nhật không thành công';
$lang['mess_add_unsuccess']             = 'Thêm mới không thành công';
$lang['mess_delete_unsuccess']          = 'Xóa không thành công';
$lang['mess_validation']                = 'Vui lòng kiểm tra lại thông tin !';

//Thông báo lỗi
$lang['error_add']                 = 'Không thể thêm';
$lang['error_edit']                 = 'Không thể sửa';
$lang['error_remove']                 = 'Không thể xóa';
$lang['error_username']            = 'Tài khoản chưa nhập hoặc không năm trong khoảng 6-15 ký tự';
$lang['error_first_name']          = 'Tên chưa nhập hoặc không năm trong khoảng 3-15 ký tự';
$lang['error_last_name']           = 'Họ chưa nhập hoặc không năm trong khoảng 3-15 ký tự';
$lang['error_email']               = 'Email chưa nhập hoặc email của bạn không đúng';
$lang['error_password']            = 'Mật khẩu chưa nhập hoặc không năm trong khoảng 8-20 ký tự';
$lang['error_repassword']          = 'Nhập lại mật khẩu chưa nhập hoặc không năm trong khoảng 8-20 ký tự';
$lang['error_company']             = 'Công ty chưa nhập phải năm trong khoảng 3-30 ký tự';
$lang['error_phone']               = 'Số điện thoại chưa nhập của bạn không đúng hoặc không năm trong khoảng 6-20 ký tự';
$lang['error_try_again']           = 'Có lỗi xảy ra vui lòng thử lại';
$lang['error_name']                = 'Tên chưa nhập hoặc không năm trong khoảng 3-15 ký tự';
$lang['error_order']               = 'Sắp xếp phải là số từ 0-n';
$lang['error_choose_delete']       = 'Bạn chưa chọn đối tượng cần xóa';
$lang['error_choose_copy']         = 'Bạn chưa chọn đối tượng cần nhân bản';
$lang['error_title']               = 'tên';
$lang['error_tel']                 = 'số điện thoại';
$lang['error_address']             = 'địa chỉ';
$lang['error_location']            = 'định vị google maps';
$lang['error_city_id']             = 'tỉnh/thành phố';
$lang['error_district_id']         = 'quận/huyện';
$lang['error_manufactures_id']     = 'thương hiệu';
$lang['error_image']     = 'Ảnh đại diện';


//Ngôn ngữ
$lang['lang_en']               = 'Tiếng anh';
$lang['lang_vi']               = 'Tiếng việt';

//Tab
$lang['tab_info']               = 'Thông tin';
$lang['tab_image']              = 'Hình ảnh';
$lang['tab_image_video']              = 'Hình ảnh & Video';
$lang['tab_setting']              = 'Cấu hình';

//Danh sách tooltip
$lang['tooltip_export_excel']               = 'Xuất dữ liệu ra file excel';

$lang['tab_per']                = 'Phân quyền';