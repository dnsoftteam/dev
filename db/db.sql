/*
SQLyog Enterprise v12.09 (64 bit)
MySQL - 10.1.13-MariaDB : Database - sharetour
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ap_banner` */

DROP TABLE IF EXISTS `ap_banner`;

CREATE TABLE `ap_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position_id` tinyint(5) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `url_video` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `is_status` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT 'trang thai',
  `displayed_time` datetime NOT NULL COMMENT 'ngay publish',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngay tao',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngay sua',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ap_banner` */

/*Table structure for table `ap_banner_translations` */

DROP TABLE IF EXISTS `ap_banner_translations`;

CREATE TABLE `ap_banner_translations` (
  `id` int(11) DEFAULT NULL,
  `language_code` varchar(5) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `content` longtext,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(170) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  UNIQUE KEY `ap_banner_translations_id_language_code_pk` (`id`,`language_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ap_banner_translations` */

/*Table structure for table `ap_career` */

DROP TABLE IF EXISTS `ap_career`;

CREATE TABLE `ap_career` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `file_cv` varchar(255) DEFAULT NULL,
  `file_letter` varchar(255) DEFAULT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ap_newsletter_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `ap_career` */

insert  into `ap_career`(`id`,`post_id`,`phone`,`name`,`email`,`address`,`file_cv`,`file_letter`,`created_time`) values (2,NULL,'0912354353','linh','linh116@gmail.com','dfsg','cv/linh116_gmail_com_cv.pdf','cv/linh116_gmail_com_coverletter.pdf','2018-04-16 10:51:16'),(11,NULL,'09253647657','linh','linhlinh26111995@gmail.com','fgdf','cv/linhlinh26111995_gmail_com_cv.pdf','','2018-04-16 11:41:05'),(12,NULL,'0976734635','jh','hoanglinh116@gmail.com','xbcxvb','cv/hoanglinh116_gmail_com_cv.pdf','','2018-04-16 11:44:52'),(14,NULL,'0923532647','sfgdg','linh@gmail.com','sgdfhgdf','cv/linh_gmail_com_cv.pdf','','2018-04-16 11:48:12'),(15,NULL,'09231534635','hoang thị linh','quang@gmail.com','hà nội','cv/quang_gmail_com_cv.pdf','','2018-04-16 11:51:00'),(19,NULL,'915224935','Steven Toan','ductoan1991@gmail.com','Ha Noi, Vietnam','cv/ductoan1991_gmail_com_cv.pdf','','2018-04-16 13:11:36'),(20,NULL,'09746655342','linh','thuy116@gmail.com','fdhgfhf','cv/thuy116_gmail_com_cv.pdf','','2018-04-16 13:18:12'),(21,NULL,'09253464576','Hoàng thị Linh','trung@gmail.com','fgdgd','cv/trung_gmail_com_cv.pdf','','2018-04-16 13:19:32');

/*Table structure for table `ap_category` */

DROP TABLE IF EXISTS `ap_category`;

CREATE TABLE `ap_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `thumbnail` varchar(255) DEFAULT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `image_bg` varchar(255) DEFAULT NULL COMMENT 'Background for parent_id = 0',
  `files` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL COMMENT 'style html',
  `class` varchar(100) DEFAULT NULL,
  `type` varchar(255) DEFAULT 'post' COMMENT 'type theo đúng tên controller',
  `order` int(3) DEFAULT '0',
  `is_status` tinyint(2) NOT NULL DEFAULT '1',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `ratting` mediumtext,
  `retionship` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Data for the table `ap_category` */

insert  into `ap_category`(`id`,`parent_id`,`thumbnail`,`banner`,`image_bg`,`files`,`style`,`class`,`type`,`order`,`is_status`,`created_time`,`updated_time`,`created_by`,`updated_by`,`ratting`,`retionship`) values (1,0,'','',NULL,'','','','national',1,1,'2018-05-31 13:42:23','2018-05-31 17:21:02',0,0,'{\"9\":{\"name\":\"Chu Quang Khanh\",\"title\":\"H\\u00e0 n\\u1ed9i, \\u0110\\u00e0 n\\u1eb5ng, H\\u1ed3 ch\\u00ed minh\",\"vote\":\"2\",\"desc\":\"H\\u00e0 n\\u1ed9i, \\u0110\\u00e0 n\\u1eb5ng, H\\u1ed3 ch\\u00ed minh\",\"date\":\"\"},\"10\":{\"name\":\"Chu Quang Khanh 1\",\"title\":\"H\\u00e0 n\\u1ed9i, \\u0110\\u00e0 n\\u1eb5ng, H\\u1ed3 ch\\u00ed minh\",\"vote\":\"4\",\"desc\":\"H\\u00e0 n\\u1ed9i, \\u0110\\u00e0 n\\u1eb5ng, H\\u1ed3 ch\\u00ed minh\",\"date\":\"\"}}',NULL),(2,0,'','',NULL,'','','','national',2,1,'2018-05-31 13:42:46','2018-05-31 17:21:33',0,0,'{\"1\":{\"name\":\"Chu Quang Khanh\",\"title\":\"H\\u00e0 n\\u1ed9i, \\u0110\\u00e0 n\\u1eb5ng, H\\u1ed3 ch\\u00ed minh\",\"vote\":\"4\",\"desc\":\"H\\u00e0 n\\u1ed9i, \\u0110\\u00e0 n\\u1eb5ng, H\\u1ed3 ch\\u00ed minh\",\"date\":\"\"},\"2\":{\"name\":\"Chu Quang Khanh 2\",\"title\":\"H\\u00e0 n\\u1ed9i, \\u0110\\u00e0 n\\u1eb5ng, H\\u1ed3 ch\\u00ed minh\",\"vote\":\"5\",\"desc\":\"H\\u00e0 n\\u1ed9i, \\u0110\\u00e0 n\\u1eb5ng, H\\u1ed3 ch\\u00ed minh\",\"date\":\"\"}}',NULL),(3,0,'','',NULL,'','','','national',3,1,'2018-05-31 13:43:02','2018-05-31 13:43:02',0,0,NULL,NULL),(4,0,'','',NULL,'','','','national',4,1,'2018-05-31 13:44:05','2018-05-31 13:44:05',0,0,NULL,NULL),(5,0,'','',NULL,'','','','national',5,1,'2018-05-31 13:44:23','2018-05-31 13:44:23',0,0,NULL,NULL),(6,0,'','',NULL,'','','','national',6,1,'2018-05-31 13:44:34','2018-06-05 10:48:23',0,0,NULL,NULL),(7,0,'','',NULL,'','','','national',7,1,'2018-05-31 13:45:08','2018-06-05 10:31:04',0,0,NULL,'[\"1\"]'),(8,1,'',NULL,NULL,NULL,'','','national',8,1,'2018-06-01 09:01:34','2018-06-01 09:02:01',0,0,'{\"2\":{\"name\":\"Chu Quang Khanh\",\"title\":\"H\\u00e0 n\\u1ed9i, \\u0110\\u00e0 n\\u1eb5ng, H\\u1ed3 ch\\u00ed minh\",\"vote\":\"4\",\"desc\":\"H\\u00e0 N\\u1ed9i\",\"date\":\"\"}}',NULL),(9,1,'',NULL,NULL,NULL,'','','national',9,1,'2018-06-01 09:03:06','2018-06-01 09:03:06',0,0,NULL,NULL),(10,3,'',NULL,NULL,NULL,'','','national',1,1,'2018-06-01 09:03:38','2018-06-01 09:03:38',0,0,NULL,NULL),(11,3,'',NULL,NULL,NULL,'','','national',8,1,'2018-06-01 09:03:53','2018-06-01 09:04:02',0,0,NULL,NULL),(12,0,'','',NULL,'','','','voucher',1,1,'2018-06-05 11:10:22','2018-06-05 11:10:22',0,0,NULL,NULL),(13,0,'','',NULL,'','','','voucher',2,1,'2018-06-05 11:11:01','2018-06-05 11:11:01',0,0,NULL,NULL);

/*Table structure for table `ap_category_translations` */

DROP TABLE IF EXISTS `ap_category_translations`;

CREATE TABLE `ap_category_translations` (
  `id` int(11) NOT NULL,
  `language_code` char(2) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `slug` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `content` mediumtext NOT NULL,
  UNIQUE KEY `ap_category_translations_id_language_code_pk` (`id`,`language_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ap_category_translations` */

insert  into `ap_category_translations`(`id`,`language_code`,`title`,`description`,`slug`,`meta_title`,`meta_description`,`meta_keyword`,`content`) values (1,'vi','Việt Nam','Hà nội, Đà nẵng, Hồ chí minh','viet-nam','Việt Nam','Việt Nam','Việt Nam',''),(2,'vi','Đông á','Đông á','dong-a','Đông á','Đông á','Đông á',''),(3,'vi','Đông Nam Á','Đông Nam Á','dong-nam-a','Đông Nam Á','Đông Nam Á','Đông Nam Á',''),(4,'vi','Nam Á / Trung Đông','Nam Á / Trung Đông','nam-a-trung-dong','Nam Á / Trung Đông','Nam Á / Trung Đông','Nam Á / Trung Đông',''),(5,'vi','Châu đại dương','Châu đại dương','chau-dai-duong','Châu đại dương','Châu đại dương','Châu đại dương',''),(6,'vi','Châu Âu','Châu Âu','chau-au','Châu Âu','Châu Âu','Châu Âu',''),(7,'vi','Bắc Mỹ','Bắc Mỹ','bac-my','Bắc Mỹ','Bắc Mỹ','Bắc Mỹ',''),(8,'vi','Hà Nội','Hà Nội','ha-noi','Hà Nội','Hà Nội','',''),(9,'vi','Thành phố Hồ Chí Minh','Thành phố Hồ Chí Minh','thanh-pho-ho-chi-minh','Thành phố Hồ Chí Minh','Thành phố Hồ Chí Minh','',''),(10,'vi','Thái Lan','Thái Lan','thai-lan','Thái Lan','Thái Lan','Thái Lan',''),(11,'vi','Lào','Lào','lao','Lào','Lào','Lào',''),(12,'vi','Đã Nẵng','Đã Nẵng','da-nang','Đã Nẵng','Đã Nẵng','Đã Nẵng',''),(13,'vi','Nha Trang','Nha Trang','nha-trang','Nha Trang','Nha Trang','Nha Trang','');

/*Table structure for table `ap_groups` */

DROP TABLE IF EXISTS `ap_groups`;

CREATE TABLE `ap_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `permission` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `ap_groups` */

insert  into `ap_groups`(`id`,`name`,`description`,`permission`) values (1,'admin','Administrator',NULL),(2,'Biên tập viên','Nhóm biên tập quản trị nội dung web','{\"banner\":{\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"groups\":{\"view\":\"1\"},\"media\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"menus\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"page\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"post\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"setting\":{\"view\":\"1\",\"add\":\"1\",\"edit\":\"1\",\"delete\":\"1\",\"import\":\"1\",\"export\":\"1\"},\"users\":{\"view\":\"1\"}}'),(3,'Nhóm đại lý','Hệ thống đại lý',NULL);

/*Table structure for table `ap_log_action` */

DROP TABLE IF EXISTS `ap_log_action`;

CREATE TABLE `ap_log_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(255) NOT NULL,
  `note` varchar(255) NOT NULL,
  `uid` int(11) NOT NULL,
  `created_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1293 DEFAULT CHARSET=utf8;

/*Data for the table `ap_log_action` */

insert  into `ap_log_action`(`id`,`action`,`note`,`uid`,`created_time`) values (1250,'post','Insert post: 0',1,'2018-05-30 12:35:04'),(1251,'post','Update post: 27',1,'2018-05-30 12:35:09'),(1252,'category','Insert category: 0',1,'2018-05-31 01:42:24'),(1253,'category','Insert category: 0',1,'2018-05-31 01:42:46'),(1254,'category','Insert category: 0',1,'2018-05-31 01:43:03'),(1255,'category','Insert category: 0',1,'2018-05-31 01:44:05'),(1256,'category','Insert category: 0',1,'2018-05-31 01:44:23'),(1257,'category','Insert category: 0',1,'2018-05-31 01:44:34'),(1258,'category','Update category: 6',1,'2018-05-31 01:44:57'),(1259,'category','Insert category: 0',1,'2018-05-31 01:45:08'),(1260,'category','Update category: 1',1,'2018-05-31 04:30:53'),(1261,'category','Update category: 1',1,'2018-05-31 04:45:21'),(1262,'category','Update category: 1',1,'2018-05-31 04:46:28'),(1263,'category','Update category: 1',1,'2018-05-31 04:46:49'),(1264,'category','Update category: 1',1,'2018-05-31 04:58:11'),(1265,'category','Update category: 1',1,'2018-05-31 05:03:17'),(1266,'category','Update category: 1',1,'2018-05-31 05:04:40'),(1267,'category','Update category: 1',1,'2018-05-31 05:10:05'),(1268,'category','Update category: 1',1,'2018-05-31 05:11:53'),(1269,'category','Update category: 1',1,'2018-05-31 05:21:03'),(1270,'category','Update category: 2',1,'2018-05-31 05:21:34'),(1271,'category','Insert category: 0',1,'2018-06-01 09:01:34'),(1272,'category','Update category: 8',1,'2018-06-01 09:02:01'),(1273,'category','Insert category: 0',1,'2018-06-01 09:03:06'),(1274,'category','Insert category: 0',1,'2018-06-01 09:03:38'),(1275,'category','Insert category: 0',1,'2018-06-01 09:03:53'),(1276,'category','Update category: 11',1,'2018-06-01 09:04:02'),(1277,'tour','Update tour: 1',1,'2018-06-05 12:10:26'),(1278,'tour','Insert tour: 0',1,'2018-06-05 12:13:21'),(1279,'tour','Update tour: 3',1,'2018-06-05 01:33:07'),(1280,'tour','Update tour: 3',1,'2018-06-05 01:37:17'),(1281,'tour','Update tour: 3',1,'2018-06-05 01:39:53'),(1282,'tour','Update tour: 3',1,'2018-06-05 02:10:27'),(1283,'tour','Update tour: 3',1,'2018-06-05 08:50:48'),(1284,'post','Insert post: 0',1,'2018-06-05 10:08:42'),(1285,'post','Insert post: 0',1,'2018-06-05 10:17:17'),(1286,'category','Update category: 7',1,'2018-06-05 10:18:33'),(1287,'category','Update category: 7',1,'2018-06-05 10:31:05'),(1288,'category','Insert category: 0',1,'2018-06-05 11:10:22'),(1289,'category','Insert category: 0',1,'2018-06-05 11:11:01'),(1290,'voucher','Insert voucher: 0',1,'2018-06-05 01:46:53'),(1291,'voucher','Update voucher: 1',1,'2018-06-05 01:59:24'),(1292,'voucher','Update voucher: 1',1,'2018-06-05 02:05:18');

/*Table structure for table `ap_logged_device` */

DROP TABLE IF EXISTS `ap_logged_device`;

CREATE TABLE `ap_logged_device` (
  `user_id` bigint(20) NOT NULL,
  `ip_address` varchar(45) NOT NULL COMMENT 'IP client',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `device_code` char(32) NOT NULL COMMENT 'Mã md5 thiết bị + key secret',
  `user_agent` varchar(255) NOT NULL COMMENT 'Tên thiết bị',
  UNIQUE KEY `ap_logged_device_user_id_ip_address_user_agent_pk` (`user_id`,`ip_address`,`user_agent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ap_logged_device` */

/*Table structure for table `ap_login_attempts` */

DROP TABLE IF EXISTS `ap_login_attempts`;

CREATE TABLE `ap_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ap_login_attempts` */

/*Table structure for table `ap_menus` */

DROP TABLE IF EXISTS `ap_menus`;

CREATE TABLE `ap_menus` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(75) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#',
  `parent_id` int(2) NOT NULL DEFAULT '0',
  `order` tinyint(2) DEFAULT NULL,
  `class` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_id` smallint(4) NOT NULL,
  `language_code` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_menus` */

/*Table structure for table `ap_newsletter` */

DROP TABLE IF EXISTS `ap_newsletter`;

CREATE TABLE `ap_newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ap_newsletter_email_uindex` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `ap_newsletter` */

insert  into `ap_newsletter`(`id`,`email`,`created_time`) values (1,'aa@gmail.com','2018-05-23 10:57:07'),(3,'steven.mucian@gmail.com','2018-05-23 11:39:54'),(7,'maianh.apecsoft@gmail.com','2018-05-28 11:13:46'),(8,'maianh.crmviet@gmail.com','2018-05-28 12:01:00');

/*Table structure for table `ap_page` */

DROP TABLE IF EXISTS `ap_page`;

CREATE TABLE `ap_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `style` varchar(20) DEFAULT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `banner` varchar(255) DEFAULT NULL,
  `is_status` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT 'trang thai',
  `file` varchar(255) DEFAULT NULL,
  `time_thanhlap` datetime NOT NULL,
  `displayed_time` datetime NOT NULL COMMENT 'ngay publish',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngay tao',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngay sua',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ap_page` */

/*Table structure for table `ap_page_translations` */

DROP TABLE IF EXISTS `ap_page_translations`;

CREATE TABLE `ap_page_translations` (
  `id` int(11) DEFAULT NULL,
  `language_code` varchar(5) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `content` longtext,
  `file_timeline` varchar(255) DEFAULT NULL,
  `content_more` longtext,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(170) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  UNIQUE KEY `ap_page_translations_id_language_code_pk` (`id`,`language_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ap_page_translations` */

/*Table structure for table `ap_partner` */

DROP TABLE IF EXISTS `ap_partner`;

CREATE TABLE `ap_partner` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: Khach hang; 1: Nha cung capap',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `area_id` int(5) DEFAULT NULL COMMENT 'Vùng xuất khẩu',
  `city_id` int(5) DEFAULT NULL,
  `district_id` int(5) DEFAULT NULL,
  `latitude` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `open_time` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_status` tinyint(2) DEFAULT '1',
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_partner` */

/*Table structure for table `ap_post` */

DROP TABLE IF EXISTS `ap_post`;

CREATE TABLE `ap_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_crawler` int(11) DEFAULT NULL,
  `category_product` varchar(255) DEFAULT 'NULL' COMMENT 'List ID cate product',
  `thumbnail` varchar(255) NOT NULL,
  `album` text,
  `url_video` varchar(255) DEFAULT NULL,
  `is_status` tinyint(2) unsigned NOT NULL DEFAULT '2' COMMENT 'trang thai',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `displayed_time` datetime DEFAULT NULL COMMENT 'ngay publish',
  `program` tinyint(1) NOT NULL DEFAULT '0',
  `number` varchar(5) DEFAULT '1',
  `viewed` bigint(20) NOT NULL DEFAULT '0',
  `type` varchar(100) DEFAULT NULL COMMENT 'Partime/fulltime',
  `type_career` int(5) DEFAULT NULL COMMENT 'Loại hình làm việc',
  `level` varchar(50) DEFAULT NULL COMMENT 'Trình độ',
  `address` varchar(255) DEFAULT NULL COMMENT 'Địa điểm làm việc',
  `address_career` varchar(255) DEFAULT NULL COMMENT 'Chi nhánh',
  `expiration_time` date NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'ngay tao',
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'ngay sua',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ap_post` */

insert  into `ap_post`(`id`,`id_crawler`,`category_product`,`thumbnail`,`album`,`url_video`,`is_status`,`is_featured`,`displayed_time`,`program`,`number`,`viewed`,`type`,`type_career`,`level`,`address`,`address_career`,`expiration_time`,`created_time`,`updated_time`) values (1,NULL,'NULL','',NULL,'',1,0,'0000-00-00 00:00:00',0,'1',0,NULL,NULL,NULL,NULL,NULL,'0000-00-00','2018-06-05 10:08:42','2018-06-05 10:08:42'),(2,NULL,'NULL','',NULL,'',1,0,'0000-00-00 00:00:00',0,'1',0,NULL,NULL,NULL,NULL,NULL,'0000-00-00','2018-06-05 10:17:16','2018-06-05 10:17:16');

/*Table structure for table `ap_post_category` */

DROP TABLE IF EXISTS `ap_post_category`;

CREATE TABLE `ap_post_category` (
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_post_category` */

/*Table structure for table `ap_post_translations` */

DROP TABLE IF EXISTS `ap_post_translations`;

CREATE TABLE `ap_post_translations` (
  `id` int(11) DEFAULT NULL,
  `language_code` varchar(5) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `content` longtext,
  `content_more` text,
  `meta_title` varchar(100) DEFAULT NULL,
  `meta_description` varchar(170) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  UNIQUE KEY `ap_post_translations_id_language_code_pk` (`id`,`language_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `ap_post_translations` */

insert  into `ap_post_translations`(`id`,`language_code`,`slug`,`title`,`description`,`content`,`content_more`,`meta_title`,`meta_description`,`meta_keyword`) values (1,'vi','ghe-bar-brixton-mau-nau','Ghế Bar Brixton, Màu nâu','Ghế Bar Brixton, Màu nâu','<p>Ghế Bar Brixton, M&agrave;u n&acirc;u</p>',NULL,'Ghế Bar Brixton, Màu nâu','Ghế Bar Brixton, Màu nâu','Ghế Bar Brixton, Màu nâu'),(2,'vi','tai-lieu-noi-bo','Tài liệu nội bộ','Tài liệu nội bộ','<p>T&agrave;i liệu nội bộ</p>',NULL,'Tài liệu nội bộ','Tài liệu nội bộ','Tài liệu nội bộ');

/*Table structure for table `ap_tour` */

DROP TABLE IF EXISTS `ap_tour`;

CREATE TABLE `ap_tour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `album` mediumtext COLLATE utf8_unicode_ci,
  `order` int(3) DEFAULT NULL,
  `is_status` tinyint(2) DEFAULT NULL,
  `price` decimal(15,0) DEFAULT '0',
  `price_sale` decimal(15,0) DEFAULT '0',
  `code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `departure` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'khởi hành',
  `from` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Nơi Đến',
  `day` int(10) DEFAULT NULL,
  `nights` int(10) DEFAULT NULL,
  `departure_date` date DEFAULT NULL COMMENT 'Ngày khởi hành',
  `map` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `album_customer` mediumtext COLLATE utf8_unicode_ci,
  `schedule` mediumtext COLLATE utf8_unicode_ci COMMENT 'Lộ trình tour',
  `detail` mediumtext COLLATE utf8_unicode_ci COMMENT 'Chi tiets tour',
  `note` mediumtext COLLATE utf8_unicode_ci,
  `included` text COLLATE utf8_unicode_ci,
  `excluded` text COLLATE utf8_unicode_ci,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `is_featured` int(2) DEFAULT '0',
  `displayed_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_tour` */

insert  into `ap_tour`(`id`,`thumbnail`,`banner`,`album`,`order`,`is_status`,`price`,`price_sale`,`code`,`company`,`departure`,`from`,`day`,`nights`,`departure_date`,`map`,`album_customer`,`schedule`,`detail`,`note`,`included`,`excluded`,`created_time`,`updated_by`,`updated_time`,`created_by`,`is_featured`,`displayed_time`) values (3,'destination-home4.jpg','bg-footer.jpg','[\"destination-home3.jpg\",\"destination-home4.jpg\",\"destination-home5.jpg\"]',NULL,1,'2000000','1500000','#123456','Apecsoft','Hà Nội','Hàn Quốc',4,3,'2018-05-30','#','[\"destination-home1.jpg\",\"destination-home2.jpg\"]','{\"6\":{\"date\":\"01\",\"title\":\"\\u0110\\u1ecba \\u0110i\\u1ec3m 1\",\"thumbnail\":\"feedback1.jpg\",\"content\":\"<p><span>N\\u1ed9i dung chuy\\u1ebfn \\u0111i test 1<\\/span><\\/p>\",\"album\":[\"destination-home3.jpg\",\"destination-home1.jpg\"]},\"7\":{\"date\":\"02\",\"title\":\"\\u0110\\u1ecba \\u0110i\\u1ec3m 2\",\"thumbnail\":\"destination-home4.jpg\",\"content\":\"<p><span>N\\u1ed9i dung chuy\\u1ebfn \\u0111i test 2<\\/span><\\/p>\",\"album\":[\"destination-home5.jpg\",\"destination-home4.jpg\",\"destination-home3.jpg\"]}}','{\"flight\":\"<p><span>Chuy\\u1ebfn bay<\\/span><\\/p>\",\"hotel\":\"<p><span>Kh&aacute;ch s\\u1ea1n<\\/span><\\/p>\",\"time\":\"<p><span>Th&ocirc;ng tin t\\u1eadp trung<\\/span><\\/p>\",\"price\":\"<p><span>Gi&aacute; tour<\\/span><\\/p>\"}','<p><span>Lưu &yacute;</span></p>','<p><label>G&oacute;i bao gồm</label><span></span></p>\r\n<div id=\"mceu_141\" class=\"mce-tinymce mce-container mce-panel\" hidefocus=\"1\" role=\"application\" tabindex=\"-1\">\r\n<div id=\"mceu_141-body\" class=\"mce-container-body mce-stack-layout\">\r\n<div id=\"mceu_142\" class=\"mce-toolbar-grp mce-container mce-panel mce-stack-layout-item mce-first\" hidefocus=\"1\" role=\"group\" tabindex=\"-1\">\r\n<div id=\"mceu_142-body\" class=\"mce-container-body mce-stack-layout\">\r\n<div id=\"mceu_143\" class=\"mce-container mce-toolbar mce-stack-layout-item mce-first\" role=\"toolbar\">\r\n<div id=\"mceu_143-body\" class=\"mce-container-body mce-flow-layout\">\r\n<div id=\"mceu_144\" class=\"mce-container mce-flow-layout-item mce-first mce-btn-group\" role=\"group\">\r\n<div id=\"mceu_144-body\">\r\n<div id=\"mceu_85\" class=\"mce-widget mce-btn mce-btn-small mce-first mce-last\" role=\"button\" aria-label=\"New document\" tabindex=\"-1\"></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>','<p><label>G&oacute;i kh&ocirc;ng bao gồm</label><span></span></p>\r\n<div id=\"mceu_226\" class=\"mce-tinymce mce-container mce-panel\" hidefocus=\"1\" role=\"application\" tabindex=\"-1\">\r\n<div id=\"mceu_226-body\" class=\"mce-container-body mce-stack-layout\">\r\n<div id=\"mceu_227\" class=\"mce-toolbar-grp mce-container mce-panel mce-stack-layout-item mce-first\" hidefocus=\"1\" role=\"group\" tabindex=\"-1\">\r\n<div id=\"mceu_227-body\" class=\"mce-container-body mce-stack-layout\">\r\n<div id=\"mceu_228\" class=\"mce-container mce-toolbar mce-stack-layout-item mce-first\" role=\"toolbar\">\r\n<div id=\"mceu_228-body\" class=\"mce-container-body mce-flow-layout\">\r\n<div id=\"mceu_229\" class=\"mce-container mce-flow-layout-item mce-first mce-btn-group\" role=\"group\">\r\n<div id=\"mceu_229-body\">\r\n<div id=\"mceu_170\" class=\"mce-widget mce-btn mce-btn-small mce-first mce-last\" role=\"button\" aria-label=\"New document\" tabindex=\"-1\"></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>','2018-06-05 00:13:20',NULL,'2018-06-05 08:50:47',NULL,0,'0000-00-00 00:00:00');

/*Table structure for table `ap_tour_category` */

DROP TABLE IF EXISTS `ap_tour_category`;

CREATE TABLE `ap_tour_category` (
  `tour_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`tour_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_tour_category` */

insert  into `ap_tour_category`(`tour_id`,`category_id`) values (1,2),(3,2);

/*Table structure for table `ap_tour_translations` */

DROP TABLE IF EXISTS `ap_tour_translations`;

CREATE TABLE `ap_tour_translations` (
  `id` int(11) DEFAULT NULL,
  `language_code` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(765) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(765) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` mediumtext COLLATE utf8_unicode_ci COMMENT 'Điểm nổi bật',
  `meta_title` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(510) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(765) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`,`language_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_tour_translations` */

insert  into `ap_tour_translations`(`id`,`language_code`,`slug`,`title`,`description`,`content`,`meta_title`,`meta_description`,`meta_keyword`) values (3,'vi','du-lich-han-quoc','Du Lịch Hàn Quốc','Du Lịch Hàn Quốc','<p>Du Lịch H&agrave;n Quốc</p>','','','');

/*Table structure for table `ap_transport` */

DROP TABLE IF EXISTS `ap_transport`;

CREATE TABLE `ap_transport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_transport` */

/*Table structure for table `ap_users` */

DROP TABLE IF EXISTS `ap_users`;

CREATE TABLE `ap_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `ap_users` */

insert  into `ap_users`(`id`,`ip_address`,`username`,`password`,`salt`,`email`,`activation_code`,`forgotten_password_code`,`forgotten_password_time`,`remember_code`,`created_on`,`last_login`,`active`,`first_name`,`last_name`,`address`,`company`,`phone`,`created_time`,`updated_time`) values (1,'127.0.0.1','admin','$2y$08$KrBa7LvLlW08hMuGayf.xuX.rpYa/eSVt09HtSwSwvN9.fiL36vlu','','contact@apecsoft.asia','',NULL,NULL,NULL,1268889823,1528163137,1,'Admin','Apecsoft',NULL,'Apecsoft','0999999999','2017-12-17 00:49:09','2018-06-05 08:45:37'),(10,'14.177.23.109','ductoan1991@gmail.com','$2y$08$J4/IAblY7eGQy.rlx8cDIuMjx0Lae.SZuUDwBzXpSqPyuD61x5Vgi',NULL,'ductoan1991@gmail.com',NULL,NULL,NULL,NULL,1522853790,1522854139,1,'Toàn Nguyễn Đức',NULL,'Ha Noi, Vietnam','Apecsoft','915224935','2018-04-04 21:56:30','2018-04-04 22:02:19');

/*Table structure for table `ap_users_groups` */

DROP TABLE IF EXISTS `ap_users_groups`;

CREATE TABLE `ap_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `ap_groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `ap_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `ap_users_groups` */

insert  into `ap_users_groups`(`id`,`user_id`,`group_id`) values (1,1,1),(2,1,2),(11,1,3),(16,10,3);

/*Table structure for table `ap_vote` */

DROP TABLE IF EXISTS `ap_vote`;

CREATE TABLE `ap_vote` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(110) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'người được vote',
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `style` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Chức vụ',
  `vote` tinyint(5) DEFAULT NULL COMMENT 'điểm vote',
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_vote` */

/*Table structure for table `ap_voucher` */

DROP TABLE IF EXISTS `ap_voucher`;

CREATE TABLE `ap_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thumbnail` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `banner` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `album` text COLLATE utf8_unicode_ci,
  `order` int(3) DEFAULT NULL,
  `is_status` tinyint(2) DEFAULT NULL,
  `price` decimal(16,0) DEFAULT NULL,
  `company` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `is_featured` int(2) DEFAULT NULL,
  `displayed_time` datetime DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ratting` int(2) DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `departure` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Khởi hành',
  `time` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Thời gian lưu trú',
  `expiration_time` date DEFAULT NULL COMMENT 'Ngày hết hạn',
  `hotel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_sale` decimal(15,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_voucher` */

insert  into `ap_voucher`(`id`,`thumbnail`,`banner`,`album`,`order`,`is_status`,`price`,`company`,`map`,`created_time`,`updated_by`,`updated_time`,`created_by`,`is_featured`,`displayed_time`,`address`,`ratting`,`phone_number`,`departure`,`time`,`expiration_time`,`hotel`,`price_sale`) values (1,'destination-home1.jpg','bg-footer.jpg','[\"destination-home1.jpg\",\"destination-home3.jpg\",\"destination-home4.jpg\",\"destination-home5.jpg\"]',NULL,1,'300000',NULL,'#',NULL,NULL,'2018-06-05 14:05:18',NULL,NULL,'2018-06-04 00:00:00',NULL,3,'001233456789',NULL,'{\"3\":{\"from\":\"H\\u00e0 N\\u1ed9i\",\"price\":\"25000\"},\"4\":{\"from\":\"H\\u1ed3 Ch\\u00ed Minh\",\"price\":\"26000\"}}',NULL,'Apecsoft','25000');

/*Table structure for table `ap_voucher_category` */

DROP TABLE IF EXISTS `ap_voucher_category`;

CREATE TABLE `ap_voucher_category` (
  `voucher_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`voucher_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_voucher_category` */

insert  into `ap_voucher_category`(`voucher_id`,`category_id`) values (1,12);

/*Table structure for table `ap_voucher_translations` */

DROP TABLE IF EXISTS `ap_voucher_translations`;

CREATE TABLE `ap_voucher_translations` (
  `id` int(11) DEFAULT NULL,
  `language_code` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(2295) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(2295) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `meta_title` varchar(900) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` varchar(1530) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_keyword` varchar(2295) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Policy` text COLLATE utf8_unicode_ci,
  `info` text COLLATE utf8_unicode_ci,
  `utilities` text COLLATE utf8_unicode_ci,
  UNIQUE KEY `ap_voucher_translations_id_language_code_pk` (`id`,`language_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `ap_voucher_translations` */

insert  into `ap_voucher_translations`(`id`,`language_code`,`slug`,`title`,`description`,`content`,`meta_title`,`meta_description`,`meta_keyword`,`Policy`,`info`,`utilities`) values (1,'vi','ghe-bar-brixton-mau-nau','Ghế Bar Brixton, Màu nâu 1','Giá chuyến bay 1','<p>Gi&aacute; chuyến bay 1</p>','Giá chuyến bay','Giá chuyến bay','','<p><span>Ch&iacute;nh s&aacute;ch 1</span></p>','<p><span>Th&ocirc;ng tin 1</span></p>','<p>Tiện &iacute;ch 1</p>');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
